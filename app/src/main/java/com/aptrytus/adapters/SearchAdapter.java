package com.aptrytus.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aptrytus.R;
import com.aptrytus.uiactivities.SearchListActivity;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Belal on 6/6/2017.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private ArrayList<String> names;
    Context context;

    public SearchAdapter(Context context, ArrayList<String> names) {
        this.names = names;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.textViewName.setText(names.get(position));

        holder.textViewName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SearchListActivity)context).sendSelectedData(holder.textViewName.getText().toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return names.size();
    }

    public void filterList(ArrayList<String> filterdNames) {

        this.names = filterdNames;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;

        ViewHolder(View itemView) {
            super(itemView);

            textViewName = (TextView) itemView.findViewById(R.id.textViewName);
        }
    }


}
