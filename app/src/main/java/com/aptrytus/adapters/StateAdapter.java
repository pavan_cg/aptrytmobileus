package com.aptrytus.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aptrytus.R;
import com.aptrytus.models.State;

import java.util.List;

public class StateAdapter  extends BaseAdapter{
    List<State> states;
    Context context;

    public StateAdapter(Context context,List<State> states){
        this.context = context;
        this.states = states;

    }

    @Override
    public int getCount() {
        return states.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.spinner_item, null);
        TextView textView = (TextView) v.findViewById(R.id.spinnerText);
        textView.setText(states.get(position).getStateName());
        return v;
    }
}
