package com.aptrytus.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.aptrytus.R;

import java.util.ArrayList;
import java.util.HashMap;

public class SingleAdapter extends BaseAdapter {

    String genderArray[];
    Context context;

    public SingleAdapter(Context context,  String objects[]) {
        //super(context);
        this.context = context;
        genderArray = objects;
    }

    @Override
    public int getCount() {
        return genderArray.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.spinner_item, null);
        TextView textView = (TextView) v.findViewById(R.id.spinnerText);
        textView.setText(genderArray[position]);

        return v;

    }

}
