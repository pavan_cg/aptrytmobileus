package com.aptrytus.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aptrytus.R;
import com.aptrytus.facilities.Address;
import com.aptrytus.facilities.FacilityAttributes;
import com.aptrytus.facilities.FacilityResponse;
import com.aptrytus.facilities.Facility;
import com.aptrytus.interfaces.DoctorListCallback;
import com.aptrytus.viewholders.DoctorViewHolder;

import java.util.List;

public class DoctotsListAdapter extends RecyclerView.Adapter<DoctorViewHolder> {

    private FacilityResponse facilityResponse;
    Context mCtx;
    DoctorListCallback callback;

    public DoctotsListAdapter(DoctorListCallback callback, FacilityResponse facilityResponse) {
        this.callback = callback;
        this.facilityResponse = facilityResponse;
    }


   /* @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;

        *//*return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row, parent, false));*//*
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        //holder.setText(list.get(position));
    }*/


    @NonNull
    @Override
    public DoctorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //create view holder
        return new DoctorViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.doctor_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorViewHolder holder, final int position) {

        List<Facility> results = facilityResponse.getFacility();//.get(position).getFacilityName();
        if (results == null || results.isEmpty()) {
            holder.setText("No Results");
        }
        final Facility facility = facilityResponse.getFacility().get(position);
        if (facility == null) {
            holder.setText("No facilty found at: " + position);
        }
        holder.setText("" + facility.getFacilityName()); // safe to de-reference and set as you are concatenating with string.
        holder.facilityBrand.setText("" + facility.getFacilityBrand()); // safe to de-reference and set as you are concatenating with string.

        Address address = facility.getAddress();
        if (address == null) {
            holder.facilityAddress.setText("");
        }
        String street = address.getStreet();
        holder.facilityAddress.setText((street == null) ? address.getCity() : street + "," + address.getCity());

        // display facility attributes
        final FacilityAttributes attributes = facility.getFacilityAttributes();
        if (attributes.getOccupationalHealth() != null &&
                attributes.getOccupationalHealth().trim().equalsIgnoreCase("y")) {
            holder.ocupational_icon.setImageResource(R.mipmap.ic_insurance_head_icon);
        } else {
            holder.ocupational_icon.setImageResource(R.mipmap.icon_occupation_grey);
        }


        if (attributes.getFamilyCare() != null &&
                attributes.getFamilyCare().trim().equalsIgnoreCase("y")) {
            holder.family_icon.setImageResource(R.mipmap.ic_group_icon);
        } else {
            holder.family_icon.setImageResource(R.mipmap.icon_familycare_grey);
        }


        if (attributes.getPediatricCare() != null &&
                attributes.getPediatricCare().trim().equalsIgnoreCase("y")) {
            holder.pediatric_icon.setImageResource(R.mipmap.pediatric_new);
        } else {
            holder.pediatric_icon.setImageResource(R.mipmap.pediatric_new);
        }


        if (attributes.getMedicare() != null &&
                attributes.getMedicare().trim().equalsIgnoreCase("y")) {
            holder.medicare_icon.setImageResource(R.mipmap.ic_m_icon);
        } else {
            holder.medicare_icon.setImageResource(R.mipmap.ic_m_icon_grey);
        }


        if (attributes.getSelfPay() != null &&
                attributes.getSelfPay().trim().equalsIgnoreCase("y")) {
            holder.selfService_icon.setImageResource(R.mipmap.ic_s_icon);
        } else {
            holder.selfService_icon.setImageResource(R.mipmap.ic_s_icon_grey);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onDoctorListClicked(facility, position);
            }
        });
        holder.umberalla_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onUmberllaIconClicked(facility, position);
            }
        });

        holder.drlist_appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onUmberllaIconClicked(facility, position);
            }
        });
        holder.selfService_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (attributes.getSelfPay().trim().equalsIgnoreCase("y")) {
                    callback.selfServiceIconClicked(facility, position);
                } else {
                }
            }
        });
        holder.medicare_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        holder.family_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        holder.pediatric_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        holder.ocupational_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    @Override
    public int getItemCount() {
        //return facilityResponse.getFacility().size();
        return facilityResponse.getFacility() == null ? 0 : facilityResponse.getFacility().size();
    }
}

