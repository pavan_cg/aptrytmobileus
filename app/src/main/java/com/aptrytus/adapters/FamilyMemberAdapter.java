package com.aptrytus.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aptrytus.Others.UserList;
import com.aptrytus.R;
import com.aptrytus.facilities.SelfPayService;
import com.aptrytus.facilities.ServiceList;

import java.util.ArrayList;
import java.util.List;

public class FamilyMemberAdapter extends BaseAdapter {

    private List<UserList> listData;

    private LayoutInflater layoutInflater;

    public FamilyMemberAdapter(Context context, List<UserList> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.selft_service_list_row, null);
            holder = new ViewHolder();
            holder.selfServiceName = (TextView) convertView.findViewById(R.id.tv_self_servicName);
            holder.getSelfServiceRange = (TextView) convertView.findViewById(R.id.tv_self_serviceRange);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.selfServiceName.setText(listData.get(position).getFirstName());
        holder.getSelfServiceRange.setText(listData.get(position).getRelation());

        return convertView;
    }

    static class ViewHolder {
        TextView selfServiceName;
        TextView getSelfServiceRange;
    }

}
