package com.aptrytus.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aptrytus.R;
import com.aptrytus.facilities.ServiceList;

import java.util.List;

public class InsuranceServiceAdapter extends BaseAdapter {

    private List<String> listData;

    private LayoutInflater layoutInflater;

    public InsuranceServiceAdapter(Context context, List<String> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        InsuranceServiceAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.insurance_service_list_row, null);
            holder = new InsuranceServiceAdapter.ViewHolder();
            holder.insuranceServiceName = (TextView) convertView.findViewById(R.id.tv_insurance_servicName);
            convertView.setTag(holder);
        } else {
            holder = (InsuranceServiceAdapter.ViewHolder) convertView.getTag();
        }

        holder.insuranceServiceName.setText(listData.get(position));

        return convertView;
    }

    static class ViewHolder {
        TextView insuranceServiceName;
    }

}

