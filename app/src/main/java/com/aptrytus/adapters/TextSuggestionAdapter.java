package com.aptrytus.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aptrytus.R;
import com.aptrytus.models.TextSearchList;
import com.aptrytus.models.TextSuggestion;
import com.aptrytus.models.TextSuggestionsResponse;

import java.util.ArrayList;
import java.util.List;

public class TextSuggestionAdapter extends RecyclerView.Adapter<TextSuggestionAdapter.MyViewHolder> {

    private List<TextSuggestionsResponse> textSuggestions;

    Context context;

    public interface OnItemClickListener {
        void onItemClick(String test,int position);
    }
    OnItemClickListener listener;
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.tv_search_name);
        }
    }

    public TextSuggestionAdapter(List<TextSuggestionsResponse> data, OnItemClickListener listener) {
        this.context = context;
        this.textSuggestions = data;
        this.listener = listener;
    }

    public TextSuggestionAdapter(List<TextSuggestionsResponse> data) {
        this.context = context;
        this.textSuggestions = data;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.text_search_card_row, parent, false);
        // view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        final TextView textViewName = holder.textViewName;

        textViewName.setText(textSuggestions.get(listPosition).getSuggestionValue());

        textViewName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("SelectedItem",textViewName.getText().toString());

                listener.onItemClick(textViewName.getText().toString(),listPosition);

            }
        });

    }

    @Override
    public int getItemCount() {
        return textSuggestions == null ? 0 : textSuggestions.size();
    }
}

