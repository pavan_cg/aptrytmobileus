package com.aptrytus.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aptrytus.R;
import com.aptrytus.familyMembersInsurance.FamilyMemberInsuranceResponse;
import com.aptrytus.familyMembersInsurance.FamilyMemberdetail;

public class FamilyMemberListAdapter extends RecyclerView.Adapter<FamilyMemberListAdapter.MyViewHolder> {

    private FamilyMemberInsuranceResponse familyMemberInsuranceResponses;

    Context context;

    public interface OnItemClickListener {
        void onItemClick(String childId,String parentId);
        void onItemClickEdit(String memberId,String parentId, FamilyMemberdetail familyMemberdetail);
    }
    FamilyMemberListAdapter.OnItemClickListener listener;
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView displayName;
        TextView relationShip;
        TextView dob;
        ImageView delete;
        ImageView edit;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.displayName = (TextView) itemView.findViewById(R.id.displayName);
            this.relationShip = (TextView) itemView.findViewById(R.id.relationDisplay);
            this.dob = (TextView) itemView.findViewById(R.id.dobDisplay);
            this.delete = (ImageView) itemView.findViewById(R.id.deleteMembers);
            this.edit = (ImageView) itemView.findViewById(R.id.editMembers);
        }
    }

    public FamilyMemberListAdapter(FamilyMemberInsuranceResponse data, FamilyMemberListAdapter.OnItemClickListener listener) {
        this.context = context;
        this.familyMemberInsuranceResponses = data;
        this.listener = listener;
    }

    public FamilyMemberListAdapter(FamilyMemberInsuranceResponse data) {
        this.context = context;
        this.familyMemberInsuranceResponses = data;
        this.listener = listener;
    }

    @Override
    public FamilyMemberListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.family_member_list_item, parent, false);
        // view.setOnClickListener(MainActivity.myOnClickListener);

        FamilyMemberListAdapter.MyViewHolder myViewHolder = new FamilyMemberListAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final FamilyMemberListAdapter.MyViewHolder holder, final int listPosition) {

        final FamilyMemberdetail familyMemberdetail = familyMemberInsuranceResponses.getUserInfoResponse().getFamilyMemberdetails().get(listPosition);

        holder.displayName.setText(familyMemberdetail.getFirstName() +" "+ familyMemberdetail.getLastName());

        holder.relationShip.setText(familyMemberdetail.getUserRelationShip());

        holder.dob.setText(familyMemberdetail.getDateOfBirth());

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(familyMemberdetail.getId(),familyMemberInsuranceResponses.getUserInfoResponse().getId());
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClickEdit(familyMemberdetail.getId(),familyMemberInsuranceResponses.getUserInfoResponse().getId(),familyMemberdetail);
            }
        });
/*        textViewName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("SelectedItem",textViewName.getText().toString());

                listener.onItemClick(textViewName.getText().toString(),listPosition);

            }
        });*/

    }

    @Override
    public int getItemCount() {
        return familyMemberInsuranceResponses.getUserInfoResponse() == null ? 0 : familyMemberInsuranceResponses.getUserInfoResponse().getFamilyMemberdetails().size();
    }
}

