package com.aptrytus.interfaces;

public interface UserCreationCallback {

    void onSuccess(String message);
    void onError(String message);
}
