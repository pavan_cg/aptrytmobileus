package com.aptrytus.interfaces;

import com.aptrytus.facilities.Facility;

public interface DoctorListCallback {

    public void onDoctorListClicked(Facility facilityResponse, int position);

    public void onUmberllaIconClicked(Facility facilityResponse, int position);

    public void selfServiceIconClicked(Facility facilityResponse, int position);
}
