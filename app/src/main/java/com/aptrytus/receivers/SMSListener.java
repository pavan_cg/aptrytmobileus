package com.aptrytus.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;

import com.aptrytus.interfaces.MessageListener;
import com.google.android.gms.common.internal.service.Common;

public class SMSListener extends BroadcastReceiver {

    private static MessageListener mListener2;

    @Override
    public void onReceive(Context context, Intent intent) {

        // Get Bundle object contained in the SMS intent passed in
        Bundle bundle = intent.getExtras();
        SmsMessage[] smsm = null;
        String sms_str ="";

        if (bundle != null)
        {
            // Get the SMS message
            Object[] pdus = (Object[]) bundle.get("pdus");
            smsm = new SmsMessage[pdus.length];
            for (int i=0; i<smsm.length; i++){

                String message = "";
                smsm[i] = SmsMessage.createFromPdu((byte[])pdus[i]);

                sms_str += smsm[i].getMessageBody().toString();

                String sender = smsm[i].getOriginatingAddress();
                message = getOnlyOtp(smsm[i].getMessageBody().toString());
                //Check here sender is yours

                //if(sender.equals("+919880065362") || sender.equals("9880065362")){
                if(sender.equals("AD-440004") || sender.equals("VK-440004") || sender.equals("AD-440604") ||
                        sender.equals("VK-440604")|| sender.equals("VK-440004")){
                    mListener2.messageReceived(message);
                }


            }
        }


    }

    public static void bindListener(MessageListener mListener){
        mListener2 = mListener;
    }

    public String getOnlyOtp(String message){

        String otp = message.substring(19,25);

        return otp;

    }
}
