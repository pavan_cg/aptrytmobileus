package com.aptrytus.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;


public abstract class AptrytDb extends RoomDatabase {

    public static AptrytDb instance;

    public abstract FamilyListDao familyListDao();

    public static synchronized AptrytDb getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), AptrytDb.class, "aptryt_database")
                    .addCallback(roomCallBack)
                    .fallbackToDestructiveMigration().build();
        }
        return instance;
    }


    private static RoomDatabase.Callback roomCallBack = new RoomDatabase.Callback() {

        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }
    };


}


