package com.aptrytus.services;

public class AppURLS {

    //put your base url here
    public static final String BASE_URL = "http://ec2-52-38-39-114.us-west-2.compute.amazonaws.com:8080/ustryt/";
    //put the end points here

    // text search end Point
    public static final String TEXT_SUGGESTIONS = "providerSuggestions";

    public static final String DOCTOR_LIST = "   ";

    public static final String FACILITIES_LIST = "providerSearch";

    public static final String SEND_OTP = "userInfo/sendOtp";

    public static final String CREATE_USER = "userInfo/createUser";
    //public static final String CREATE_USER = "userInfo/createProfileInfo";

    public static final String VERIFY_OTP = "userInfo/verifyOtp";

    public static final String LOGIN_USER = "login";

    public static final String INSURANCE_LIST = "insurance/getInsuranceList";

    public static final String ADD_FAMILY_MEMBER = "userInfo/createFamilyMember";

    public static final String GET_FAMILY_MEMBER = "userInfo/getUserInfo";

    public static final String DELETE_FAMILY_MEMBER = "userInfo/deleteFamilyMember";

    public static final String UPLOAD_INSURANCE = "user/uploadInsurance";

    public static final String APPOINTMENT_BOOKING = "appointment/create";

}
