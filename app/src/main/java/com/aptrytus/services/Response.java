package com.aptrytus.services;

public class Response<T> {

    Status status;
    String message;
    T data;
}
