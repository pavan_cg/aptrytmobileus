package com.aptrytus.services;

public enum Status {

    Loading, LoadingFinished, Success, Error
}
