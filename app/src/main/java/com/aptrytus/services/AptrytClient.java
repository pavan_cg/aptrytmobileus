package com.aptrytus.services;

import android.support.annotation.NonNull;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AptrytClient {

    @NonNull
    public static Retrofit getClient() {
        return new Retrofit.Builder()
                .baseUrl(AppURLS.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
