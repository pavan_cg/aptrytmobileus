package com.aptrytus.services;

import com.aptrytus.facilities.FacilityResponse;
import com.aptrytus.familyMembersInsurance.FamilyMemberInsuranceResponse;
import com.aptrytus.familymebers.familyMemberResponse;
import com.aptrytus.login.LoginRequest;
import com.aptrytus.login.LoginResponse;
import com.aptrytus.models.DoctorsListResponse;
import com.aptrytus.models.SignUpResponse;
import com.aptrytus.models.TextSuggestionsResponse;
import com.aptrytus.request.CreateUserRequest;
import com.aptrytus.request.SendOtpRequest;
import com.aptrytus.request.appointmentBooking.AppointmentBookingRequest;
import com.aptrytus.response.CreateUserResponse;
import com.aptrytus.response.SendOtpResponse;
import com.aptrytus.response.appointmentbookingResponse.AppointmentBookingResponse;
import com.aptrytus.response.imageuploadresp.ImageUploadResponse;
import com.aptrytus.response.insurance.InsuranceListResponse;
import com.google.gson.JsonElement;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface AptrytInterface {

    //String BASE_URL = "https://jsonplaceholder.typicode.com/";

    /*@GET("photos")
    Call<List<PhotosModel>> getThumbUrls();*/

    // to get text suggestions from user inputs
    @GET(AppURLS.TEXT_SUGGESTIONS)
    Call<List<TextSuggestionsResponse>> getSuggestions(@Query("searchQry") String textSuggestion,@Query("City") String city,@Query("Zipcode") String zipcode);

    /*//  to get the list of doctors
    @GET(AppURLS.DOCTOR_LIST)
    Call<DoctorsListResponse> getDoctors(@Query("searchQry") String searchQuery);
*/

    //  to get the list of doctors
    @GET(AppURLS.FACILITIES_LIST)
    Call<FacilityResponse> geFacilities(@Query("searchQry") String searchQuery,@Query("City") String city,@Query("Zipcode") String zipcode);

   /* //  to get the list of doctors
    @GET(AppURLS.FACILITIES_LIST)
    Call<FacilityResponse> geFacilities(@Query("searchQry") String searchQuery);*/

    // to get the OTP for user activation
    @POST(AppURLS.SEND_OTP)
    Call<SendOtpResponse> getOtp(@Body SendOtpRequest sendOtpRequest);

    // to verify the OTP received for user activation
    @POST(AppURLS.VERIFY_OTP)
    Call<SendOtpResponse> verifyOtp(@Body SendOtpRequest sendOtpRequest);

    // to register user
    @POST(AppURLS.CREATE_USER)
    Call<CreateUserResponse> registerUser(@Body CreateUserRequest createUserRequest);

    // to login user
    @POST(AppURLS.LOGIN_USER)
    Call<LoginResponse> loginUserRequest(@Body LoginRequest loginRequest);

    //  to get the list of insurances
    @GET(AppURLS.INSURANCE_LIST)
    Call<InsuranceListResponse> getInsurances(@Query("searchQry") String searchQuery);


    @POST(AppURLS.ADD_FAMILY_MEMBER)
    Call<familyMemberResponse> addFamilyMember(@Body CreateUserRequest addFamilyMember,@Query("userId") String userId,@Query("relationShip") String relationShip);

    //  to get the list of insurances
    @GET(AppURLS.GET_FAMILY_MEMBER)
    Call<FamilyMemberInsuranceResponse> getFamilyMembers(@Query("userId") String userId);

    //  to get the list of insurances
    @GET(AppURLS.DELETE_FAMILY_MEMBER)
    Call<familyMemberResponse> deleteFamilyMembers(@Query("childId") String childId,@Query("parentUserId") String parentUserId);

    // Multipart request to upload images
    @Multipart
    @POST(AppURLS.UPLOAD_INSURANCE)
    Call<ImageUploadResponse> uploadImage(@Part MultipartBody.Part file, @Part("userId") RequestBody requestBody);

    // to book an appointmnet

    @POST(AppURLS.APPOINTMENT_BOOKING)
    Call<AppointmentBookingResponse> getAppointment(@Body AppointmentBookingRequest appointmentBookingRequest);


}
