package com.aptrytus.services;

import com.aptrytus.facilities.FacilityResponse;
import com.aptrytus.familyMembersInsurance.FamilyMemberInsuranceResponse;
import com.aptrytus.familymebers.familyMemberResponse;
import com.aptrytus.login.LoginResponse;
import com.aptrytus.models.DoctorsListResponse;
import com.aptrytus.models.SignUpResponse;
import com.aptrytus.models.TextSuggestionsResponse;
import com.aptrytus.response.CreateUserResponse;
import com.aptrytus.response.SendOtpResponse;
import com.aptrytus.response.appointmentbookingResponse.AppointmentBookingResponse;
import com.aptrytus.response.imageuploadresp.ImageUploadResponse;
import com.aptrytus.response.insurance.InsuranceListResponse;

import org.json.JSONObject;

import java.util.List;

import okhttp3.ResponseBody;

public class ApiResponse {

    public List<TextSuggestionsResponse> posts;
    public DoctorsListResponse doctorsListResponse;
    public SignUpResponse signUpResponse;
    public InsuranceListResponse insuranceListResponse;
    private Throwable error;
    private ResponseBody errorBody;
    private JSONObject apiError;
    public AppointmentBookingResponse appointmentBookingResponse;

    private SendOtpResponse sendOtpResponse;


    public FacilityResponse facilitiesList;

    public CreateUserResponse createUserResponse;

    public LoginResponse loginResponse;

    public familyMemberResponse familyMemberResponse;

    public FamilyMemberInsuranceResponse familyMemberInsuranceResponse;

    public ImageUploadResponse imageUploadResponse;

    public ApiResponse(List<TextSuggestionsResponse> posts) {
        this.posts = posts;
        this.error = null;
    }

    public ApiResponse(SendOtpResponse sendOtpResponse) {
        this.sendOtpResponse = sendOtpResponse;
        this.error = null;
    }

    public ApiResponse(Throwable error) {
        this.error = error;
        this.posts = null;
    }

    public ApiResponse(JSONObject apiError) {
        this.apiError = apiError;
        this.posts = null;
    }

    public ApiResponse(ResponseBody errorBody) {
        this.errorBody = errorBody;
        this.error = null;
    }

    public ApiResponse(DoctorsListResponse doctorsListResponse) {
        this.doctorsListResponse = doctorsListResponse;
        this.error = null;
    }

    public ApiResponse(FacilityResponse facilitiesList) {
        this.facilitiesList = facilitiesList;
        this.error = null;
    }

    public ApiResponse(AppointmentBookingResponse appointmentBookingResponse) {
        this.appointmentBookingResponse = appointmentBookingResponse;
        this.error = null;
    }


    public ApiResponse(FamilyMemberInsuranceResponse familyMemberInsuranceResponse) {
        this.familyMemberInsuranceResponse = familyMemberInsuranceResponse;
        this.error = null;
    }

    public InsuranceListResponse getInsuranceListResponse() {
        return insuranceListResponse;
    }

    public void setInsuranceListResponse(InsuranceListResponse insuranceListResponse) {
        this.insuranceListResponse = insuranceListResponse;
    }

    public ApiResponse(CreateUserResponse createUserResponse) {
        this.createUserResponse = createUserResponse;
        this.error = null;
    }



    public void setAppointmentBookingResponse(AppointmentBookingResponse appointmentBookingResponse) {
        this.appointmentBookingResponse = appointmentBookingResponse;
    }

    public ApiResponse(InsuranceListResponse insuranceListResponse) {
        this.insuranceListResponse = insuranceListResponse;

        this.error = null;
    }
    public ApiResponse(familyMemberResponse familyMemberResponse) {
        this.familyMemberResponse = familyMemberResponse;
        this.error = null;
    }

    public ApiResponse(ImageUploadResponse imageUploadResponse) {
        this.imageUploadResponse = imageUploadResponse;
        this.error = null;
    }

    public ResponseBody getErrorBody() {
        return errorBody;
    }

    public void setErrorBody(ResponseBody errorBody) {
        this.errorBody = errorBody;
    }

    public JSONObject getApiError() {
        return apiError;
    }

    public void setApiError(JSONObject apiError) {
        this.apiError = apiError;
    }

    public ApiResponse(LoginResponse loginResponse) {

        this.loginResponse = loginResponse;
        this.error = null;
    }
    public FacilityResponse getFacilitiesListResponse() {
        return facilitiesList;
    }

    public void setFacilitiesListResponse(FacilityResponse resultList) {
        this.facilitiesList = resultList;
    }

    public DoctorsListResponse getDoctorsListResponse() {
        return doctorsListResponse;
    }

    public List<TextSuggestionsResponse> getTextSuggestions() {
        return posts;
    }

   /* public SignUpResponse getSignUpResponse() {
        return signUpResponse;
    }*/

    public void setTextSuggestions(List<TextSuggestionsResponse> posts) {
        this.posts = posts;
    }

    public void setDoctorsListResponse(DoctorsListResponse doctorsListResponse) {
        this.doctorsListResponse = doctorsListResponse;
    }

    public SendOtpResponse getSendOtpResponse() {
        return sendOtpResponse;
    }

    public void setSendOtpResponse(SendOtpResponse sendOtpResponse) {
        this.sendOtpResponse = sendOtpResponse;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public CreateUserResponse getCreateUserResponse() {
        return createUserResponse;
    }

    public void setCreateUserResponse(CreateUserResponse createUserResponse) {
        this.createUserResponse = createUserResponse;
    }

    public LoginResponse getLoginResponse() {
        return loginResponse;
    }

    public AppointmentBookingResponse getAppointmentBookingResponse() {
        return appointmentBookingResponse;
    }

}
