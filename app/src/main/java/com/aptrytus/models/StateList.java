package com.aptrytus.models;

import java.util.List;

public class StateList {

    private List<State> stateList;

    public List<State> getStateList() {
        return stateList;
    }

    public void setStateList(List<State> stateList) {
        this.stateList = stateList;
    }
}
