
package com.aptrytus.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TextSuggestionsResponse {

    /*
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("searchTag")
    @Expose
    private String searchTag;
    @SerializedName("resultList")
    @Expose
    private List<TextSearchList> resultList = null;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public TextSuggestionsResponse withMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public String getSearchTag() {
        return searchTag;
    }

    public void setSearchTag(String searchTag) {
        this.searchTag = searchTag;
    }

    public TextSuggestionsResponse withSearchTag(String searchTag) {
        this.searchTag = searchTag;
        return this;
    }

    public List<TextSearchList> getTextSearchList() {
        return resultList;
    }

    public void setResultList(List<TextSearchList> resultList) {
        this.resultList = resultList;
    }

    public TextSuggestionsResponse withResultList(List<TextSearchList> resultList) {
        this.resultList = resultList;
        return this;
    }*/

    @SerializedName("suggestionValue")
    @Expose
    private String suggestionValue;

    public String getSuggestionValue() {
        return suggestionValue;
    }

    public void setSuggestionValue(String suggestionValue) {
        this.suggestionValue = suggestionValue;
    }
}
