package com.aptrytus.models.databindingmodels;

import android.databinding.BaseObservable;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Patterns;

public class UserDetails extends BaseObservable {

    @NonNull
    private String pcpName;
    @NonNull
    private String pcpContact;
    @NonNull
    private Long mobileNumber;
    @NonNull
    private String email;
    @NonNull
    private String password;

    public UserDetails(@NonNull String pcpName, @NonNull String pcpContact, @NonNull Long mobileNumber, @NonNull String email, @NonNull String password) {
        this.pcpName = pcpName;
        this.pcpContact = pcpContact;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.password = password;
    }

    public UserDetails(){

    }

    @NonNull
    public String getPcpName() {
        return pcpName;
    }

    public void setPcpName(@NonNull String pcpName) {
        this.pcpName = pcpName;
    }

    @NonNull
    public String getPcpContact() {
        return pcpContact;
    }

    public void setPcpContact(@NonNull String pcpContact) {
        this.pcpContact = pcpContact;
    }

    @NonNull
    public Long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(@NonNull Long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NonNull String email) {
        this.email = email;
    }

    @NonNull
    public String getPassword() {
        return password;
    }

    public void setPassword(@NonNull String password) {
        this.password = password;
    }

    public boolean isValidData(){
        return !TextUtils.isEmpty(getEmail()) &&
                Patterns.EMAIL_ADDRESS.matcher(getEmail()).matches() &&
                getPassword().length()  > 6;
    }
}
