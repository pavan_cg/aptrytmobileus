package com.aptrytus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignUpResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("list")
    @Expose
    private String list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SignUpResponse withMessage(String message) {
        this.message = message;
        return this;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public SignUpResponse withList(String list) {
        this.list = list;
        return this;
    }

}