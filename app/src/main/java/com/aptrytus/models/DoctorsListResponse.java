package com.aptrytus.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorsListResponse {

    @SerializedName("message")
    @Expose
    private Object msg;
    @SerializedName("resultList")
    @Expose
    private List<DoctorDetails> resultList = null;


    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }

    public DoctorsListResponse withMsg(Object msg) {
        this.msg = msg;
        return this;
    }

    public List<DoctorDetails> getResultList() {
        return resultList;
    }

    public void setResultList(List<DoctorDetails> resultList) {
        this.resultList = resultList;
    }

    public DoctorsListResponse withResultList(List<DoctorDetails> resultList) {
        this.resultList = resultList;
        return this;
    }

}

