
package com.aptrytus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TextSearchList {

    @SerializedName("suggestionID")
    @Expose
    private Integer suggestionID;
    @SerializedName("searchQuery")
    @Expose
    private String searchQuery;
    @SerializedName("suggestionTag")
    @Expose
    private String suggestionTag;

    public Integer getSuggestionID() {
        return suggestionID;
    }

    public void setSuggestionID(Integer suggestionID) {
        this.suggestionID = suggestionID;
    }

    public TextSearchList withSuggestionID(Integer suggestionID) {
        this.suggestionID = suggestionID;
        return this;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public TextSearchList withSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
        return this;
    }

    public String getSuggestionTag() {
        return suggestionTag;
    }

    public void setSuggestionTag(String suggestionTag) {
        this.suggestionTag = suggestionTag;
    }

    public TextSearchList withSuggestionTag(String suggestionTag) {
        this.suggestionTag = suggestionTag;
        return this;
    }

}
