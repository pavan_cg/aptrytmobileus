package com.aptrytus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("medicalCondition")
    @Expose
    private String medicalCondition;
    @SerializedName("medCondId")
    @Expose
    private Integer medCondId;
    @SerializedName("specialtyId")
    @Expose
    private Integer specialtyId;
    @SerializedName("specialtyName")
    @Expose
    private String specialtyName;
    @SerializedName("docId")
    @Expose
    private Integer docId;
    @SerializedName("docFullName")
    @Expose
    private String docFullName;
    @SerializedName("facilityId")
    @Expose
    private Integer facilityId;
    @SerializedName("facilityName")
    @Expose
    private String facilityName;
    @SerializedName("facilityWorkDays")
    @Expose
    private String facilityWorkDays;
    @SerializedName("facilityPrimaryContact")
    @Expose
    private String facilityPrimaryContact;
    @SerializedName("facilitySecondaryContact")
    @Expose
    private String facilitySecondaryContact;
    @SerializedName("facilityImageLoc")
    @Expose
    private Object facilityImageLoc;
    @SerializedName("facilityAddrDrno")
    @Expose
    private String facilityAddrDrno;
    @SerializedName("facilityAddrFlrno")
    @Expose
    private String facilityAddrFlrno;
    @SerializedName("facilityAddrRoomno")
    @Expose
    private String facilityAddrRoomno;
    @SerializedName("facilityAddrStreet")
    @Expose
    private String facilityAddrStreet;
    @SerializedName("facilityAddrCity")
    @Expose
    private String facilityAddrCity;
    @SerializedName("facilityAddrPostalCode")
    @Expose
    private Integer facilityAddrPostalCode;
    @SerializedName("facilityAddrState")
    @Expose
    private String facilityAddrState;
    @SerializedName("facilityCountry")
    @Expose
    private String facilityCountry;
    @SerializedName("facilityLatitude")
    @Expose
    private Object facilityLatitude;
    @SerializedName("facilityLongitude")
    @Expose
    private Object facilityLongitude;
    @SerializedName("facilityEndTime")
    @Expose
    private String facilityEndTime;
    @SerializedName("facilityStartTime")
    @Expose
    private String facilityStartTime;
    @SerializedName("elasticserachDocFacQry")
    @Expose
    private String elasticserachDocFacQry;
    @SerializedName("elasticserachCondQry")
    @Expose
    private String elasticserachCondQry;
    @SerializedName("elasticserachQry")
    @Expose
    private String elasticserachQry;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DoctorDetails withId(Integer id) {
        this.id = id;
        return this;
    }

    public String getMedicalCondition() {
        return medicalCondition;
    }

    public void setMedicalCondition(String medicalCondition) {
        this.medicalCondition = medicalCondition;
    }

    public DoctorDetails withMedicalCondition(String medicalCondition) {
        this.medicalCondition = medicalCondition;
        return this;
    }

    public Integer getMedCondId() {
        return medCondId;
    }

    public void setMedCondId(Integer medCondId) {
        this.medCondId = medCondId;
    }

    public DoctorDetails withMedCondId(Integer medCondId) {
        this.medCondId = medCondId;
        return this;
    }

    public Integer getSpecialtyId() {
        return specialtyId;
    }

    public void setSpecialtyId(Integer specialtyId) {
        this.specialtyId = specialtyId;
    }

    public DoctorDetails withSpecialtyId(Integer specialtyId) {
        this.specialtyId = specialtyId;
        return this;
    }

    public String getSpecialtyName() {
        return specialtyName;
    }

    public void setSpecialtyName(String specialtyName) {
        this.specialtyName = specialtyName;
    }

    public DoctorDetails withSpecialtyName(String specialtyName) {
        this.specialtyName = specialtyName;
        return this;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    public DoctorDetails withDocId(Integer docId) {
        this.docId = docId;
        return this;
    }

    public String getDocFullName() {
        return docFullName;
    }

    public void setDocFullName(String docFullName) {
        this.docFullName = docFullName;
    }

    public DoctorDetails withDocFullName(String docFullName) {
        this.docFullName = docFullName;
        return this;
    }

    public Integer getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Integer facilityId) {
        this.facilityId = facilityId;
    }

    public DoctorDetails withFacilityId(Integer facilityId) {
        this.facilityId = facilityId;
        return this;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public DoctorDetails withFacilityName(String facilityName) {
        this.facilityName = facilityName;
        return this;
    }

    public String getFacilityWorkDays() {
        return facilityWorkDays;
    }

    public void setFacilityWorkDays(String facilityWorkDays) {
        this.facilityWorkDays = facilityWorkDays;
    }

    public DoctorDetails withFacilityWorkDays(String facilityWorkDays) {
        this.facilityWorkDays = facilityWorkDays;
        return this;
    }

    public String getFacilityPrimaryContact() {
        return facilityPrimaryContact;
    }

    public void setFacilityPrimaryContact(String facilityPrimaryContact) {
        this.facilityPrimaryContact = facilityPrimaryContact;
    }

    public DoctorDetails withFacilityPrimaryContact(String facilityPrimaryContact) {
        this.facilityPrimaryContact = facilityPrimaryContact;
        return this;
    }

    public String getFacilitySecondaryContact() {
        return facilitySecondaryContact;
    }

    public void setFacilitySecondaryContact(String facilitySecondaryContact) {
        this.facilitySecondaryContact = facilitySecondaryContact;
    }

    public DoctorDetails withFacilitySecondaryContact(String facilitySecondaryContact) {
        this.facilitySecondaryContact = facilitySecondaryContact;
        return this;
    }

    public Object getFacilityImageLoc() {
        return facilityImageLoc;
    }

    public void setFacilityImageLoc(Object facilityImageLoc) {
        this.facilityImageLoc = facilityImageLoc;
    }

    public DoctorDetails withFacilityImageLoc(Object facilityImageLoc) {
        this.facilityImageLoc = facilityImageLoc;
        return this;
    }

    public String getFacilityAddrDrno() {
        return facilityAddrDrno;
    }

    public void setFacilityAddrDrno(String facilityAddrDrno) {
        this.facilityAddrDrno = facilityAddrDrno;
    }

    public DoctorDetails withFacilityAddrDrno(String facilityAddrDrno) {
        this.facilityAddrDrno = facilityAddrDrno;
        return this;
    }

    public String getFacilityAddrFlrno() {
        return facilityAddrFlrno;
    }

    public void setFacilityAddrFlrno(String facilityAddrFlrno) {
        this.facilityAddrFlrno = facilityAddrFlrno;
    }

    public DoctorDetails withFacilityAddrFlrno(String facilityAddrFlrno) {
        this.facilityAddrFlrno = facilityAddrFlrno;
        return this;
    }

    public String getFacilityAddrRoomno() {
        return facilityAddrRoomno;
    }

    public void setFacilityAddrRoomno(String facilityAddrRoomno) {
        this.facilityAddrRoomno = facilityAddrRoomno;
    }

    public DoctorDetails withFacilityAddrRoomno(String facilityAddrRoomno) {
        this.facilityAddrRoomno = facilityAddrRoomno;
        return this;
    }

    public String getFacilityAddrStreet() {
        return facilityAddrStreet;
    }

    public void setFacilityAddrStreet(String facilityAddrStreet) {
        this.facilityAddrStreet = facilityAddrStreet;
    }

    public DoctorDetails withFacilityAddrStreet(String facilityAddrStreet) {
        this.facilityAddrStreet = facilityAddrStreet;
        return this;
    }

    public String getFacilityAddrCity() {
        return facilityAddrCity;
    }

    public void setFacilityAddrCity(String facilityAddrCity) {
        this.facilityAddrCity = facilityAddrCity;
    }

    public DoctorDetails withFacilityAddrCity(String facilityAddrCity) {
        this.facilityAddrCity = facilityAddrCity;
        return this;
    }

    public Integer getFacilityAddrPostalCode() {
        return facilityAddrPostalCode;
    }

    public void setFacilityAddrPostalCode(Integer facilityAddrPostalCode) {
        this.facilityAddrPostalCode = facilityAddrPostalCode;
    }

    public DoctorDetails withFacilityAddrPostalCode(Integer facilityAddrPostalCode) {
        this.facilityAddrPostalCode = facilityAddrPostalCode;
        return this;
    }

    public String getFacilityAddrState() {
        return facilityAddrState;
    }

    public void setFacilityAddrState(String facilityAddrState) {
        this.facilityAddrState = facilityAddrState;
    }

    public DoctorDetails withFacilityAddrState(String facilityAddrState) {
        this.facilityAddrState = facilityAddrState;
        return this;
    }

    public String getFacilityCountry() {
        return facilityCountry;
    }

    public void setFacilityCountry(String facilityCountry) {
        this.facilityCountry = facilityCountry;
    }

    public DoctorDetails withFacilityCountry(String facilityCountry) {
        this.facilityCountry = facilityCountry;
        return this;
    }

    public Object getFacilityLatitude() {
        return facilityLatitude;
    }

    public void setFacilityLatitude(Object facilityLatitude) {
        this.facilityLatitude = facilityLatitude;
    }

    public DoctorDetails withFacilityLatitude(Object facilityLatitude) {
        this.facilityLatitude = facilityLatitude;
        return this;
    }

    public Object getFacilityLongitude() {
        return facilityLongitude;
    }

    public void setFacilityLongitude(Object facilityLongitude) {
        this.facilityLongitude = facilityLongitude;
    }

    public DoctorDetails withFacilityLongitude(Object facilityLongitude) {
        this.facilityLongitude = facilityLongitude;
        return this;
    }

    public String getFacilityEndTime() {
        return facilityEndTime;
    }

    public void setFacilityEndTime(String facilityEndTime) {
        this.facilityEndTime = facilityEndTime;
    }

    public DoctorDetails withFacilityEndTime(String facilityEndTime) {
        this.facilityEndTime = facilityEndTime;
        return this;
    }

    public String getFacilityStartTime() {
        return facilityStartTime;
    }

    public void setFacilityStartTime(String facilityStartTime) {
        this.facilityStartTime = facilityStartTime;
    }

    public DoctorDetails withFacilityStartTime(String facilityStartTime) {
        this.facilityStartTime = facilityStartTime;
        return this;
    }

    public String getElasticserachDocFacQry() {
        return elasticserachDocFacQry;
    }

    public void setElasticserachDocFacQry(String elasticserachDocFacQry) {
        this.elasticserachDocFacQry = elasticserachDocFacQry;
    }

    public DoctorDetails withElasticserachDocFacQry(String elasticserachDocFacQry) {
        this.elasticserachDocFacQry = elasticserachDocFacQry;
        return this;
    }

    public String getElasticserachCondQry() {
        return elasticserachCondQry;
    }

    public void setElasticserachCondQry(String elasticserachCondQry) {
        this.elasticserachCondQry = elasticserachCondQry;
    }

    public DoctorDetails withElasticserachCondQry(String elasticserachCondQry) {
        this.elasticserachCondQry = elasticserachCondQry;
        return this;
    }

    public String getElasticserachQry() {
        return elasticserachQry;
    }

    public void setElasticserachQry(String elasticserachQry) {
        this.elasticserachQry = elasticserachQry;
    }

    public DoctorDetails withElasticserachQry(String elasticserachQry) {
        this.elasticserachQry = elasticserachQry;
        return this;
    }

}
