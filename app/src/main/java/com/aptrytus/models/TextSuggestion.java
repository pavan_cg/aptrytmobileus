package com.aptrytus.models;

public class TextSuggestion {

    private int suggestionID;
    private String searchQuery;
    private String suggestionTag;

    public int getSuggestionID() {
        return suggestionID;
    }

    public void setSuggestionID(int suggestionID) {
        this.suggestionID = suggestionID;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getSuggestionTag() {
        return suggestionTag;
    }

    public void setSuggestionTag(String suggestionTag) {
        this.suggestionTag = suggestionTag;
    }
}
