package com.aptrytus.models;

import android.os.Parcel;
import android.os.Parcelable;

public class IndividualDrivingLicense implements Parcelable {

    private String addressCity;
    private String addressState;
    private String addressStreet;
    private String addressZip;
    private String birthDate;
    private String documentType;
    private String expiryDate;
    private String firstName;
    private String gender;
    private String maritalStatus;



    private String issueDate;
    private String issuingCountry;
    private String lastName;
    private String licenseNumber;
    private String middleName;

    public IndividualDrivingLicense(Parcel in) {
        addressCity = in.readString();
        addressState = in.readString();
        addressStreet = in.readString();
        addressZip = in.readString();
        birthDate = in.readString();
        documentType = in.readString();
        expiryDate = in.readString();
        firstName = in.readString();
        gender = in.readString();
        issueDate = in.readString();
        issuingCountry = in.readString();
        lastName = in.readString();
        licenseNumber = in.readString();
        middleName = in.readString();
    }

    public static final Creator<IndividualDrivingLicense> CREATOR = new Creator<IndividualDrivingLicense>() {
        @Override
        public IndividualDrivingLicense createFromParcel(Parcel in) {
            return new IndividualDrivingLicense(in);
        }

        @Override
        public IndividualDrivingLicense[] newArray(int size) {
            return new IndividualDrivingLicense[size];
        }
    };

    public IndividualDrivingLicense() {

    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressState() {
        return addressState;
    }

    public void setAddressState(String addressState) {
        this.addressState = addressState;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public String getAddressZip() {
        return addressZip;
    }

    public void setAddressZip(String addressZip) {
        this.addressZip = addressZip;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssuingCountry() {
        return issuingCountry;
    }

    public void setIssuingCountry(String issuingCountry) {
        this.issuingCountry = issuingCountry;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(addressCity);
        dest.writeString(addressState);
        dest.writeString(addressStreet);
        dest.writeString(addressZip);
        dest.writeString(birthDate);
        dest.writeString(documentType);
        dest.writeString(expiryDate);
        dest.writeString(firstName);
        dest.writeString(gender);
        dest.writeString(issueDate);
        dest.writeString(issuingCountry);
        dest.writeString(lastName);
        dest.writeString(licenseNumber);
        dest.writeString(middleName);
    }
}
