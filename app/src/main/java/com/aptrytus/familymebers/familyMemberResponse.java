package com.aptrytus.familymebers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class familyMemberResponse {
    @SerializedName("message")
    @Expose

    private String message;
/*    @SerializedName("familyMemberInfo")
    @Expose
    private List<FamilyMemberInfo> familyMemberInfo = null;
*/

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
/*

    public List<FamilyMemberInfo> getFamilyMemberInfo() {
        return familyMemberInfo;
    }

    public void setFamilyMemberInfo(List<FamilyMemberInfo> familyMemberInfo) {
        this.familyMemberInfo = familyMemberInfo;
    }
*/

}
