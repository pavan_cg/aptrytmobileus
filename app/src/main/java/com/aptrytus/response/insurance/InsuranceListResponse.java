package com.aptrytus.response.insurance;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsuranceListResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("insuranceNamesList")
    @Expose
    private List<InsuranceNamesList> insuranceNamesList = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public InsuranceListResponse withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<InsuranceNamesList> getInsuranceNamesList() {
        return insuranceNamesList;
    }

    public void setInsuranceNamesList(List<InsuranceNamesList> insuranceNamesList) {
        this.insuranceNamesList = insuranceNamesList;
    }

    public InsuranceListResponse withInsuranceNamesList(List<InsuranceNamesList> insuranceNamesList) {
        this.insuranceNamesList = insuranceNamesList;
        return this;
    }

}