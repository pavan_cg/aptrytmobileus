
package com.aptrytus.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateUserResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userInfoResponse")
    @Expose
    private UserInfoResponse userInfoResponse;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CreateUserResponse withMessage(String message) {
        this.message = message;
        return this;
    }

    public UserInfoResponse getUserInfoResponse() {
        return userInfoResponse;
    }

    public void setUserInfoResponse(UserInfoResponse userInfoResponse) {
        this.userInfoResponse = userInfoResponse;
    }

    public CreateUserResponse withUserInfoResponse(UserInfoResponse userInfoResponse) {
        this.userInfoResponse = userInfoResponse;
        return this;
    }

}
