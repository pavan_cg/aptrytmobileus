package com.aptrytus.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendOtpResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("responseObject")
    @Expose
    private ResponseObject responseObject;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SendOtpResponse withMessage(String message) {
        this.message = message;
        return this;
    }

    public ResponseObject getResponseObject() {
        return responseObject;
    }

    public void setResponseObject(ResponseObject responseObject) {
        this.responseObject = responseObject;
    }

    public SendOtpResponse withResponseObject(ResponseObject responseObject) {
        this.responseObject = responseObject;
        return this;
    }

}
