package com.aptrytus.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfoResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("primaryPhone")
    @Expose
    private String primaryPhone;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("maritalStatus")
    @Expose
    private String maritalStatus;
    @SerializedName("idDetails")
    @Expose
    private IdDetails idDetails;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("userRelationShip")
    @Expose
    private String userRelationShip;
    @SerializedName("insuranceDetails")
    @Expose
    private List<InsuranceDetail> insuranceDetails = null;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("userStatus")
    @Expose
    private Boolean userStatus;
    @SerializedName("pcpDetails")
    @Expose
    private PcpDetails pcpDetails;
    @SerializedName("familyMemberdetails")
    @Expose
    private Object familyMemberdetails;
    @SerializedName("primaryUser")
    @Expose
    private Boolean primaryUser;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserInfoResponse withId(String id) {
        this.id = id;
        return this;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public UserInfoResponse withPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public UserInfoResponse withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserInfoResponse withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public UserInfoResponse withMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public UserInfoResponse withMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
        return this;
    }

    public IdDetails getIdDetails() {
        return idDetails;
    }

    public void setIdDetails(IdDetails idDetails) {
        this.idDetails = idDetails;
    }

    public UserInfoResponse withIdDetails(IdDetails idDetails) {
        this.idDetails = idDetails;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public UserInfoResponse withGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public UserInfoResponse withDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public UserInfoResponse withAddress(Address address) {
        this.address = address;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserInfoResponse withEmail(String email) {
        this.email = email;
        return this;
    }

    public String getUserRelationShip() {
        return userRelationShip;
    }

    public void setUserRelationShip(String userRelationShip) {
        this.userRelationShip = userRelationShip;
    }

    public UserInfoResponse withUserRelationShip(String userRelationShip) {
        this.userRelationShip = userRelationShip;
        return this;
    }

    public List<InsuranceDetail> getInsuranceDetails() {
        return insuranceDetails;
    }

    public void setInsuranceDetails(List<InsuranceDetail> insuranceDetails) {
        this.insuranceDetails = insuranceDetails;
    }

    public UserInfoResponse withInsuranceDetails(List<InsuranceDetail> insuranceDetails) {
        this.insuranceDetails = insuranceDetails;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserInfoResponse withUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserInfoResponse withPassword(String password) {
        this.password = password;
        return this;
    }

    public Boolean getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Boolean userStatus) {
        this.userStatus = userStatus;
    }

    public UserInfoResponse withUserStatus(Boolean userStatus) {
        this.userStatus = userStatus;
        return this;
    }

    public PcpDetails getPcpDetails() {
        return pcpDetails;
    }

    public void setPcpDetails(PcpDetails pcpDetails) {
        this.pcpDetails = pcpDetails;
    }

    public UserInfoResponse withPcpDetails(PcpDetails pcpDetails) {
        this.pcpDetails = pcpDetails;
        return this;
    }

    public Object getFamilyMemberdetails() {
        return familyMemberdetails;
    }

    public void setFamilyMemberdetails(Object familyMemberdetails) {
        this.familyMemberdetails = familyMemberdetails;
    }

    public UserInfoResponse withFamilyMemberdetails(Object familyMemberdetails) {
        this.familyMemberdetails = familyMemberdetails;
        return this;
    }

    public Boolean getPrimaryUser() {
        return primaryUser;
    }

    public void setPrimaryUser(Boolean primaryUser) {
        this.primaryUser = primaryUser;
    }

    public UserInfoResponse withPrimaryUser(Boolean primaryUser) {
        this.primaryUser = primaryUser;
        return this;
    }

}
