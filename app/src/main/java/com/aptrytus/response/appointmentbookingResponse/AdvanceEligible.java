
package com.aptrytus.response.appointmentbookingResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class AdvanceEligible {

    @SerializedName("patientAcceptance")
    @Expose
    private Boolean patientAcceptance;
    @SerializedName("advance")
    @Expose
    private Boolean advance;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Boolean getPatientAcceptance() {
        return patientAcceptance;
    }

    public void setPatientAcceptance(Boolean patientAcceptance) {
        this.patientAcceptance = patientAcceptance;
    }

    public Boolean getAdvance() {
        return advance;
    }

    public void setAdvance(Boolean advance) {
        this.advance = advance;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
