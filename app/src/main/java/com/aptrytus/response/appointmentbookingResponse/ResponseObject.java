
package com.aptrytus.response.appointmentbookingResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class ResponseObject {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("userInfoId")
    @Expose
    private String userInfoId;
    @SerializedName("insuranceInfo")
    @Expose
    private InsuranceInfo insuranceInfo;
    @SerializedName("advanceEligible")
    @Expose
    private AdvanceEligible advanceEligible;
    @SerializedName("facilityId")
    @Expose
    private String facilityId;
    @SerializedName("appointment")
    @Expose
    private Appointment appointment;
    @SerializedName("appointmentStatus")
    @Expose
    private AppointmentStatus appointmentStatus;
    @SerializedName("selfPay")
    @Expose
    private Boolean selfPay;
    @SerializedName("newPatient")
    @Expose
    private Boolean newPatient;
    @SerializedName("followUp")
    @Expose
    private Boolean followUp;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(String userInfoId) {
        this.userInfoId = userInfoId;
    }

    public InsuranceInfo getInsuranceInfo() {
        return insuranceInfo;
    }

    public void setInsuranceInfo(InsuranceInfo insuranceInfo) {
        this.insuranceInfo = insuranceInfo;
    }

    public AdvanceEligible getAdvanceEligible() {
        return advanceEligible;
    }

    public void setAdvanceEligible(AdvanceEligible advanceEligible) {
        this.advanceEligible = advanceEligible;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public AppointmentStatus getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public Boolean getSelfPay() {
        return selfPay;
    }

    public void setSelfPay(Boolean selfPay) {
        this.selfPay = selfPay;
    }

    public Boolean getNewPatient() {
        return newPatient;
    }

    public void setNewPatient(Boolean newPatient) {
        this.newPatient = newPatient;
    }

    public Boolean getFollowUp() {
        return followUp;
    }

    public void setFollowUp(Boolean followUp) {
        this.followUp = followUp;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
