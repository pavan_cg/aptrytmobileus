
package com.aptrytus.response.appointmentbookingResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class InsuranceInfo {

    @SerializedName("payerName")
    @Expose
    private String payerName;
    @SerializedName("location")
    @Expose
    private String location;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
