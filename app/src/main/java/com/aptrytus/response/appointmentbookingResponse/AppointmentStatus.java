
package com.aptrytus.response.appointmentbookingResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class AppointmentStatus {

    @SerializedName("currentStatus")
    @Expose
    private Object currentStatus;
    @SerializedName("checkIn")
    @Expose
    private Object checkIn;
    @SerializedName("nurseGreet")
    @Expose
    private Object nurseGreet;
    @SerializedName("doctorGreet")
    @Expose
    private Object doctorGreet;
    @SerializedName("checkOut")
    @Expose
    private Object checkOut;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Object getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(Object currentStatus) {
        this.currentStatus = currentStatus;
    }

    public Object getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Object checkIn) {
        this.checkIn = checkIn;
    }

    public Object getNurseGreet() {
        return nurseGreet;
    }

    public void setNurseGreet(Object nurseGreet) {
        this.nurseGreet = nurseGreet;
    }

    public Object getDoctorGreet() {
        return doctorGreet;
    }

    public void setDoctorGreet(Object doctorGreet) {
        this.doctorGreet = doctorGreet;
    }

    public Object getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Object checkOut) {
        this.checkOut = checkOut;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
