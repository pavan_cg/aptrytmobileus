package com.aptrytus.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorBody {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("resultList")
    @Expose
    private Object resultList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ErrorBody withMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getResultList() {
        return resultList;
    }

    public void setResultList(Object resultList) {
        this.resultList = resultList;
    }

    public ErrorBody withResultList(Object resultList) {
        this.resultList = resultList;
        return this;
    }

}
