
package com.aptrytus.response.familyDetails;

import java.util.HashMap;
import java.util.Map;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class FamilyMemberAddress {

    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("zipcode")
    @Expose
    private Integer zipcode;

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @SerializedName("street")
    public String getStreet() {
        return street;
    }

    @SerializedName("street")
    public void setStreet(String street) {
        this.street = street;
    }

    @SerializedName("city")
    public String getCity() {
        return city;
    }

    @SerializedName("city")
    public void setCity(String city) {
        this.city = city;
    }

    @SerializedName("state")
    public String getState() {
        return state;
    }

    @SerializedName("state")
    public void setState(String state) {
        this.state = state;
    }

    @SerializedName("country")
    public String getCountry() {
        return country;
    }

    @SerializedName("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @SerializedName("zipcode")
    public Integer getZipcode() {
        return zipcode;
    }

    @SerializedName("zipcode")
    public void setZipcode(Integer zipcode) {
        this.zipcode = zipcode;
    }


    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }


    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
