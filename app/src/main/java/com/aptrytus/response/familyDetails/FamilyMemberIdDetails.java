
package com.aptrytus.response.familyDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class FamilyMemberIdDetails {

    @SerializedName("validFromDate")
    @Expose
    private String validFromDate;
    @SerializedName("validToDate")
    @Expose
    private String validToDate;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("idIssuedState")
    @Expose
    private String idIssuedState;
    @SerializedName("number")
    @Expose
    private String number;

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public String getValidFromDate() {
        return validFromDate;
    }


    public void setValidFromDate(String validFromDate) {
        this.validFromDate = validFromDate;
    }


    public String getValidToDate() {
        return validToDate;
    }


    public void setValidToDate(String validToDate) {
        this.validToDate = validToDate;
    }


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public String getIdIssuedState() {
        return idIssuedState;
    }


    public void setIdIssuedState(String idIssuedState) {
        this.idIssuedState = idIssuedState;
    }


    public String getNumber() {
        return number;
    }


    public void setNumber(String number) {
        this.number = number;
    }


    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }


    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
