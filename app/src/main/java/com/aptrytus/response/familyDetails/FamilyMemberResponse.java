
package com.aptrytus.response.familyDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;


public class FamilyMemberResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userInfoResponse")
    @Expose
    private UserInfoResponse userInfoResponse;

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }


    public UserInfoResponse getUserInfoResponse() {
        return userInfoResponse;
    }


    public void setUserInfoResponse(UserInfoResponse userInfoResponse) {
        this.userInfoResponse = userInfoResponse;
    }


    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }


    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
