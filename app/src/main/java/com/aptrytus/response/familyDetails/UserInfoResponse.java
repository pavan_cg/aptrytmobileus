
package com.aptrytus.response.familyDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class UserInfoResponse {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("primaryPhone")
    @Expose
    private String primaryPhone;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("maritalStatus")
    @Expose
    private String maritalStatus;
    @SerializedName("idDetails")
    @Expose
    private IdDetails idDetails;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("userRelationShip")
    @Expose
    private String userRelationShip;
    @SerializedName("insuranceDetails")
    @Expose
    private Object insuranceDetails;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("userStatus")
    @Expose
    private Boolean userStatus;
    @SerializedName("pcpDetails")
    @Expose
    private PcpDetails pcpDetails;
    @SerializedName("familyMemberdetails")
    @Expose
    private List<FamilyMemberdetail> familyMemberdetails = null;
    @SerializedName("primaryUser")
    @Expose
    private Boolean primaryUser;

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getPrimaryPhone() {
        return primaryPhone;
    }


    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getMiddleName() {
        return middleName;
    }


    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }


    public String getMaritalStatus() {
        return maritalStatus;
    }


    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }


    public IdDetails getIdDetails() {
        return idDetails;
    }


    public void setIdDetails(IdDetails idDetails) {
        this.idDetails = idDetails;
    }


    public String getGender() {
        return gender;
    }


    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getDateOfBirth() {
        return dateOfBirth;
    }


    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getUserRelationShip() {
        return userRelationShip;
    }


    public void setUserRelationShip(String userRelationShip) {
        this.userRelationShip = userRelationShip;
    }


    public Object getInsuranceDetails() {
        return insuranceDetails;
    }


    public void setInsuranceDetails(Object insuranceDetails) {
        this.insuranceDetails = insuranceDetails;
    }


    public String getUserId() {
        return userId;
    }


    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public Boolean getUserStatus() {
        return userStatus;
    }


    public void setUserStatus(Boolean userStatus) {
        this.userStatus = userStatus;
    }


    public PcpDetails getPcpDetails() {
        return pcpDetails;
    }


    public void setPcpDetails(PcpDetails pcpDetails) {
        this.pcpDetails = pcpDetails;
    }


    public List<FamilyMemberdetail> getFamilyMemberdetails() {
        return familyMemberdetails;
    }


    public void setFamilyMemberdetails(List<FamilyMemberdetail> familyMemberdetails) {
        this.familyMemberdetails = familyMemberdetails;
    }


    public Boolean getPrimaryUser() {
        return primaryUser;
    }


    public void setPrimaryUser(Boolean primaryUser) {
        this.primaryUser = primaryUser;
    }


    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }


    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
