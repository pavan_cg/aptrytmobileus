
package com.aptrytus.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultList {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("primaryPhone")
    @Expose
    private String primaryPhone;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("maritalStatus")
    @Expose
    private String maritalStatus;
    @SerializedName("driverLicenseNumber")
    @Expose
    private String driverLicenseNumber;
    @SerializedName("driverLicenseValidFromDate")
    @Expose
    private String driverLicenseValidFromDate;
    @SerializedName("driverLicenseValidToDate")
    @Expose
    private String driverLicenseValidToDate;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("family")
    @Expose
    private Object family;
    @SerializedName("insurance")
    @Expose
    private List<Insurance> insurance = null;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("isGuest")
    @Expose
    private String isGuest;
    @SerializedName("otpVerfCode")
    @Expose
    private String otpVerfCode;
    @SerializedName("otpCreateDateTime")
    @Expose
    private String otpCreateDateTime;
    @SerializedName("userStatus")
    @Expose
    private String userStatus;
    @SerializedName("pcp")
    @Expose
    private Pcp pcp;
    @SerializedName("auditInfo")
    @Expose
    private AuditInfo auditInfo;
    @SerializedName("primaryUser")
    @Expose
    private Boolean primaryUser;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ResultList withId(String id) {
        this.id = id;
        return this;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public ResultList withPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public ResultList withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ResultList withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public ResultList withMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public ResultList withMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
        return this;
    }

    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        this.driverLicenseNumber = driverLicenseNumber;
    }

    public ResultList withDriverLicenseNumber(String driverLicenseNumber) {
        this.driverLicenseNumber = driverLicenseNumber;
        return this;
    }

    public String getDriverLicenseValidFromDate() {
        return driverLicenseValidFromDate;
    }

    public void setDriverLicenseValidFromDate(String driverLicenseValidFromDate) {
        this.driverLicenseValidFromDate = driverLicenseValidFromDate;
    }

    public ResultList withDriverLicenseValidFromDate(String driverLicenseValidFromDate) {
        this.driverLicenseValidFromDate = driverLicenseValidFromDate;
        return this;
    }

    public String getDriverLicenseValidToDate() {
        return driverLicenseValidToDate;
    }

    public void setDriverLicenseValidToDate(String driverLicenseValidToDate) {
        this.driverLicenseValidToDate = driverLicenseValidToDate;
    }

    public ResultList withDriverLicenseValidToDate(String driverLicenseValidToDate) {
        this.driverLicenseValidToDate = driverLicenseValidToDate;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ResultList withGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public ResultList withDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ResultList withAddress(Address address) {
        this.address = address;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ResultList withEmail(String email) {
        this.email = email;
        return this;
    }

    public Object getFamily() {
        return family;
    }

    public void setFamily(Object family) {
        this.family = family;
    }

    public ResultList withFamily(Object family) {
        this.family = family;
        return this;
    }

    public List<Insurance> getInsurance() {
        return insurance;
    }

    public void setInsurance(List<Insurance> insurance) {
        this.insurance = insurance;
    }

    public ResultList withInsurance(List<Insurance> insurance) {
        this.insurance = insurance;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ResultList withUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ResultList withPassword(String password) {
        this.password = password;
        return this;
    }

    public String getIsGuest() {
        return isGuest;
    }

    public void setIsGuest(String isGuest) {
        this.isGuest = isGuest;
    }

    public ResultList withIsGuest(String isGuest) {
        this.isGuest = isGuest;
        return this;
    }

    public String getOtpVerfCode() {
        return otpVerfCode;
    }

    public void setOtpVerfCode(String otpVerfCode) {
        this.otpVerfCode = otpVerfCode;
    }

    public ResultList withOtpVerfCode(String otpVerfCode) {
        this.otpVerfCode = otpVerfCode;
        return this;
    }

    public String getOtpCreateDateTime() {
        return otpCreateDateTime;
    }

    public void setOtpCreateDateTime(String otpCreateDateTime) {
        this.otpCreateDateTime = otpCreateDateTime;
    }

    public ResultList withOtpCreateDateTime(String otpCreateDateTime) {
        this.otpCreateDateTime = otpCreateDateTime;
        return this;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public ResultList withUserStatus(String userStatus) {
        this.userStatus = userStatus;
        return this;
    }

    public Pcp getPcp() {
        return pcp;
    }

    public void setPcp(Pcp pcp) {
        this.pcp = pcp;
    }

    public ResultList withPcp(Pcp pcp) {
        this.pcp = pcp;
        return this;
    }

    public AuditInfo getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(AuditInfo auditInfo) {
        this.auditInfo = auditInfo;
    }

    public ResultList withAuditInfo(AuditInfo auditInfo) {
        this.auditInfo = auditInfo;
        return this;
    }

    public Boolean getPrimaryUser() {
        return primaryUser;
    }

    public void setPrimaryUser(Boolean primaryUser) {
        this.primaryUser = primaryUser;
    }

    public ResultList withPrimaryUser(Boolean primaryUser) {
        this.primaryUser = primaryUser;
        return this;
    }

}
