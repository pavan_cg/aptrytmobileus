package com.aptrytus.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseObject {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("otpVerfCode")
    @Expose
    private String otpVerfCode;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("purpose")
    @Expose
    private Object purpose;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ResponseObject withUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ResponseObject withId(String id) {
        this.id = id;
        return this;
    }

    public String getOtpVerfCode() {
        return otpVerfCode;
    }

    public void setOtpVerfCode(String otpVerfCode) {
        this.otpVerfCode = otpVerfCode;
    }

    public ResponseObject withOtpVerfCode(String otpVerfCode) {
        this.otpVerfCode = otpVerfCode;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public ResponseObject withPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public Object getPurpose() {
        return purpose;
    }

    public void setPurpose(Object purpose) {
        this.purpose = purpose;
    }

    public ResponseObject withPurpose(Object purpose) {
        this.purpose = purpose;
        return this;
    }

}
