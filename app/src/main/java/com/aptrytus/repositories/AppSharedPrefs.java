package com.aptrytus.repositories;

import android.content.Context;
import android.content.SharedPreferences;

import com.aptrytus.Others.SharedPreferenceConstants;

public class AppSharedPrefs {
    /**
     * PREFS_NAME is a file name which generates inside data folder of application
     */
    private static final String PREFS_NAME = "aptrytpreferences";

    static SharedPreferences sharedPreferences;
    static SharedPreferences.Editor prefEditor = null;

    private static Context mContext = null;
    public static AppSharedPrefs instance = null;

    public static AppSharedPrefs getInstance(Context context) {
        mContext = context;

        if (instance == null) {
            synchronized (AppSharedPrefs.class) {
                instance = new AppSharedPrefs();
                sharedPreferences = context.getSharedPreferences(SharedPreferenceConstants.APTRY_PREF, Context.MODE_PRIVATE);
                prefEditor = sharedPreferences.edit();
            }
        }
        return instance;
    }

    public void setName(String key, String value) {
        prefEditor.putString(key, value);
        prefEditor.commit();
    }

    public String getName(String key) {
        // if name key available then it will returned value of name otherwise returned empty string.
        return sharedPreferences.getString(key, "");
    }

    public int getInt(String key, int defValue) {
        return sharedPreferences.getInt(key, defValue);
    }

    public int getInt(String key) {
        return getInt(key, 0);
    }

    public void setInt(String key, int value) {
        prefEditor.putInt(key, value);
        prefEditor.commit();
    }

    public long getLong(String key, long defValue) {
        return sharedPreferences.getLong(key, defValue);
    }

    public long getLong(String key) {
        return getLong(key, 0L);
    }

    public void setLong(String key, long value) {
        prefEditor.putLong(key, value);
        prefEditor.commit();
    }

    public boolean getBoolean(String key, boolean defValue) {
        return sharedPreferences.getBoolean(key, defValue);
    }

    public boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public void setBoolean(String key, boolean value) {
        prefEditor.putBoolean(key, value);
        prefEditor.commit();
    }

    public boolean getFloat(String key) {
        return getFloat(key, 0f);
    }

    public boolean getFloat(String key, float defValue) {
        return getFloat(key, defValue);
    }

    public void setFloat(String key, Float value) {
        prefEditor.putFloat(key, value);
        prefEditor.commit();
    }

    public void clearData() {
        prefEditor.clear();
        prefEditor.commit();
    }
}
