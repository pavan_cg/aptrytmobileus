package com.aptrytus.repositories;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;
import android.widget.Toast;

import com.aptrytus.facilities.FacilityResponse;
import com.aptrytus.familyMembersInsurance.FamilyMemberInsuranceResponse;
import com.aptrytus.familymebers.familyMemberResponse;
import com.aptrytus.login.LoginRequest;
import com.aptrytus.login.LoginResponse;
import com.aptrytus.models.DoctorsListResponse;
import com.aptrytus.models.SignUpResponse;
import com.aptrytus.models.TextSuggestionsResponse;
import com.aptrytus.request.CreateUserRequest;
import com.aptrytus.request.SendOtpRequest;
import com.aptrytus.request.appointmentBooking.AppointmentBookingRequest;
import com.aptrytus.response.CreateUserResponse;
import com.aptrytus.response.SendOtpResponse;
import com.aptrytus.response.appointmentbookingResponse.AppointmentBookingResponse;
import com.aptrytus.response.familyDetails.FamilyMemberResponse;
import com.aptrytus.response.imageuploadresp.ImageUploadResponse;
import com.aptrytus.response.insurance.InsuranceListResponse;
import com.aptrytus.services.ApiResponse;
import com.aptrytus.services.AptrytClient;
import com.aptrytus.services.AptrytInterface;
import com.google.android.gms.common.api.Api;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Part;

public class AppRepository {

    static AptrytInterface aptrytInterface = null;
    public static AppRepository instance = null;
    MutableLiveData<ApiResponse> facilitiesListMutableLiveData = new MutableLiveData<ApiResponse>();
    MutableLiveData<ApiResponse> loginMutableLiveData = new MutableLiveData<ApiResponse>();
    MutableLiveData<ApiResponse> textSuggestionsResponseMutableLiveData = new MutableLiveData<>();
    MutableLiveData<ApiResponse> familyMembersMutableLiveData = new MutableLiveData<>();
    MutableLiveData<ApiResponse> familyMembersListMutableLiveData = new MutableLiveData<>();

    MutableLiveData<ApiResponse> imageUploadResponse = new MutableLiveData<>();

    MutableLiveData<ApiResponse> appointmentResponseMutableLiveData = new MutableLiveData<>();


    private AppRepository() {

    }

    public static AppRepository getInstance() {
        if (instance == null) {
            instance = new AppRepository();
            aptrytInterface = AptrytClient.getClient().create(AptrytInterface.class);
        }
        return instance;
    }
    /*public MutableLiveData<TextSuggestionsResponse> getTextSuggestions(String searchText) {

        final MutableLiveData<TextSuggestionsResponse> textSuggestionsResponseMutableLiveData = new MutableLiveData<>();
        aptrytInterface.getSuggestions(searchText).enqueue(new Callback<TextSuggestionsResponse>() {
            @Override
            public void onResponse(Call<TextSuggestionsResponse> call, Response<TextSuggestionsResponse> response) {

                textSuggestionsResponseMutableLiveData.postValue(response.body());

            }

            @Override
            public void onFailure(Call<TextSuggestionsResponse> call, Throwable t) {
                Log.i("Error", "Text Search Service Error");
                //textSuggestionsResponseMutableLiveData.postValue(t.);

            }
        });
        return textSuggestionsResponseMutableLiveData;
    }*/

    public MutableLiveData<ApiResponse> getFacilitiesData() {
        return facilitiesListMutableLiveData;
    }

    public MutableLiveData<ApiResponse> getLoginMutableLiveData() {
        return loginMutableLiveData;
    }

    public MutableLiveData<ApiResponse> getFamilyMembersInsuranceList() {
        return familyMembersMutableLiveData;
    }

    public MutableLiveData<ApiResponse> getFamilyMembersList() {
        return familyMembersListMutableLiveData;
    }

    public MutableLiveData<ApiResponse> getTextSuggestionMutableLiveData() {
        return textSuggestionsResponseMutableLiveData;
    }
    /**
     * to get the text searches when a text is keyed
     *
     * @param searchText
     * @return
     */

    public MutableLiveData<ApiResponse> getTextSuggestions(String searchText, String city, String zipcode) {
        Log.i("PAVAN", aptrytInterface.getSuggestions(searchText, city, zipcode).request().url().toString());

        aptrytInterface.getSuggestions(searchText, city, zipcode).enqueue(new Callback<List<TextSuggestionsResponse>>() {
            @Override
            public void onResponse(Call<List<TextSuggestionsResponse>> call, Response<List<TextSuggestionsResponse>> response) {
                if (response.isSuccessful()) {
                    textSuggestionsResponseMutableLiveData.postValue(new ApiResponse(response.body()));
                } else {
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        //Toast.makeText(getApp(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    textSuggestionsResponseMutableLiveData.postValue(new ApiResponse(jObjError));
                }

            }

            @Override
            public void onFailure(Call<List<TextSuggestionsResponse>> call, Throwable t) {
                textSuggestionsResponseMutableLiveData.postValue(new ApiResponse(t));
            }
        });
        return textSuggestionsResponseMutableLiveData;
    }

    /**
     * to get the List of Doctors for selected type of disease/ dcctor name
     *
     * @param zipcode
     * @param city
     * @param suggestionQuery
     * @return
     */
    public MutableLiveData<ApiResponse> getDoctors(String suggestionQuery, String city, String zipcode) {

        final MutableLiveData<ApiResponse> doctorListMutableLiveData = new MutableLiveData<>();

        /*aptrytInterface.getDoctors(suggestionQuery).enqueue(new Callback<DoctorsListResponse>() {
            @Override
            public void onResponse(Call<DoctorsListResponse> call, Response<DoctorsListResponse> response) {
                if (response.isSuccessful()) {
                    doctorListMutableLiveData.postValue(new ApiResponse(response.body()));
                }
            }

            @Override
            public void onFailure(Call<DoctorsListResponse> call, Throwable t) {
                Log.i("Error","Doctors List Service Error");
                doctorListMutableLiveData.postValue(new ApiResponse(t));
            }
        });*/
        return doctorListMutableLiveData;
    }

    /**
     * to get the List of Doctors for selected type of disease/ dcctor name
     *
     * @param city
     * @param suggestionQuery
     * @param zipcode
     * @return
     */
    public MutableLiveData<ApiResponse> getFacilitiesData(String suggestionQuery, String city, String zipcode) {
        //Log.i("PAVAN :",aptrytInterface.geFacilities(suggestionQuery,city,zipcode).request().url().toString());
        aptrytInterface.geFacilities(suggestionQuery, city, zipcode).enqueue(new Callback<FacilityResponse>() {
            @Override
            public void onResponse(Call<FacilityResponse> call, Response<FacilityResponse> response) {

                //facilitiesListMutableLiveData.postValue(new ApiResponse(response.body()));
                if (response.isSuccessful()) {
                    facilitiesListMutableLiveData.postValue(new ApiResponse(response.body()));
                } else {
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        //Toast.makeText(getApp(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    facilitiesListMutableLiveData.postValue(new ApiResponse(response.body()));
                }
            }

            @Override
            public void onFailure(Call<FacilityResponse> call, Throwable t) {

                facilitiesListMutableLiveData.postValue(new ApiResponse(t));
            }
        });

        return facilitiesListMutableLiveData;
    }

    //  to send Otp
    public MutableLiveData<ApiResponse> getOtp(SendOtpRequest sendOtpRequest) {

        final MutableLiveData<ApiResponse> otpSuccessData = new MutableLiveData<>();

        aptrytInterface.getOtp(sendOtpRequest).enqueue(new Callback<SendOtpResponse>() {
            @Override
            public void onResponse(Call<SendOtpResponse> call, Response<SendOtpResponse> response) {
                if (response.isSuccessful()) {
                    otpSuccessData.postValue(new ApiResponse(response.body()));
                }
            }

            @Override
            public void onFailure(Call<SendOtpResponse> call, Throwable t) {
                otpSuccessData.postValue(new ApiResponse(t));

            }


        });
        return otpSuccessData;
    }

    //  to verify received Otp
    public MutableLiveData<ApiResponse> verifyOtp(SendOtpRequest sendOtpRequest) {

        final MutableLiveData<ApiResponse> verifyOTpSuccessData = new MutableLiveData<>();

        aptrytInterface.verifyOtp(sendOtpRequest).enqueue(new Callback<SendOtpResponse>() {
            @Override
            public void onResponse(Call<SendOtpResponse> call, Response<SendOtpResponse> response) {
                if (response.isSuccessful()) {
                    verifyOTpSuccessData.postValue(new ApiResponse(response.body()));
                }
            }

            @Override
            public void onFailure(Call<SendOtpResponse> call, Throwable t) {
                verifyOTpSuccessData.postValue(new ApiResponse(t));

            }


        });
        return verifyOTpSuccessData;
    }


    //  method to register user

    public MutableLiveData<ApiResponse> registerUser(CreateUserRequest createUserRequest) {

        final MutableLiveData<ApiResponse> createUserResponseData = new MutableLiveData<>();

        aptrytInterface.registerUser(createUserRequest).enqueue(new Callback<CreateUserResponse>() {
            @Override
            public void onResponse(Call<CreateUserResponse> call, Response<CreateUserResponse> response) {
                if (response.isSuccessful()) {
                    createUserResponseData.postValue(new ApiResponse(response.body()));
                } else {
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        //Toast.makeText(getApp(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    createUserResponseData.postValue(new ApiResponse(jObjError));
                }
            }

            @Override
            public void onFailure(Call<CreateUserResponse> call, Throwable t) {
                createUserResponseData.postValue(new ApiResponse(t));

            }


        });
        return createUserResponseData;
    }


    public MutableLiveData<ApiResponse> addFamilyMember(CreateUserRequest createUserRequest, String userId, String relation) {

        final MutableLiveData<ApiResponse> addMemberResponseData = new MutableLiveData<>();

        aptrytInterface.addFamilyMember(createUserRequest, userId, relation).enqueue(new Callback<familyMemberResponse>() {
            @Override
            public void onResponse(Call<familyMemberResponse> call, Response<familyMemberResponse> response) {
                if (response.isSuccessful()) {
                    addMemberResponseData.postValue(new ApiResponse(response.body()));
                } else {
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        //Toast.makeText(getApp(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    addMemberResponseData.postValue(new ApiResponse(jObjError));
                }
            }

            @Override
            public void onFailure(Call<familyMemberResponse> call, Throwable t) {
                addMemberResponseData.postValue(new ApiResponse(t));
            }
        });


        return addMemberResponseData;
    }

    //  method to login user

    public MutableLiveData<ApiResponse> loginUserData(LoginRequest loginRequest) {


        aptrytInterface.loginUserRequest(loginRequest).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.isSuccessful()) {
                    loginMutableLiveData.postValue(new ApiResponse(response.body()));
                } else {
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        //Toast.makeText(getApp(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    loginMutableLiveData.postValue(new ApiResponse(jObjError));
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginMutableLiveData.postValue(new ApiResponse(t));
            }


        });
        return loginMutableLiveData;
    }

    public MutableLiveData<ApiResponse> getInsuranceNames(String searchText) {
        final MutableLiveData<ApiResponse> insuranceMutableLiveData = new MutableLiveData<>();

        aptrytInterface.getInsurances(searchText).enqueue(new Callback<InsuranceListResponse>() {
            @Override
            public void onResponse(Call<InsuranceListResponse> call, Response<InsuranceListResponse> response) {

                insuranceMutableLiveData.postValue(new ApiResponse(response.body()));

            }

            @Override
            public void onFailure(Call<InsuranceListResponse> call, Throwable t) {
                insuranceMutableLiveData.postValue(new ApiResponse(t));
            }


        });
        return insuranceMutableLiveData;
    }


    public MutableLiveData<ApiResponse> getFamilyMemebersList(String email) {
        //final MutableLiveData<ApiResponse> familyMembersMutableLiveData = new MutableLiveData<>();
Log.i("paban","E:"+email);
        aptrytInterface.getFamilyMembers(email).enqueue(new Callback<FamilyMemberInsuranceResponse>() {
            @Override
            public void onResponse(Call<FamilyMemberInsuranceResponse> call, Response<FamilyMemberInsuranceResponse> response) {

                if (response.isSuccessful()) {
                    familyMembersMutableLiveData.postValue(new ApiResponse(response.body()));
                } else {
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        //Toast.makeText(getApp(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    familyMembersMutableLiveData.postValue(new ApiResponse(jObjError));
                }
            }

            @Override
            public void onFailure(Call<FamilyMemberInsuranceResponse> call, Throwable t) {
                familyMembersMutableLiveData.postValue(new ApiResponse(t));
            }
        });

        return familyMembersMutableLiveData;
    }

    /*public MutableLiveData<ApiResponse> getFamiliesData(String email) {
       // final MutableLiveData<ApiResponse> familyMembersListMutableLiveData = new MutableLiveData<>();

        aptrytInterface.getFamilyMembersList(email).enqueue(new Callback<FamilyMemberResponse>() {
            @Override
            public void onResponse(Call<FamilyMemberResponse> call, Response<FamilyMemberResponse> response) {

                if (response.isSuccessful()) {
                    familyMembersListMutableLiveData.postValue(new ApiResponse(response.body()));
                } else {
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        //Toast.makeText(getApp(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    familyMembersListMutableLiveData.postValue(new ApiResponse(jObjError));
                }

            }

            @Override
            public void onFailure(Call<FamilyMemberResponse> call, Throwable t) {

            }
        });

        return familyMembersMutableLiveData;
    }*/



    public MutableLiveData<ApiResponse> uploadImage(MultipartBody.Part file, RequestBody requestBody) {
        //Log.i("PAVAN", aptrytInterface.getSuggestions(searchText, city, zipcode).request().url().toString());

        aptrytInterface.uploadImage(file,requestBody).enqueue(new Callback<ImageUploadResponse>() {
            @Override
            public void onResponse(Call<ImageUploadResponse> call, Response<ImageUploadResponse>     response) {
                imageUploadResponse.postValue(new ApiResponse(response.body()));

            }

            @Override
            public void onFailure(Call<ImageUploadResponse> call, Throwable t) {

            }

        });
        return imageUploadResponse;
    }


    // method to book an appointment

    public MutableLiveData<ApiResponse> bookAppointment(final AppointmentBookingRequest appointmentBookingRequest) {
        //Log.i("PAVAN", aptrytInterface.getSuggestions(searchText, city, zipcode).request().url().toString());

        aptrytInterface.getAppointment(appointmentBookingRequest).enqueue(new Callback<AppointmentBookingResponse>() {
            @Override
            public void onResponse(Call<AppointmentBookingResponse> call, Response<AppointmentBookingResponse> response) {
                appointmentResponseMutableLiveData.setValue(new ApiResponse(response.body()));
            }

            @Override
            public void onFailure(Call<AppointmentBookingResponse> call, Throwable t) {

            }
        });
        return appointmentResponseMutableLiveData;
    }


}
