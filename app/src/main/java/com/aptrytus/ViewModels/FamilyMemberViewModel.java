package com.aptrytus.ViewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import com.aptrytus.models.DoctorDetails;
import com.aptrytus.repositories.AppRepository;
import com.aptrytus.response.familyDetails.FamilyMemberResponse;
import com.aptrytus.services.ApiResponse;

public class FamilyMemberViewModel extends ViewModel{

    private AppRepository appRepository =null;

    private MediatorLiveData<ApiResponse> mApiResponse;

    private MutableLiveData<FamilyMemberResponse> familyMemberResponseMutableLiveData = new MutableLiveData<FamilyMemberResponse>();

    public FamilyMemberViewModel(){
        if(appRepository==null) {
            appRepository = AppRepository.getInstance();
        }
        mApiResponse = new MediatorLiveData<>();
    }

    public LiveData<ApiResponse> getFamilyMebers(String email) {
        if(mApiResponse.hasActiveObservers()) {
            mApiResponse.removeSource(appRepository.getFamilyMembersInsuranceList());
        }
        mApiResponse.addSource(appRepository.getFamilyMemebersList(email), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }

}
