package com.aptrytus.ViewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;
import android.util.Log;

import com.aptrytus.models.TextSuggestionsResponse;
import com.aptrytus.repositories.AppRepository;
import com.aptrytus.services.ApiResponse;

import java.util.List;

public class TextSuggestionModel extends ViewModel {

    private AppRepository appRepository = null;

    //MutableLiveData<TextSuggestionsResponse>  data;

    private MediatorLiveData<ApiResponse> mApiResponse;


    public TextSuggestionModel(){
        if(appRepository == null) {
            appRepository = AppRepository.getInstance();
        }
        mApiResponse = new MediatorLiveData<>();
    }

    /*public LiveData<TextSuggestionsResponse> getTextSuggestions(String searchText) {
        return appRepository.getTextSuggestions(searchText);
    }*/

    public LiveData<ApiResponse> getTextSuggestions(String searchText,String city,String zipcode) {
        if(mApiResponse.hasActiveObservers()) {
            mApiResponse.removeSource(appRepository.getTextSuggestionMutableLiveData());
        }
            mApiResponse.addSource(appRepository.getTextSuggestions(searchText,city,zipcode), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                Log.i("Respnse:",""+apiResponse);
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }

}


