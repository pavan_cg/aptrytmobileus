package com.aptrytus.ViewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import com.aptrytus.repositories.AppRepository;
import com.aptrytus.request.SendOtpRequest;
import com.aptrytus.services.ApiResponse;

public class VerifyOtpModel extends ViewModel {

    private AppRepository appRepository=null;

    private MediatorLiveData<ApiResponse> mApiResponse;

    //private MutableLiveData<SendOtpResponse> signUpSendOtpData = new MutableLiveData<SendOtpResponse>();

    public VerifyOtpModel(){
        if(appRepository==null) {
            appRepository = AppRepository.getInstance();
        }
        mApiResponse = new MediatorLiveData<>();
    }



    public LiveData<ApiResponse> verifyOtp(SendOtpRequest sendOtpRequest) {
        mApiResponse.addSource(appRepository.verifyOtp(sendOtpRequest), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;


    }
}
