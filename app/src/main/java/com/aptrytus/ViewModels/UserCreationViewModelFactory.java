package com.aptrytus.ViewModels;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;

import com.aptrytus.interfaces.UserCreationCallback;

public class UserCreationViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    public UserCreationCallback userCreationCallback;

    public UserCreationViewModelFactory(UserCreationCallback userCreationCallback){
        this.userCreationCallback = userCreationCallback;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new UserCreationViewModel(userCreationCallback);
    }
}
