package com.aptrytus.ViewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import com.aptrytus.repositories.AppRepository;
import com.aptrytus.request.CreateUserRequest;
import com.aptrytus.services.ApiResponse;

public class InsuranceViewModel extends ViewModel {

    private AppRepository appRepository = null;

    private MediatorLiveData<ApiResponse> mApiResponse;


    public InsuranceViewModel(){
        if(appRepository==null) {
            appRepository = AppRepository.getInstance();
        }
        mApiResponse = new MediatorLiveData<>();
    }


    public LiveData<ApiResponse> getInsurances(String insuranceName) {
        mApiResponse.addSource(appRepository.getInsuranceNames(insuranceName), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }
}

