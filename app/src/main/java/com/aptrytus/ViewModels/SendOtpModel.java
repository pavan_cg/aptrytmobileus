package com.aptrytus.ViewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import com.aptrytus.models.SignUpResponse;
import com.aptrytus.repositories.AppRepository;
import com.aptrytus.request.SendOtpRequest;
import com.aptrytus.response.SendOtpResponse;
import com.aptrytus.services.ApiResponse;

public class SendOtpModel extends ViewModel {

    private AppRepository appRepository=null;

    private MediatorLiveData<ApiResponse> mApiResponse;

    //private MutableLiveData<SendOtpResponse> signUpSendOtpData = new MutableLiveData<SendOtpResponse>();

    public SendOtpModel(){
        if(appRepository==null) {
            appRepository = AppRepository.getInstance();
        }
        mApiResponse = new MediatorLiveData<>();
    }

    public LiveData<ApiResponse> getOtp(SendOtpRequest sendOtpRequest) {
        mApiResponse.addSource(appRepository.getOtp(sendOtpRequest), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }

    /*public LiveData<ApiResponse> verifyOtp(SendOtpRequest sendOtpRequest) {
        mApiResponse.addSource(appRepository.verifyOtp(sendOtpRequest), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }*/
}
