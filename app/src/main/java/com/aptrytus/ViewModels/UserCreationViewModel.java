package com.aptrytus.ViewModels;

import android.arch.lifecycle.ViewModel;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.aptrytus.interfaces.UserCreationCallback;
import com.aptrytus.models.databindingmodels.UserDetails;

public class UserCreationViewModel extends ViewModel {

    private UserDetails userDetails;
    private UserCreationCallback userCreationCallback;

    public UserCreationViewModel(UserCreationCallback userCreationCallback){

        this.userCreationCallback = userCreationCallback;
        this.userDetails = new UserDetails();

    }

    public TextWatcher getPcpNameTextWatcher(){
        return  new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                userDetails.setPcpName(s.toString());
            }
        };
    }

    // method to process Login
    public void onSubmitClicked(View view){

        if(userDetails.isValidData()){
            userCreationCallback.onSuccess("Login Successful");
        }else{
            userCreationCallback.onError("Login Error");
        }
    }
}
