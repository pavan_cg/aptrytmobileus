package com.aptrytus.ViewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import com.aptrytus.repositories.AppRepository;
import com.aptrytus.request.CreateUserRequest;
import com.aptrytus.request.appointmentBooking.AppointmentBookingRequest;
import com.aptrytus.services.ApiResponse;

import retrofit2.Call;

public class AppointmentViewModel extends ViewModel{

    private AppRepository appRepository = null;
   // private Call<ApiResponse> mApiResponse;
    private MediatorLiveData<ApiResponse> mApiResponse;

    public AppointmentViewModel(){
        if(appRepository==null) {
            appRepository = AppRepository.getInstance();
        }
        mApiResponse = new MediatorLiveData<>();

    }

    public LiveData<ApiResponse> bookAppointment(AppointmentBookingRequest appointmentBookingRequest) {
        mApiResponse.addSource(appRepository.bookAppointment(appointmentBookingRequest), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }
}
