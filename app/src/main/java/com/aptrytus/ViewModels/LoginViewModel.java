package com.aptrytus.ViewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;
import android.util.Log;

import com.aptrytus.facilities.FacilityResponse;
import com.aptrytus.login.LoginRequest;
import com.aptrytus.login.LoginResponse;
import com.aptrytus.repositories.AppRepository;
import com.aptrytus.request.CreateUserRequest;
import com.aptrytus.services.ApiResponse;

public class LoginViewModel extends ViewModel {

    private AppRepository appRepository = null;

    private MediatorLiveData<ApiResponse> mApiResponse;


    public LoginViewModel(){
        if(appRepository==null) {
            appRepository = AppRepository.getInstance();
        }
        mApiResponse = new MediatorLiveData<>();
    }

    public LoginResponse getLoginDetails(){
        Log.i("Login","appRepository:"+appRepository);
        Log.i("Login","appRepository1:"+appRepository.getLoginMutableLiveData().getValue());
        LoginResponse response = null;
        if(appRepository.getLoginMutableLiveData().getValue()==null){
            return response;
        }else {
             response = appRepository.getLoginMutableLiveData().getValue().loginResponse;
        }
        return response;
    }
    public LiveData<ApiResponse> loginUser(LoginRequest loginRequest) {
        if(mApiResponse.hasActiveObservers()) {
            mApiResponse.removeSource(appRepository.getLoginMutableLiveData());
        }
        mApiResponse.addSource(appRepository.loginUserData(loginRequest), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }
}

