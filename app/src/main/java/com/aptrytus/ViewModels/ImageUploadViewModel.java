package com.aptrytus.ViewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import com.aptrytus.repositories.AppRepository;
import com.aptrytus.request.CreateUserRequest;
import com.aptrytus.services.ApiResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ImageUploadViewModel extends ViewModel {

    private AppRepository appRepository = null;

    private MediatorLiveData<ApiResponse> mApiResponse;


    public ImageUploadViewModel(){
        if(appRepository==null) {
            appRepository = AppRepository.getInstance();
        }
        mApiResponse = new MediatorLiveData<>();
    }


    public LiveData<ApiResponse> uploadImage(MultipartBody.Part file, RequestBody requestBody) {
        mApiResponse.addSource(appRepository.uploadImage(file,requestBody), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }


}
