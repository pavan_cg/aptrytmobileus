package com.aptrytus.ViewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import com.aptrytus.repositories.AppRepository;
import com.aptrytus.request.CreateUserRequest;
import com.aptrytus.services.ApiResponse;

public class CreateUserModel extends ViewModel {

    private AppRepository appRepository = null;

    private MediatorLiveData<ApiResponse> mApiResponse;


    public CreateUserModel(){
        if(appRepository==null) {
            appRepository = AppRepository.getInstance();
        }
        mApiResponse = new MediatorLiveData<>();
    }


    public LiveData<ApiResponse> registerUser(CreateUserRequest createUserRequest) {
        mApiResponse.addSource(appRepository.registerUser(createUserRequest), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }

    public LiveData<ApiResponse> addFamilyMember(CreateUserRequest createUserRequest,String userId,String relation) {
        mApiResponse.addSource(appRepository.addFamilyMember(createUserRequest, userId, relation), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }
}
