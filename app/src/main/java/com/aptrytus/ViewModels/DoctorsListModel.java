package com.aptrytus.ViewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;
import android.util.Log;

import com.aptrytus.facilities.FacilityResponse;
import com.aptrytus.models.DoctorDetails;
import com.aptrytus.repositories.AppRepository;
import com.aptrytus.services.ApiResponse;

public class DoctorsListModel extends ViewModel {

    private AppRepository appRepository =null;

    private MediatorLiveData<ApiResponse> mApiResponse;

    private  MutableLiveData<DoctorDetails> selectedResultList = new MutableLiveData<DoctorDetails>();

    private  MutableLiveData<String> selectedDoctorName = new MutableLiveData<String>();


    public DoctorsListModel(){

        if(appRepository==null){
            appRepository = AppRepository.getInstance();
        }
        mApiResponse = new MediatorLiveData<>();
    }

    public void instantiateRepository(){
        if(appRepository==null){
            appRepository = AppRepository.getInstance();
        }
    }
    public FacilityResponse getFacilitiesDeatilsScreen(){
        FacilityResponse response = appRepository.getFacilitiesData().getValue().facilitiesList;
        return response;
    }
    public LiveData<ApiResponse> getDoctors(String suggestionQuery,String city,String zipcode) {
        mApiResponse.addSource(appRepository.getDoctors(suggestionQuery,city,zipcode), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }

    public LiveData<ApiResponse> getFacilities(String suggestionQuery,String city,String zipcode) {
        if(mApiResponse.hasActiveObservers()) {
            mApiResponse.removeSource(appRepository.getFacilitiesData());
        }
        mApiResponse.addSource(appRepository.getFacilitiesData(suggestionQuery,city,zipcode), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }


    public LiveData<ApiResponse> getFamilyMebersList(String email) {
        if(mApiResponse.hasActiveObservers()) {
            mApiResponse.removeSource(appRepository.getFamilyMembersInsuranceList());
        }
        mApiResponse.addSource(appRepository.getFamilyMemebersList(email), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse)            {
                mApiResponse.setValue(apiResponse);
            }
        });
        return mApiResponse;
    }
    public void select(DoctorDetails item) {
        selectedResultList.setValue(item);
    }
    public MutableLiveData<DoctorDetails> getSelected() {
        return selectedResultList;
    }

    public void select(String item) {
        selectedDoctorName.setValue(item);
    }
    public MutableLiveData<String> getSelectedDoctorName() {
        return selectedDoctorName;
    }
}
