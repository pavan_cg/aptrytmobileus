package com.aptrytus.uiactivities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.aptrytus.R;
import com.aptrytus.fragments.CreatePatientAccount;

public class RegisterUserActivity extends AppCompatActivity {

    int FRAGMENT_FLAG = 5;
    TextView toolBarText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_popup_activity);

        toolBarText = findViewById(R.id.toolbar_title);

        FRAGMENT_FLAG = getIntent().getExtras().getInt("FRAGMENT_FLAG");

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        CreatePatientAccount createPatientAccount = CreatePatientAccount.newInstance(1,"Patient Registration");

        /*LoginFragment loginFragment = LoginFragment.newInstance(1,"Login");
        CreateAccountFragment createAccountFragment = CreateAccountFragment.newInstance();*/

        if(FRAGMENT_FLAG == 0){
            ft.add(R.id.fragment_layout, createPatientAccount);
            toolBarText.setText(getResources().getText(R.string.title_create_patient));
        }else{
            /*ft.add(R.id.fragment_layout, createAccountFragment);
            toolBarText.setText(getResources().getText(R.string.title_create_account));*/
        }

        ft.commit();

    }

    public void closePopup(View view){
        finish();
    }
}

