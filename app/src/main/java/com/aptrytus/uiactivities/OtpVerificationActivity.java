package com.aptrytus.uiactivities;

import android.Manifest;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aptrytus.Others.Utility;
import com.aptrytus.R;
import com.aptrytus.ViewModels.CreateUserModel;
import com.aptrytus.ViewModels.SendOtpModel;
import com.aptrytus.ViewModels.VerifyOtpModel;
import com.aptrytus.interfaces.MessageListener;
import com.aptrytus.receivers.SMSListener;
import com.aptrytus.request.SendOtpRequest;
import com.aptrytus.services.ApiResponse;

import java.util.ArrayList;
import java.util.List;

public class OtpVerificationActivity extends AppCompatActivity implements View.OnClickListener{

    private Toolbar toolbar;
    ProgressDialog progressDialog;

    SendOtpModel sendOtpModel;
    VerifyOtpModel verfifyOtpModel;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    String id = "";
    String userId = "";
    String phoneNum = "";
    String otpReceivedText = "";

    EditText otpText;

    IntentFilter mIntentFilter;

    private static final String TAG = "OtpVerification Actvity";
    private static final String PREF_USER_MOBILE_PHONE = "pref_user_mobile_phone";
    private static final int SMS_PERMISSION_CODE = 0;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_otp_verification);

        Bundle bundle = getIntent().getExtras();

        id = bundle.get("id").toString();
        userId = bundle.get("userId").toString();
        phoneNum = bundle.get("phoneNum").toString();
        //id = bundle.get("id").toString();

        toolbar =  findViewById(R.id.toolbar_dr_details);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ActionBar actionBar = this.getSupportActionBar();

        actionBar.setTitle("SMS Verification");

        otpText =  (EditText) findViewById(R.id.otpReceivedEdit);

        sendOtpModel = ViewModelProviders.of(this).get(SendOtpModel.class);
        verfifyOtpModel =ViewModelProviders.of(this).get(VerifyOtpModel.class);


        // sms auto read disabled

       /* if (!hasReadSmsPermission()) {
            showRequestPermissionsInfoAlertDialog();
        }else{
            getOTP();
        }

        //SMSListener.bindListener(this);*/

        getOTP();

       //

    }

    /**
     * Runtime permission shenanigans
     */
    private boolean hasReadSmsPermission() {
        return ContextCompat.checkSelfPermission(OtpVerificationActivity.this,
                Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(OtpVerificationActivity.this,
                        Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Optional informative alert dialog to explain the user why the app needs the Read/Send SMS permission
     */
    private void showRequestPermissionsInfoAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("SMS Permission");
        builder.setMessage("Need SMS Permission");
        builder.setPositiveButton("GRANT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                requestReadAndSendSmsPermission();
            }
        });
        builder.show();
    }

    private void requestReadAndSendSmsPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(OtpVerificationActivity.this, Manifest.permission.READ_SMS)) {
            Log.d(TAG, "shouldShowRequestPermissionRationale(), no permission requested");
            return;
        }
        ActivityCompat.requestPermissions(OtpVerificationActivity.this, new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS},
                SMS_PERMISSION_CODE);
    }

    /*private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");


                otpText.setText(message);
                Toast.makeText(getApplicationContext(), "asd"+message, Toast.LENGTH_SHORT).show();
            }

        }


    };*/

    @Override
    public void onPause() {
        super.onPause();
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onClick(View v) {

    }

    public void verifyOTP(){

        otpReceivedText = otpText.getText().toString();

        SendOtpRequest sendOtpRequest = new SendOtpRequest();
        sendOtpRequest.setId(id);
        sendOtpRequest.setUserId(userId);
        sendOtpRequest.setPhoneNumber(phoneNum);
        sendOtpRequest.setOtpVerfCode(otpReceivedText);

        verfifyOtpModel.verifyOtp(sendOtpRequest).observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {

                if(apiResponse.getSendOtpResponse() != null){

                    String msg = apiResponse.getSendOtpResponse().getMessage();

                    displayVerifyMessage(msg);




                    /*Log.i("Response received ",apiResponse.createUserResponse.getMessage().toString());*/
                }else {
                    Toast.makeText(getApplicationContext(), "Error received "+apiResponse.createUserResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

                progressDialog.dismiss();
            }
        });

    }

    /**
     * to verfiy the otp received
     * @param view
     */

    public void verifyOtpClicked(View view){

        verifyOTP();

    }

    public void resendOTP(View view){
        getOTP();
    }

    /**
     * method to make a OTP request
     */

    public void getOTP(){

        progressDialog = Utility.getProgressDialog(this, "Please wait...");
        progressDialog.show();

        SendOtpRequest sendOtpRequest = new SendOtpRequest();
        sendOtpRequest.setId(id);
        sendOtpRequest.setUserId(userId);
        sendOtpRequest.setPhoneNumber(phoneNum);

        sendOtpModel.getOtp(sendOtpRequest).observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {

                if(apiResponse.getSendOtpResponse() != null){

                    String msg = apiResponse.getSendOtpResponse().getMessage();
                    //Toast.makeText(getApplicationContext(), " "+msg, Toast.LENGTH_SHORT).show();
                    /*Log.i("Response received ",apiResponse.createUserResponse.getMessage().toString());*/
                }else {
                    Toast.makeText(getApplicationContext(), "Error received "+apiResponse.createUserResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

                progressDialog.dismiss();
            }
        });

    }

    private  boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);
        int receiveSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
        int readSMS = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        /*if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_MMS);
            //ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.RECEIVE_MMS},0);
        }*/
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        /*if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }*/
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

   /* @Override
    public void messageReceived(String message) {
        Toast.makeText(this, "New Message Received: " + message, Toast.LENGTH_SHORT).show();
        otpText.setText(message);

        verifyOTP();
    }*/

    public void displayVerifyMessage(String msg){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Account Created");
        builder.setMessage(msg);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Toast.makeText(getApplicationContext(), " "+msg, Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.show();

    }
}
