package com.aptrytus.uiactivities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aptrytus.Others.SharedPreferenceConstants;
import com.aptrytus.Others.Utility;
import com.aptrytus.R;
import com.aptrytus.ViewModels.CreateUserModel;
import com.aptrytus.ViewModels.DoctorsListModel;
import com.aptrytus.ViewModels.ImageUploadViewModel;
import com.aptrytus.familyMembersInsurance.FamilyMemberInsuranceResponse;
import com.aptrytus.login.LoginResponse;
import com.aptrytus.login.ResponseObject;
import com.aptrytus.models.IndividualDrivingLicense;
import com.aptrytus.repositories.AppSharedPrefs;
import com.aptrytus.request.CreateUserRequest;
import com.aptrytus.request.IdDetails;
import com.aptrytus.request.Insurance;
import com.aptrytus.services.ApiResponse;
import com.aptrytus.services.AppURLS;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.notbytes.barcode_reader.BarcodeReaderActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddInsuranceActivity extends AppCompatActivity {

    private Toolbar toolbar;

    ProgressDialog progressDialog;
    CreateUserModel createUserModel;

    ImageUploadViewModel imageUploadViewModel;

    //TextView selectedInsurance;
    EditText insuranceName;
    //Spinner insuranceStatusSpinner;
    Spinner insuranceHolderTypeSpinner;
    Button addInsuranceBtn;

    ArrayList<Insurance> insuranceList = new ArrayList<>();

    public static final int REQUEST_IMAGE = 100;
    public static final int REQUEST_PERMISSION = 200;

    private String imageFilePath = "";

    private String frontImagePath = "";
    private String backImagePath = "";

    private String mergedImagePath = "";

    private File frontImageFile;
    private File backImageFile;
    private File mergedImage;

    int imageFlag = 0;

    String userId = "";



    ImageView frontImageView;
    ImageView backImageView;

    int MAX_IMAGE_SIZE = 1000;

    int fromScreen = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_insurance_addition);

        toolbar = findViewById(R.id.toolbar_dr_details);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ActionBar actionBar = this.getSupportActionBar();

        actionBar.setTitle("Insurance Info");

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //selectedInsurance = findViewById(R.id.selectedInsurance);
        //insuranceStatusSpinner = findViewById(R.id.insuranceStatus);

        insuranceName = findViewById(R.id.insuranceName);
        insuranceHolderTypeSpinner = findViewById(R.id.insuranceHolderType);
        addInsuranceBtn = findViewById(R.id.addInsuranceBtn);

        // images

        frontImageView  = (ImageView) findViewById(R.id.frontImage);
        backImageView  = (ImageView) findViewById(R.id.backImage);

        //  to store the image captured in the device
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION);
        }

        imageUploadViewModel = ViewModelProviders.of(this).get(ImageUploadViewModel.class);
        userId = AppSharedPrefs.getInstance(getApplicationContext()).getName(SharedPreferenceConstants.LOGEED_IN_USER_ID);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


   /* public void selectInsurances(View view) {

        if (!insuranceName.getText().toString().trim().equals("")) {


            Insurance insurance = new Insurance();
            insurance.setInsuranceName(insuranceName.getText().toString());
            insurance.setPriority(0);

            insuranceList.add(insurance);

            String txt = "";
            for (int i = 0; i < insuranceList.size(); i++) {
                String txt2;
                txt2 = insuranceList.get(i).getInsuranceName();
                txt = txt.concat(txt2);
            }

            if (insuranceList.size() > 0) {
                addInsuranceBtn.setEnabled(true);
            }

            selectedInsurance.setText(txt);
        } else {
            Toast.makeText(getBaseContext(), "Please update Insurance", Toast.LENGTH_LONG).show();
        }


    }*/

    public void addInsurance(View view) {

        Insurance insurance = new Insurance();
        insurance.setInsuranceName(insuranceName.getText().toString());
        insurance.setPriority(0);

        insuranceList.add(insurance);

        if(insuranceList.size()>0) {

            Intent intent = new Intent();
            intent.putExtra("insurance", insuranceList);
            setResult(RESULT_OK, intent);
            uploadImagesToServer();
            //finish();
        }else{
            Toast.makeText(this,"Please select insurance ",Toast.LENGTH_LONG).show();
        }



    }

    public void getInsuranceList(View view) {

        Intent searchIntent = new Intent(AddInsuranceActivity.this, SearchListActivity.class);
        startActivityForResult(searchIntent, 5);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == RESULT_OK) {
                if(imageFlag == 0){
                    frontImageView.setImageURI(Uri.parse(imageFilePath));
                    frontImageView.setRotation(0);
                }else{
                    backImageView.setImageURI(Uri.parse(imageFilePath));
                    backImageView.setRotation(0);
                }

            }
            else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "You cancelled the operation", Toast.LENGTH_SHORT).show();
            }
        }else if (requestCode == 5 && data != null) {

            String insuraneSelected = data.getStringExtra("insuranceName");
            insuranceName.setText(insuraneSelected);
        }else if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(this, "error fetching data", Toast.LENGTH_SHORT).show();
            return;
        }

    }

    public void captureFrontImage(View view){
        imageFlag = 0;
        openCameraIntent();
    }

    public void captureBackImage(View view){
        imageFlag = 1;
        openCameraIntent();
    }

    private void openCameraIntent() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            }
            catch (IOException e) {
                e.printStackTrace();
                return;
            }
            Uri photoUri = FileProvider.getUriForFile(this, "com.aptrytus.provider", photoFile);
            pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            startActivityForResult(pictureIntent, REQUEST_IMAGE);
        }
    }

    private File createImageFile() throws IOException{

        String imageName = "BACK";
        if(imageFlag == 0){
            imageName = "FRONT";
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = imageName+"IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        imageFilePath = image.getAbsolutePath();

        Log.i("File Name of captured->",imageFilePath);

        if(imageFlag == 0){
            frontImagePath = imageFilePath;
            frontImageFile = image;
        }else{
            backImagePath = imageFilePath;
            backImageFile = image;
        }

        return image;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Thanks for granting Permission", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void uploadImagesToServer(){
        // Create a request body with file and image media type

        //addImages();

       /* RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), mergedImagePath);
        MultipartBody.Part part = MultipartBody.Part.createFormData("insuranceCard", mergedImage.getName(), fileReqBody);*/

       if(frontImageFile != null && backImageFile != null && mergedImage != null) {

           RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), mergedImage);
           MultipartBody.Part part = MultipartBody.Part.createFormData("insuranceCard", mergedImage.getName(), fileReqBody);

           progressDialog = Utility.getProgressDialog(this, "Please wait...");
           RequestBody description = RequestBody.create(MediaType.parse("text/plain"), userId);
           //RequestBody description = RequestBody.create(MediaType.parse("text/plain"), "image-type");
           progressDialog.show();
           imageUploadViewModel.uploadImage(part, description).observe(this, new Observer<ApiResponse>() {
               @Override
               public void onChanged(@Nullable ApiResponse apiResponse) {
                   progressDialog.dismiss();

                   if (apiResponse.imageUploadResponse != null) {
                       String message = apiResponse.imageUploadResponse.getStatus();
                       if (message.equalsIgnoreCase("success")) {
                           Toast.makeText(getBaseContext(), " " + message, Toast.LENGTH_SHORT).show();
                           if(fromScreen == 0){
                               finish();
                           }else{
                               addInsuranceToUser();
                           }

                       } else {
                           Toast.makeText(getBaseContext(), " " + message, Toast.LENGTH_SHORT).show();
                       }
                   } else {
                       String message = Utility.getApiError(apiResponse);
                       Toast.makeText(getApplicationContext(), "Error received " + message, Toast.LENGTH_SHORT).show();
                   }
               /* if (apiResponse.familyMemberInsuranceResponse != null) {
                    FamilyMemberInsuranceResponse response = apiResponse.familyMemberInsuranceResponse;
                    displayFamilyList(response);
                } else {
                    String message = Utility.getApiError(apiResponse);
                    Toast.makeText(getApplicationContext(), "Error received " + message, Toast.LENGTH_SHORT).show();
                }*/
               }
           });

       }else{
               Toast.makeText(getApplicationContext(), "Please capture required images" , Toast.LENGTH_SHORT).show();

       }
    }

    //  to merge two bit map images

    private Bitmap createSingleImageFromMultipleImages(Bitmap firstImage, Bitmap secondImage){

        //Bitmap result = Bitmap.createBitmap(600, 1024, firstImage.getConfig());
        int w = firstImage.getWidth();
        int h = firstImage.getHeight();

        Bitmap result = Bitmap.createBitmap(w, h+h, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(firstImage, 0f, 0f, null);
        canvas.drawBitmap(secondImage, 10, h, null);
        //canvas.drawBitmap(secondImage, new Matrix(), null);
        return result;
    }

    private File saveMergedImages(Bitmap image){

        /*String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();*/

        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fname = "mergedImage";

        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File imageFile = null;
        try {
            imageFile = File.createTempFile(fname, ".jpg", storageDir);
            //imageFilePath = file.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            image.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }


        //File file = new File(myDir, fname);



        Log.i("Merged Filename ->",mergedImagePath);

        addInsuranceBtn.setEnabled(true);

        return imageFile;
    }



    public void addImages(View view){

        /*Bitmap frontImageBitmap = BitmapFactory.decodeFile(frontImagePath);
        Bitmap backImageBitmap = BitmapFactory.decodeFile(backImagePath);*/

        Bitmap frontImageBitmap = scaleDown( BitmapFactory.decodeFile(frontImagePath),MAX_IMAGE_SIZE, true);
        Bitmap backImageBitmap = scaleDown( BitmapFactory.decodeFile(backImagePath),MAX_IMAGE_SIZE, true);

        Bitmap image = createSingleImageFromMultipleImages(frontImageBitmap,backImageBitmap);

        mergedImage = saveMergedImages(image);

        frontImageView.setImageBitmap(image);
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    public void addInsuranceToUser(){
        //  finish will be called only after successfully adding insurance.
    }

    public void registerUserWithoutInsurance() {

        CreateUserRequest createUserRequest = new CreateUserRequest();
       // createUserRequest = createUserRequestObject(createUserRequest);

        progressDialog = Utility.getProgressDialog(this, "Please wait...");
        progressDialog.show();
        createUserModel.registerUser(createUserRequest).observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {

                if (apiResponse.createUserResponse != null) {

                    String msg = apiResponse.createUserResponse.getMessage();
                    Toast.makeText(getApplicationContext(), " " + msg, Toast.LENGTH_SHORT).show();

                    finish();
                    /*Log.i("Response received ",apiResponse.createUserResponse.getMessage().toString());*/
                } else {
                    String message = Utility.getApiError(apiResponse);
                    Toast.makeText(getApplicationContext(), "Error received " + message, Toast.LENGTH_SHORT).show();
                }

                progressDialog.dismiss();
            }
        });

    }

    /*public CreateUserRequest createUserRequestObject(CreateUserRequest createUserRequest) {

        ResponseObject loginResp = Utility.loginResponse.getResponseObject();

        createUserRequest.setFirstName(loginResp.getFirstName());
        createUserRequest.setLastName(loginResp.getLastName());
        createUserRequest.setMiddleName(loginResp.getMiddleName());
        if (loginResp.getMaritalStatus() == null) {
            createUserRequest.setMaritalStatus("Single");
        } else {
            createUserRequest.setMaritalStatus(loginResp.getMaritalStatus());
        }

        IdDetails idDetails = new IdDetails();

        idDetails.setNumber(loginResp.getDriverLicenseNumber());
        //idDetails.setType(drivingLicense.getDocumentType);
        idDetails.setType("DL");

        if (loginResp.getDriverLicenseValidFromDate() != null) {
            idDetails.setIdIssuedState(Utility.getFormatedDate(loginResp.getDriverLicenseValidFromDate()));
            idDetails.setValidFromDate(Utility.getFormatedDate(loginResp.getDriverLicenseValidFromDate()));
           // idDetails.setValidToDate(Utility.getFormatedDate(drivingLicense.getExpiryDate()));
        } else {
            idDetails.setIdIssuedState("");
            idDetails.setValidFromDate("");
            idDetails.setValidToDate("");
        }


        createUserRequest.setIdDetails(idDetails);

        String genderVal = "M";
        if (loginResp.getGender().equals("2")) {
            genderVal = "F";
        }

        createUserRequest.setGender(genderVal);
        createUserRequest.setDateOfBirth(Utility.getFormatedDate(loginResp.getDateOfBirth()));

        createUserRequest.setEmail(email.getText().toString());
        if (!AppSharedPrefs.getInstance(getApplicationContext()).getBoolean(SharedPreferenceConstants.SCAN_FROM_SETTINGS)) {
            String phone = Utility.formatUSPhoneNum(mobileNo.getText().toString().trim());
            createUserRequest.setPrimaryPhone(phone);
            createUserRequest.setUserId(email.getText().toString());
            createUserRequest.setPassword(password.getText().toString());
        }


        // address
        Address address = new Address();
        address.setStreetAddress(drivingLicense.getAddressStreet());
        address.setCity(drivingLicense.getAddressCity());
        address.setState(drivingLicense.getAddressState());
        address.setZipcode(drivingLicense.getAddressZip());
        address.setCountry(drivingLicense.getIssuingCountry());

        createUserRequest.setAddress(address);

        //  Insurance
        if (insuranceList != null) {
            if (insuranceList.size() > 0) {

                createUserRequest.setInsuranceDetails(insuranceList);

            }
        }

        // add pcp details

        /*Pcp pcp = new Pcp();
        pcp.setName(pcpName.getText().toString());
        pcp.setPhone(pcpContact.getText().toString());

        PcpDetails pcp = new PcpDetails();
        pcp.setName(pcpName.getText().toString());
        pcp.setContact(pcpContact.getText().toString());
        createUserRequest.setPcpDetails(pcp);

        if (AppSharedPrefs.getInstance(getApplicationContext()).getBoolean(SharedPreferenceConstants.SCAN_FROM_SETTINGS)) {
            createUserRequest.setUserRelationShip(AppSharedPrefs.getInstance(getApplicationContext()).getName(SharedPreferenceConstants.SELECTED_FAMILY_MEMBERS));
            createUserRequest.setPrimaryPhone(AppSharedPrefs.getInstance(getApplicationContext()).getName(SharedPreferenceConstants.LOGEED_IN_USER_MOBILE));

        } else {
            createUserRequest.setUserRelationShip("Self");
        }
        createUserRequest.setId("");

        return createUserRequest;
    }*/

}
