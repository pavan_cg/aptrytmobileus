package com.aptrytus.uiactivities;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.aptrytus.R;
import com.aptrytus.ViewModels.DoctorsListModel;
import com.aptrytus.ViewModels.InsuranceViewModel;
import com.aptrytus.adapters.SearchAdapter;
import com.aptrytus.facilities.FacilityResponse;
import com.aptrytus.response.insurance.InsuranceListResponse;
import com.aptrytus.response.insurance.InsuranceNamesList;
import com.aptrytus.services.ApiResponse;

import java.util.ArrayList;
import java.util.List;

public class SearchListActivity extends AppCompatActivity{

    RecyclerView recyclerView;
    EditText editTextSearch;
    ArrayList<String> names;

    SearchAdapter adapter;
    InsuranceViewModel insuranceViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.search_list_layout);

        names = new ArrayList<>();

        /*names.add("Ramiz");
        names.add("Belal");
        names.add("Azad");
        names.add("Manish");
        names.add("Sunny");
        names.add("Shahid");
        names.add("Deepak");
        names.add("Deepika");
        names.add("Sumit");
        names.add("Mehtab");
        names.add("Vivek");*/

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        editTextSearch = (EditText) findViewById(R.id.editTextSearch);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new SearchAdapter(this,names);

        recyclerView.setAdapter(adapter);


        //adding a TextChangedListener
        //to call a method whenever there is some change on the EditText
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });

        insuranceViewModel = ViewModelProviders.of(this).get(InsuranceViewModel.class);

    }

    private void filter(String text) {
        //new array list that will hold the filtered data


        /*//looping through existing elements
        for (String s : names) {
            //if the existing elements contains the search input
            if (s.toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }*/

        insuranceViewModel.getInsurances(text).observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                setInsuranceList(apiResponse.insuranceListResponse);
                //progressDialog.dismiss();
            }
        });

        //calling a method of the adapter class and passing the filtered list

    }

    private void setInsuranceList(InsuranceListResponse insuranceList) {

        if(insuranceList!=null) {
            ArrayList<String> filterdNames = new ArrayList<>();
            List<InsuranceNamesList> listOfInsurance = insuranceList.getInsuranceNamesList();

            for (InsuranceNamesList insurance : listOfInsurance) {
                filterdNames.add(insurance.getName());
            }

            adapter.filterList(filterdNames);
        }
    }

    /*public void filterList(ArrayList<String> filterdNames) {
        this.names = filterdNames;
        notifyDataSetChanged();
    }*/

    public void sendSelectedData(String name){

        Intent intent = new Intent();
        intent.putExtra("insuranceName",name);
        setResult(RESULT_OK,intent);
        finish();

    }
}
