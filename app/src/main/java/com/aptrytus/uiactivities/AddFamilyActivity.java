package com.aptrytus.uiactivities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.aptrytus.Others.SharedPreferenceConstants;
import com.aptrytus.Others.Utility;
import com.aptrytus.R;
import com.aptrytus.ViewModels.DoctorsListModel;
import com.aptrytus.adapters.DoctotsListAdapter;
import com.aptrytus.adapters.FamilyMemberListAdapter;
import com.aptrytus.facilities.FacilityResponse;
import com.aptrytus.familyMembersInsurance.FamilyMemberInsuranceResponse;
import com.aptrytus.familyMembersInsurance.FamilyMemberdetail;
import com.aptrytus.familymebers.familyMemberResponse;
import com.aptrytus.models.IndividualDrivingLicense;
import com.aptrytus.repositories.AppSharedPrefs;
import com.aptrytus.services.ApiResponse;
import com.aptrytus.services.AptrytClient;
import com.aptrytus.services.AptrytInterface;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddFamilyActivity extends AppCompatActivity implements FamilyMemberListAdapter.OnItemClickListener{


    @BindView(R.id.familyMemberSpinner)
    Spinner familyMemders;

    @BindView(R.id.addfmly_continue)
    Button continueBtn;

    @BindView(R.id.displayFamilyList)
    RecyclerView recyclerView;

    Toolbar toolbar;
    DoctorsListModel doctorViewModel;
    ProgressDialog progressDialog;

    // Add a different request code for every activity you are starting from here
    private static final int ACTIVITY_REQUEST_CODE = 0;

    String userEmailId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_family);
        ButterKnife.bind(this);

        toolbar =  findViewById(R.id.toolbar_add_family);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ActionBar actionBar = this.getSupportActionBar();

        actionBar.setTitle("Family Tree");

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppSharedPrefs.getInstance(getApplicationContext()).setBoolean(SharedPreferenceConstants.SCAN_FROM_SETTINGS,true);
                AppSharedPrefs.getInstance(getApplicationContext()).setName(SharedPreferenceConstants.SELECTED_FAMILY_MEMBERS,familyMemders.getSelectedItem().toString());
                Intent intent = new Intent(getApplicationContext(),UserCreationActivity.class);
               // startActivity(intent);
                startActivityForResult(intent, ACTIVITY_REQUEST_CODE);

            }
        });

        doctorViewModel = ViewModelProviders.of(this).get(DoctorsListModel.class);
         userEmailId = AppSharedPrefs.getInstance(getApplicationContext()).getName(SharedPreferenceConstants.LOGEED_IN_USER_EMAIL_ID);

        if (userEmailId != null)
            displayFamilyDetailsList(userEmailId);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void displayFamilyDetailsList(String userEmailId) {
        progressDialog = Utility.getProgressDialog(this, "Please wait...");
        progressDialog.show();


        doctorViewModel.getFamilyMebersList(userEmailId).observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                progressDialog.dismiss();
                if (apiResponse.familyMemberInsuranceResponse != null) {
                    FamilyMemberInsuranceResponse response = apiResponse.familyMemberInsuranceResponse;
                    displayFamilyList(response);
                } else {
                    String message = Utility.getApiError(apiResponse);
                    Toast.makeText(getApplicationContext(), "Error received " + message, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void displayFamilyList(FamilyMemberInsuranceResponse familyMemberInsuranceResponse) {
        if (familyMemberInsuranceResponse != null) {
                FamilyMemberListAdapter adapter = new FamilyMemberListAdapter(familyMemberInsuranceResponse,this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(adapter);
            }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ACTIVITY_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK){
                if (userEmailId != null)
                    displayFamilyDetailsList(userEmailId);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult


    public void deleteFamilyMember(String childId,String parentId){
        AptrytInterface  aptrytInterface = AptrytClient.getClient().create(AptrytInterface.class);
        progressDialog = Utility.getProgressDialog(this, "Deleteing user...");
        progressDialog.show();

        aptrytInterface.deleteFamilyMembers(childId,parentId).enqueue(new Callback<familyMemberResponse>() {
            @Override
            public void onResponse(Call<familyMemberResponse> call, Response<familyMemberResponse> response) {
                progressDialog.dismiss();
                displayFamilyDetailsList(userEmailId);
                if (response.isSuccessful()) {
                    familyMemberResponse familyMemberResponse = response.body();
                    Toast.makeText(getApplicationContext(), "" + familyMemberResponse.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    JSONObject jObjError = null;
                    try {
                        jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getApplicationContext(), "" + response.message().toString(), Toast.LENGTH_LONG).show();
                        //Toast.makeText(getApp(), jObjError.FamilyMemberListAdapter("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        // Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    //familyMembersMutableLiveData.postValue(new ApiResponse(jObjError));

                }
            }

            @Override
            public void onFailure(Call<familyMemberResponse> call, Throwable t) {

            }
        });

    }

    private void deleteDialog(final String childId, final String parentId) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to remove family memeber ?")
                .setCancelable(false)

                .setPositiveButton("DELETE",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                deleteFamilyMember(childId,parentId);
                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onItemClick(String childId, String parentId) {
        deleteDialog(childId,parentId);
    }

    @Override
    public void onItemClickEdit(String memberId, String parentId, FamilyMemberdetail familyMemberdetail) {
        IndividualDrivingLicense drivingLicense = new IndividualDrivingLicense();
        drivingLicense.setAddressCity(familyMemberdetail.getAddress().getCity());

        drivingLicense.setAddressState(familyMemberdetail.getAddress().getState());
        drivingLicense.setAddressStreet(familyMemberdetail.getAddress().getStreet());
        drivingLicense.setAddressZip(""+familyMemberdetail.getAddress().getZipcode());
        drivingLicense.setBirthDate(familyMemberdetail.getDateOfBirth());
        drivingLicense.setDocumentType(familyMemberdetail.getIdDetails().getType());
        drivingLicense.setExpiryDate(familyMemberdetail.getIdDetails().getValidToDate());
        drivingLicense.setFirstName(familyMemberdetail.getFirstName());
        drivingLicense.setGender(familyMemberdetail.getGender());
        drivingLicense.setIssueDate(familyMemberdetail.getIdDetails().getValidFromDate());
        drivingLicense.setIssuingCountry(familyMemberdetail.getAddress().getCountry());
        drivingLicense.setLastName(familyMemberdetail.getLastName());
        drivingLicense.setLicenseNumber(familyMemberdetail.getIdDetails().getNumber());
        drivingLicense.setMiddleName(familyMemberdetail.getMiddleName());

        Intent personalInfoIntent = new Intent(this,EditFamilyMember.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("drivingLicense", (Parcelable) drivingLicense);
        personalInfoIntent.putExtra("memberId",memberId);
        personalInfoIntent.putExtra("parentId",parentId);
        personalInfoIntent.putExtra("relation",familyMemberdetail.getUserRelationShip());
        personalInfoIntent.putExtras(bundle);
        startActivity(personalInfoIntent);

    }
}
