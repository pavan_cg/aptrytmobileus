package com.aptrytus.uiactivities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.aptrytus.Others.Utility;
import com.aptrytus.R;
import com.aptrytus.models.IndividualDrivingLicense;
import com.google.android.gms.vision.barcode.Barcode;
import com.notbytes.barcode_reader.BarcodeReaderActivity;

public class UserCreationActivity extends AppCompatActivity {

    private Toolbar toolbar;
    int RC_BARCODE_CAPTURE = 9001;

    ProgressDialog progressDialog;

    int flag = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_user_creation);

        toolbar =  findViewById(R.id.toolbar_dr_details);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ActionBar actionBar = this.getSupportActionBar();

        actionBar.setTitle("User Creation");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



    public void scanBarcode(View view){
        Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(getApplicationContext(), true, false);
        startActivityForResult(launchIntent, RC_BARCODE_CAPTURE);
        flag = 1;

    }

    public void getInfoManually(View view){

        Intent personalInfoIntent = new Intent(UserCreationActivity.this,PersonalInfoActivity.class);

        personalInfoIntent.putExtra("manualEntry",1);
        startActivity(personalInfoIntent);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(this, "error in  scanning", Toast.LENGTH_SHORT).show();
            return;
        }

        if (requestCode == RC_BARCODE_CAPTURE && data != null) {

            finish();

            Barcode barcode = data.getParcelableExtra(BarcodeReaderActivity.KEY_CAPTURED_BARCODE);

            IndividualDrivingLicense drivingLicense = new IndividualDrivingLicense();

            drivingLicense.setAddressCity(barcode.driverLicense.addressCity);
            drivingLicense.setAddressState(barcode.driverLicense.addressState);
            drivingLicense.setAddressStreet(barcode.driverLicense.addressStreet);
            drivingLicense.setAddressZip(barcode.driverLicense.addressZip);
            drivingLicense.setBirthDate(barcode.driverLicense.birthDate);
            drivingLicense.setDocumentType(barcode.driverLicense.documentType);
            drivingLicense.setExpiryDate(barcode.driverLicense.expiryDate);
            drivingLicense.setFirstName(barcode.driverLicense.firstName);
            drivingLicense.setGender(barcode.driverLicense.gender);
            drivingLicense.setIssueDate(barcode.driverLicense.issueDate);
            drivingLicense.setIssuingCountry(barcode.driverLicense.issuingCountry);
            drivingLicense.setLastName(barcode.driverLicense.lastName);
            drivingLicense.setLicenseNumber(barcode.driverLicense.licenseNumber);
            drivingLicense.setMiddleName(barcode.driverLicense.middleName);

            Intent personalInfoIntent = new Intent(UserCreationActivity.this,PersonalInfoActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelable("drivingLicense", (Parcelable) drivingLicense);
            personalInfoIntent.putExtra("manualEntry",2);
            personalInfoIntent.putExtras(bundle);
            startActivity(personalInfoIntent);

            //finish();

        }


    }
}
