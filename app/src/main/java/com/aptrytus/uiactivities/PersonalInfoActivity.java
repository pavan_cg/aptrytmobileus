package com.aptrytus.uiactivities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.aptrytus.Others.SharedPreferenceConstants;
import com.aptrytus.Others.Utility;
import com.aptrytus.R;
import com.aptrytus.ViewModels.CreateUserModel;
import com.aptrytus.ViewModels.DoctorsListModel;
import com.aptrytus.adapters.StateAdapter;
import com.aptrytus.interfaces.UserCreationCallback;
import com.aptrytus.models.DrivingLicense;
import com.aptrytus.models.IndividualDrivingLicense;
import com.aptrytus.models.State;
import com.aptrytus.repositories.AppSharedPrefs;
import com.aptrytus.request.Address;
import com.aptrytus.request.AuditInfo;
import com.aptrytus.request.CreateUserRequest;
import com.aptrytus.request.IdDetails;
import com.aptrytus.request.Insurance;
import com.aptrytus.request.InsuranceDetail;
import com.aptrytus.request.Pcp;
import com.aptrytus.request.PcpDetails;
import com.aptrytus.response.CreateUserResponse;
import com.aptrytus.services.ApiResponse;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.notbytes.barcode_reader.BarcodeReaderActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonalInfoActivity extends AppCompatActivity implements View.OnClickListener {

    int RC_BARCODE_CAPTURE = 9001;
    int ADD_INSURANCE = 2;
    int EDIT_PERSONAL_INFO = 3;
    int EDIT_ADDRESS_INFO = 4;

    IndividualDrivingLicense drivingLicense;
    ProgressDialog progressDialog;
    CreateUserModel createUserModel;

    List<InsuranceDetail> insuranceList = new ArrayList<>();


    @BindView(R.id.toolbar_dr_details)
    Toolbar toolbar;
    @BindView(R.id.pcpName)
    EditText pcpName;
    @BindView(R.id.pcpContact)
    EditText pcpContact;
    @BindView(R.id.mobileNo)
    EditText mobileNo;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.confirmPassword)
    EditText confirmPassword;

    @BindView(R.id.streetName)
    TextView streetName;
    @BindView(R.id.cityName)
    TextView cityName;

    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.userAge)
    TextView userAge;

    @BindView(R.id.barcodePersonalInfoLayout)
    LinearLayout barcodePersonalInfoLayout;
    @BindView(R.id.manualPersonalInfoLayout)
    LinearLayout manualPersonalInfoLayout;

    // Views to manual entry

    @BindView(R.id.editFirstName)
    EditText editFirstName;
    @BindView(R.id.editLastName)
    EditText editLastName;
    @BindView(R.id.editLicenseNumber)
    EditText editLicenseNumber;
    @BindView(R.id.editAddress)
    EditText editAddress;
    @BindView(R.id.editCity)
    EditText editCity;
    @BindView(R.id.editZipcode)
    EditText editZipcode;
    @BindView(R.id.editDob)
    EditText editDob;

    @BindView(R.id.dateImg)
    ImageView dateImg;

    /*@BindView(R.id.editStatesSpinner)
    Spinner editStatesSpinner;*/

    @BindView(R.id.editStatesSpinnerManual)
    Spinner editStatesSpinnerManual;

    @BindView(R.id.editGenderSpinner)
    Spinner editGenderSpinner;
    @BindView(R.id.editMaritalStatusSpinner)
    Spinner editMaritalStatusSpinner;


    @BindView(R.id.creation_mobileLayout)
    LinearLayout mobileLayout;
    @BindView(R.id.creation_emailLayout)
    LinearLayout emailLayout;
    @BindView(R.id.creation_passwordLayout)
    LinearLayout passwordLayout;
    @BindView(R.id.creation_confirmLayout)
    LinearLayout confirmPasswordLayout;
    @BindView(R.id.titleText)
    TextView titleText;


    String validationMesssage = "";

    private String addressCity;
    private String addressState;
    private String addressStreet;
    private String addressZip;
    private String birthDate;
    private String documentType;
    private String expiryDate;
    private String firstName;
    private String gender;
    private String maritalStatus;

    private String issueDate;
    private String issuingCountry;
    private String lastName;
    private String licenseNumber;
    private String middleName;

    int flag = 1;

    private int mYear, mMonth, mDay;

    List<State> countryList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_personal_info);
        //LayoutPersonalInfoBinding layoutPersonalInfoBinding = DataBindingUtil.setContentView(this,R.layout.layout_personal_info);
        ButterKnife.bind(this);
        //toolbar =  findViewById(R.id.toolbar_dr_details);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ActionBar actionBar = this.getSupportActionBar();

        actionBar.setTitle("Personal Info");

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        createUserModel = ViewModelProviders.of(this).get(CreateUserModel.class);

        flag = getIntent().getIntExtra("manualEntry", 0);

        if (flag == 2) {
            manualPersonalInfoLayout.setVisibility(View.GONE);

            drivingLicense = getIntent().getParcelableExtra("drivingLicense");
            setInfoFromBarcode(drivingLicense);
        } else {
            barcodePersonalInfoLayout.setVisibility(View.GONE);
            manualPersonalInfoLayout.setVisibility(View.VISIBLE);

            /*editStatesSpinner = findViewById(R.id.editStatesSpinner);
            editGenderSpinner = findViewById(R.id.editGenderSpinner);
            editMaritalStatusSpinner = findViewById(R.id.editMaritalStatusSpinner);*/

            setStateList();
        }

        dateImg.setOnClickListener(this);

        //mobileNo.addTextChangedListener(new MobileNumberFormatting());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mobileNo.addTextChangedListener(new PhoneNumberFormattingTextWatcher("US"));
        }

        if (AppSharedPrefs.getInstance(getApplicationContext()).getBoolean(SharedPreferenceConstants.SCAN_FROM_SETTINGS)) {
            mobileLayout.setVisibility(View.GONE);
            emailLayout.setVisibility(View.GONE);
            passwordLayout.setVisibility(View.GONE);
            confirmPasswordLayout.setVisibility(View.GONE);
            titleText.setText("How is Your " + AppSharedPrefs.getInstance(getApplicationContext()).getName(SharedPreferenceConstants.SELECTED_FAMILY_MEMBERS) + " Profile look?");
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void setInfoFromBarcode(IndividualDrivingLicense drivingLicense) {

        addressCity = drivingLicense.getAddressCity();
        addressState = drivingLicense.getAddressState();
        addressStreet = drivingLicense.getAddressStreet();
        addressZip = drivingLicense.getAddressZip();
        birthDate = drivingLicense.getBirthDate();
        documentType = drivingLicense.getDocumentType();
        expiryDate = drivingLicense.getExpiryDate();
        firstName = drivingLicense.getFirstName();
        gender = drivingLicense.getGender();
        maritalStatus = drivingLicense.getMaritalStatus();
        issueDate = drivingLicense.getIssueDate();
        issuingCountry = drivingLicense.getIssuingCountry();
        lastName = drivingLicense.getLastName();
        licenseNumber = drivingLicense.getLicenseNumber();
        middleName = drivingLicense.getMiddleName();

        // set Values into the fields

        //  set Address
        streetName.setText(toCamelCase(addressStreet));
        String cityNames = addressCity + "," + addressState.toUpperCase() + " " + addressZip;
        cityName.setText(toCamelCase(cityNames));

        // set personal info
        String name = firstName + " " + lastName;
        userName.setText(toCamelCase(name));
        String dob = Utility.getFormatedDate(drivingLicense.getBirthDate());
        int age = getAge(dob);
        userAge.setText("Age : " + age + " yrs");

    }

    //  edit Personal Address
    public void editPersonalAddress(View view) {

        Intent editPersonalAddressIntent = new Intent(this, EditPersonalInfo.class);
        //editPersonalAddressIntent.putExtra("editType",1);

        Bundle bundle = new Bundle();
        bundle.putInt("editType", 1);
        bundle.putParcelable("drivingLicense", (Parcelable) drivingLicense);
        editPersonalAddressIntent.putExtras(bundle);
        startActivityForResult(editPersonalAddressIntent, EDIT_ADDRESS_INFO);

    }

    //  edit personal Info
    public void editPersonalInfo(View view) {
        Intent editPersonaInfoIntent = new Intent(this, EditPersonalInfo.class);
        Bundle bundle = new Bundle();
        bundle.putInt("editType", 2);
        bundle.putParcelable("drivingLicense", (Parcelable) drivingLicense);
        editPersonaInfoIntent.putExtras(bundle);
        startActivityForResult(editPersonaInfoIntent, EDIT_PERSONAL_INFO);
    }


    public void registerUser(View view) {
        if (validateUser(flag)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Add Insurance !");
            builder.setMessage("Want to save time on your next visit? Add your Insurance Info");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent addInsuranceIntent = new Intent(getApplicationContext(), AddInsuranceActivity.class);
                    startActivityForResult(addInsuranceIntent, ADD_INSURANCE);
                }
            });

            builder.setNegativeButton("SKIP", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (AppSharedPrefs.getInstance(getApplicationContext()).getBoolean(SharedPreferenceConstants.SCAN_FROM_SETTINGS)) {
                        addFamilyMeberWithoutInsurance();
                    } else {
                        registerUserWithoutInsurance();
                    }
                }
            });
            builder.show();


        } else {
            Toast.makeText(getApplicationContext(), validationMesssage, Toast.LENGTH_LONG).show();

            //getOTPValidation("","","");
        }
    }

    public boolean validateUser(int flag) {
        boolean isValid = true;

        if (flag == 1) {
            if (TextUtils.isEmpty(editFirstName.getText().toString().trim())) {
                validationMesssage = "Please update First Name";
                return false;
            }
            if (TextUtils.isEmpty(editLastName.getText().toString().trim())) {
                validationMesssage = "Please update Last Name";
                return false;
            }
            if (TextUtils.isEmpty(editLicenseNumber.getText().toString().trim())) {
                validationMesssage = "Please update License Number";
                return false;
            }
            if (TextUtils.isEmpty(editAddress.getText().toString().trim())) {
                validationMesssage = "Please update Address";
                return false;
            }
            if (TextUtils.isEmpty(editCity.getText().toString().trim())) {
                validationMesssage = "Please update City";
                return false;
            }
            if (TextUtils.isEmpty(editZipcode.getText().toString().trim())) {
                validationMesssage = "Please update Zip code";
                return false;
            }
            if (TextUtils.isEmpty(editDob.getText().toString().trim())) {
                validationMesssage = "Please update Date of Birth";
                return false;
            }
        }

        String password1 = password.getText().toString();
        String password2 = confirmPassword.getText().toString();

        if (TextUtils.isEmpty(pcpName.getText().toString().trim())) {
            validationMesssage = "Please update PCP Name";
            return false;
        }
        if (TextUtils.isEmpty(pcpContact.getText().toString().trim())) {
            validationMesssage = "Please update PCP Contact";
            return false;
        }
        if (!AppSharedPrefs.getInstance(getApplicationContext()).getBoolean(SharedPreferenceConstants.SCAN_FROM_SETTINGS)) {

            if (TextUtils.isEmpty(mobileNo.getText().toString().trim())) {
                validationMesssage = "Please update Mobile Number";
                return false;
            }

            if (TextUtils.isEmpty(email.getText().toString().trim())) {
                validationMesssage = "Please update Valid Email";
                return false;
            } else if (!emailValidator(email.getText().toString().trim())) {
                validationMesssage = "Please update Valid Email";
                return false;
            }
            if (TextUtils.isEmpty(password.getText().toString().trim())) {
                validationMesssage = "Please update password";
                return false;
            } else if (!password1.equals(password2)) {
                validationMesssage = "Both Passwords should be same";
                return false;
            }

        }

        return isValid;
    }

    public void registerUserWithoutInsurance() {

        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest = createUserRequestObject(createUserRequest);

        progressDialog = Utility.getProgressDialog(this, "Please wait...");
        progressDialog.show();
        createUserModel.registerUser(createUserRequest).observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {

                if (apiResponse.createUserResponse != null) {

                    String msg = apiResponse.createUserResponse.getMessage();
                    Toast.makeText(getApplicationContext(), " " + msg, Toast.LENGTH_SHORT).show();
                    activateUser(apiResponse.createUserResponse);
                    finish();
                    /*Log.i("Response received ",apiResponse.createUserResponse.getMessage().toString());*/
                } else {
                    String message = Utility.getApiError(apiResponse);
                    Toast.makeText(getApplicationContext(), "Error received " + message, Toast.LENGTH_SHORT).show();
                }

                progressDialog.dismiss();
            }
        });

    }


    public void addFamilyMeberWithoutInsurance() {

        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest = createUserRequestObject(createUserRequest);
        progressDialog = Utility.getProgressDialog(this, "Please wait...");
        progressDialog.show();
        String userEmailId = AppSharedPrefs.getInstance(getApplicationContext()).getName(SharedPreferenceConstants.LOGEED_IN_USER_EMAIL_ID);
        createUserModel.addFamilyMember(createUserRequest, userEmailId, AppSharedPrefs.getInstance(getApplicationContext()).getName(SharedPreferenceConstants.SELECTED_FAMILY_MEMBERS)).observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                if (apiResponse.familyMemberResponse != null) {

                    String msg = apiResponse.familyMemberResponse.getMessage();
                    Toast.makeText(getApplicationContext(), " " + msg, Toast.LENGTH_SHORT).show();
                    Intent intent = getIntent();
                    setResult(RESULT_OK, intent);
                    finish();
                    /*Log.i("Response received ",apiResponse.createUserResponse.getMessage().toString());*/
                } else {
                    String message = Utility.getApiError(apiResponse);
                    Toast.makeText(getApplicationContext(), "Error received " + message, Toast.LENGTH_SHORT).show();
                }

                progressDialog.dismiss();
            }
        });

    }

    /**
     * activate the user with OTP verification
     *
     * @param createUserResponse
     */
    public void activateUser(CreateUserResponse createUserResponse) {
        getOTPValidation(createUserResponse.getUserInfoResponse().getId(),
                createUserResponse.getUserInfoResponse().getUserId(),
                createUserResponse.getUserInfoResponse().getPrimaryPhone());
    }

    public void getOTPValidation(String id, String userId, String phNumber) {

        Intent otpIntent = new Intent(this, OtpVerificationActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("userId", userId);
        bundle.putString("phoneNum", phNumber);
        otpIntent.putExtras(bundle);
        startActivity(otpIntent);


    }

    public CreateUserRequest createUserRequestObject(CreateUserRequest createUserRequest) {

        if (flag == 1) {
            //editStatesSpinner.setSelection(0);
            IndividualDrivingLicense dl = new IndividualDrivingLicense();
            dl.setFirstName(editFirstName.getText().toString());
            dl.setLastName(editLastName.getText().toString());
            dl.setLicenseNumber(editLicenseNumber.getText().toString());
            dl.setAddressStreet(editAddress.getText().toString());
            dl.setAddressCity(editCity.getText().toString());
            dl.setAddressZip(editZipcode.getText().toString());
            dl.setBirthDate(editDob.getText().toString());
            String state = editStatesSpinnerManual.getSelectedItem().toString();
            dl.setAddressState(editStatesSpinnerManual.getSelectedItem().toString());

            String genderTxt = editGenderSpinner.getSelectedItem().toString();
            String gender = "M";
            if (genderTxt.equals("Male")) {
                gender = "M";
            } else {
                gender = "F";
            }
            dl.setGender(gender);
            dl.setMaritalStatus(editMaritalStatusSpinner.getSelectedItem().toString());

            drivingLicense = dl;
        }


        createUserRequest.setFirstName(drivingLicense.getFirstName());
        createUserRequest.setLastName(drivingLicense.getLastName());
        createUserRequest.setMiddleName(drivingLicense.getMiddleName());
        if (drivingLicense.getMaritalStatus() == null) {
            createUserRequest.setMaritalStatus("Single");
        } else {
            createUserRequest.setMaritalStatus(drivingLicense.getMaritalStatus());
        }

        IdDetails idDetails = new IdDetails();

        idDetails.setNumber(drivingLicense.getLicenseNumber());
        idDetails.setType(drivingLicense.getDocumentType());

        if (drivingLicense.getIssueDate() != null) {
            idDetails.setIdIssuedState(Utility.getFormatedDate(drivingLicense.getIssueDate()));
            idDetails.setValidFromDate(Utility.getFormatedDate(drivingLicense.getIssueDate()));
            idDetails.setValidToDate(Utility.getFormatedDate(drivingLicense.getExpiryDate()));
        } else {
            idDetails.setIdIssuedState("");
            idDetails.setValidFromDate("");
            idDetails.setValidToDate("");
        }

        /*createUserRequest.setDriverLicenseNumber(drivingLicense.getLicenseNumber());
        createUserRequest.setDriverLicenseValidFromDate(Utility.getFormatedDate(drivingLicense.getIssueDate()));
        createUserRequest.setDriverLicenseValidToDate(Utility.getFormatedDate(drivingLicense.getExpiryDate()));*/
        createUserRequest.setIdDetails(idDetails);

        String genderVal = "M";
        if (drivingLicense.getGender().equals("2")) {
            genderVal = "F";
        }

        createUserRequest.setGender(genderVal);
        createUserRequest.setDateOfBirth(Utility.getFormatedDate(drivingLicense.getBirthDate()));

        createUserRequest.setEmail(email.getText().toString());
        if (!AppSharedPrefs.getInstance(getApplicationContext()).getBoolean(SharedPreferenceConstants.SCAN_FROM_SETTINGS)) {
            String phone = Utility.formatUSPhoneNum(mobileNo.getText().toString().trim());
            createUserRequest.setPrimaryPhone(phone);
            createUserRequest.setUserId(email.getText().toString());
            createUserRequest.setPassword(password.getText().toString());
        }
        /*createUserRequest.setIsGuest("0");
        createUserRequest.setOtpVerfCode("");
        createUserRequest.setOtpCreateDateTime("2018-12-29T06:06:54.416+0000");
        createUserRequest.setUserStatus("active");
        createUserRequest.setPrimaryUser(true);*/

        // address
        Address address = new Address();
        address.setStreetAddress(drivingLicense.getAddressStreet());
        address.setCity(drivingLicense.getAddressCity());
        address.setState(drivingLicense.getAddressState());
        address.setZipcode(drivingLicense.getAddressZip());
        address.setCountry(drivingLicense.getIssuingCountry());

        createUserRequest.setAddress(address);

        //  Insurance
        if (insuranceList != null) {
            if (insuranceList.size() > 0) {

                createUserRequest.setInsuranceDetails(insuranceList);

            }
        }

        // add pcp details

        /*Pcp pcp = new Pcp();
        pcp.setName(pcpName.getText().toString());
        pcp.setPhone(pcpContact.getText().toString());*/

        PcpDetails pcp = new PcpDetails();
        pcp.setName(pcpName.getText().toString());
        pcp.setContact(pcpContact.getText().toString());
        createUserRequest.setPcpDetails(pcp);

        if (AppSharedPrefs.getInstance(getApplicationContext()).getBoolean(SharedPreferenceConstants.SCAN_FROM_SETTINGS)) {
            createUserRequest.setUserRelationShip(AppSharedPrefs.getInstance(getApplicationContext()).getName(SharedPreferenceConstants.SELECTED_FAMILY_MEMBERS));
            createUserRequest.setPrimaryPhone(AppSharedPrefs.getInstance(getApplicationContext()).getName(SharedPreferenceConstants.LOGEED_IN_USER_MOBILE));

        } else {
            createUserRequest.setUserRelationShip("Self");
        }
        createUserRequest.setId("");

        return createUserRequest;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_INSURANCE) {

            if (resultCode == RESULT_OK) {

                insuranceList = (List<InsuranceDetail>) data.getExtras().get("insurance");
                if (AppSharedPrefs.getInstance(getApplicationContext()).getBoolean(SharedPreferenceConstants.SCAN_FROM_SETTINGS)) {
                    addFamilyMeberWithoutInsurance();
                } else {
                    registerUserWithoutInsurance();
                }

            }

        } else if (requestCode == EDIT_PERSONAL_INFO || requestCode == EDIT_ADDRESS_INFO) {

            if (resultCode == RESULT_OK) {
                drivingLicense = data.getParcelableExtra("editPersonaldata");
                //drivingLicense = getIntent().getParcelableExtra("editPersonaldata");
                setInfoFromBarcode(drivingLicense);
            }

        }
    }

    @Override
    public void onClick(View v) {
        if (v == dateImg) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();

            /*String dobFromLicense = drivingLicense.getBirthDate();

            mYear = Integer.parseInt(dobFromLicense.substring(4));
            mMonth = Integer.parseInt(dobFromLicense.substring(0,2));
            mDay = Integer.parseInt(dobFromLicense.substring(2,4));*/

            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DATE);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            String months = "";
                            if (monthOfYear < 9) {
                                int monthNum = monthOfYear + 1;
                                months = "0" + monthNum;

                            } else {
                                int monthNum = monthOfYear + 1;
                                months = "" + monthNum;
                            }

                            editDob.setText(year + "-" + months + "-" + dayOfMonth);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
    }

    /**
     * Formatting a Mobile number: ###-###-####
     */
    public static class MobileNumberFormatting implements TextWatcher {

        private boolean lock;

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (lock || s.length() > 10) {
                return;
            }
            lock = true;
            for (int i = 0; i > s.length(); i++) {
                if (s.length() == 2 || s.length() == 6) {
                    s.append("-");
                }
            }
            lock = false;
        }
    }

    /**
     * validate your email address format. Ex-akhi@mani.com
     */
    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher((CharSequence) email);
        return matcher.matches();
    }

    public static String toCamelCase(final String init) {
        if (init == null)
            return null;

        final StringBuilder ret = new StringBuilder(init.length());

        for (final String word : init.split(" ")) {
            if (!word.isEmpty()) {
                ret.append(word.substring(0, 1).toUpperCase());
                ret.append(word.substring(1).toLowerCase());
            }
            if (!(ret.length() == init.length()))
                ret.append(" ");
        }

        return ret.toString();
    }

    private int getAge(String dobString) {

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        return age;
    }

    public void setStateList() {
        String json = null;
        try {
            InputStream is = getAssets().open("states.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

             /*stateList = new Gson().fromJson(json,StateList.class);
             Toast.makeText(this,"",Toast.LENGTH_LONG).show();*/
            Gson gson = new Gson();
            // Usinf fromJson(reader,type)
            countryList = gson.fromJson(json, new TypeToken<List<State>>() {
            }.getType());
            //Toast.makeText(this, "--> " + countryList.size(), Toast.LENGTH_LONG).show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        StateAdapter adapter = new StateAdapter(this, countryList);
        // editStatesSpinner.setAdapter(adapter);

    }


}
