package com.aptrytus.uiactivities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.aptrytus.R;
import com.aptrytus.fragments.CreateAccountFragment;
import com.aptrytus.fragments.LoginFragment;

public class PopuActivity extends AppCompatActivity {

    int FRAGMENT_FLAG = 5;
    TextView toolBarText;

    FragmentManager fm;
    FragmentTransaction ft;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_popup_activity);

        toolBarText = findViewById(R.id.toolbar_title);

        FRAGMENT_FLAG = getIntent().getExtras().getInt("FRAGMENT_FLAG");

        fm = getFragmentManager();
        ft = fm.beginTransaction();;

        LoginFragment loginFragment = LoginFragment.newInstance(1,"Login");
        CreateAccountFragment createAccountFragment = CreateAccountFragment.newInstance();

        if(FRAGMENT_FLAG == 0){
            ft.add(R.id.fragment_layout, loginFragment);
            toolBarText.setText(getResources().getText(R.string.title_login));
        }else{
            ft.add(R.id.fragment_layout, createAccountFragment);
            toolBarText.setText(getResources().getText(R.string.title_create_account));
        }

        ft.commit();

    }

    public void closePopup(View view){
        finish();
    }


}