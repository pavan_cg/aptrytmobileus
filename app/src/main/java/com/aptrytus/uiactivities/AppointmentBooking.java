package com.aptrytus.uiactivities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aptrytus.Others.SharedPreferenceConstants;
import com.aptrytus.Others.UserList;
import com.aptrytus.Others.Utility;
import com.aptrytus.R;
import com.aptrytus.ViewModels.AppointmentViewModel;
import com.aptrytus.ViewModels.DoctorsListModel;
import com.aptrytus.ViewModels.FamilyMemberViewModel;
import com.aptrytus.adapters.FamilyMemberAdapter;
import com.aptrytus.adapters.SelfPayServiceAdapter;
import com.aptrytus.facilities.FacilityResponse;
import com.aptrytus.facilities.SelfPayService;
import com.aptrytus.familyMembersInsurance.FamilyMemberInsuranceResponse;
import com.aptrytus.familyMembersInsurance.FamilyMemberdetail;
import com.aptrytus.familyMembersInsurance.Insurance;
import com.aptrytus.familyMembersInsurance.UserInfoResponse;
import com.aptrytus.repositories.AppSharedPrefs;
import com.aptrytus.request.appointmentBooking.AdvanceEligible;
import com.aptrytus.request.appointmentBooking.Appointment;
import com.aptrytus.request.appointmentBooking.AppointmentBookingRequest;
import com.aptrytus.request.appointmentBooking.InsuranceInfo;
import com.aptrytus.response.familyDetails.FamilyMemberResponse;
import com.aptrytus.response.insurance.InsuranceNamesList;
import com.aptrytus.services.ApiResponse;

import java.util.ArrayList;
import java.util.List;

public class AppointmentBooking extends AppCompatActivity {

    private Toolbar toolbar;
    ProgressDialog progressDialog;

    DoctorsListModel doctorViewModel;
    FamilyMemberResponse familyMemberResponse;
    FamilyMemberViewModel familyMemberViewModel;
    AppointmentViewModel appointmentViewModel;
    List<UserList> userList = new ArrayList<>();
    List<Insurance> insuranceList = new ArrayList<>();

    //  username details
    TextView userNameView;
    TextView userAgeView;

    //  Personal address
    TextView streetNameView;
    TextView cityNameView;

    //  urgent care address

    TextView urgentCareNameView;
    TextView urgentCareAddressView;
    TextView patientsInQView;
    TextView waitingTimeView;

    //  purpose of visit
    EditText purpose;


    FacilityResponse facilityResponse;

    int index;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_appointment_booking);

        index = getIntent().getIntExtra("pos",0);

        facilityResponse = Utility.facilityResponse;

        toolbar =  findViewById(R.id.toolbar_dr_details);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ActionBar actionBar = this.getSupportActionBar();

        actionBar.setTitle("Appointment Booking");

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        userNameView = (TextView) findViewById(R.id.userName);
        userAgeView = (TextView) findViewById(R.id.userAge);
        streetNameView = (TextView) findViewById(R.id.streetName);
        cityNameView = (TextView) findViewById(R.id.cityName);

        urgentCareNameView = (TextView) findViewById(R.id.urgentCareName);
        urgentCareAddressView = (TextView) findViewById(R.id.urgentCareAddress);
        patientsInQView = (TextView) findViewById(R.id.patientsInQ);
        waitingTimeView = (TextView) findViewById(R.id.waitingTime);

        purpose = (EditText) findViewById(R.id.purposeOfVisit);


        progressDialog = Utility.getProgressDialog(this,"fetching membersinfo");
        progressDialog.show();

        //  to get the family  member details
        familyMemberViewModel = ViewModelProviders.of(this).get(FamilyMemberViewModel.class);

        familyMemberViewModel.getFamilyMebers("vsn@we.in").observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                FamilyMemberInsuranceResponse familyMemberInsuranceResponse = apiResponse.familyMemberInsuranceResponse;
                Toast.makeText(getBaseContext(),""+familyMemberInsuranceResponse.getMessage(),Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                setMemberList(familyMemberInsuranceResponse);
            }
        });

        //  to get an appointment
        appointmentViewModel = ViewModelProviders.of(this).get(AppointmentViewModel.class);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void scanBarcode(View view){

    }

    public void setMemberList(FamilyMemberInsuranceResponse familyMemberInsuranceResponse){

        if(userList.size()>0){
            userList.clear();
        }
        if(insuranceList.size()>0){
            insuranceList.clear();
        }

        UserInfoResponse userInfoResponse =  familyMemberInsuranceResponse.getUserInfoResponse();

        UserList user = new UserList(userInfoResponse.getFirstName(),
                                        userInfoResponse.getLastName(),
                                        ""+Utility.getAge(userInfoResponse.getDateOfBirth()),
                                        userInfoResponse.getUserRelationShip(),
                                        userInfoResponse.getId());

        userList.add(user);
        List<FamilyMemberdetail> familyMemberdetails = familyMemberInsuranceResponse.getUserInfoResponse().getFamilyMemberdetails();

        for (int i=0;i<familyMemberdetails.size();i++){

            UserList user2 = new UserList(familyMemberdetails.get(i).getFirstName(),
                                            familyMemberdetails.get(i).getLastName(),
                    ""+Utility.getAge(familyMemberdetails.get(i).getDateOfBirth()),
                                            familyMemberdetails.get(i).getUserRelationShip(),
                                            familyMemberdetails.get(i).getId());

            userList.add(user2);

        }
        // set insurance list
        insuranceList = familyMemberInsuranceResponse.getUserInfoResponse().getInsuranceDetails();

        // init user list

        setSelectedFamilyMember(0);
        setAddress(familyMemberInsuranceResponse);
        setUgrentCareAdress();

    }

    public void displayFamilyList(View view){
        showFamilyMemberDialog(userList);
    }

    public void setAddress(FamilyMemberInsuranceResponse member){

        streetNameView.setText(member.getUserInfoResponse().getAddress().getStreet());
        cityNameView.setText(member.getUserInfoResponse().getAddress().getCity());

    }

    public void showFamilyMemberDialog(final List<UserList> userlist) {
        Log.i("showFamilyMemberDialog","UserList:"+userlist.size());
        final Dialog dialog = new Dialog(this);
        View view = getLayoutInflater().inflate(R.layout.selft_service_dialog, null);
        ListView lv = (ListView) view.findViewById(R.id.self_service_dialog_list);
        ImageView img = (ImageView) view.findViewById(R.id.self_dialog_close);
        TextView popupHeader = view.findViewById(R.id.popup_header);
        popupHeader.setText("Family Members");
        img.setImageResource(R.drawable.cross);
        img.getLayoutParams().height = 80;
        img.getLayoutParams().width = 80;
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),""+userlist.get(position).getFirstName(),Toast.LENGTH_LONG).show();
                setSelectedFamilyMember(position);
                dialog.dismiss();
            }
        });

        FamilyMemberAdapter familyMemberAdapter = new FamilyMemberAdapter(this, userlist);

        lv.setAdapter(familyMemberAdapter);
        dialog.setTitle("Family List");
        dialog.setContentView(view);
        dialog.show();

    }

    public void setSelectedFamilyMember(int index){

        String name = userList.get(index).getFirstName().toUpperCase()+" "+userList.get(index).getLastName().toUpperCase();
        String age = "Age : "+userList.get(index).getAge();

        userNameView.setText(name);
        userAgeView.setText(age);

    }

    public void setUgrentCareAdress(){

        urgentCareNameView.setText(facilityResponse.getFacility().get(index).getFacilityName());
        String address = facilityResponse.getFacility().get(index).getAddress().getStreet()+", "+facilityResponse.getFacility().get(index).getAddress().getCity();
        urgentCareAddressView.setText(address);
        /*TextView patientsInQView;
        TextView waitingTimeView;*/

    }

    public void getAnAppointment(View view){
        // build the request
        AppointmentBookingRequest request = new AppointmentBookingRequest();

        request.setId("4be035bbde9640e3aafce582fb835235");
        request.setUserInfoId(userList.get(index).getUserId());
        request.setSelfPay(false);
        request.setFacilityId(""+facilityResponse.getFacility().get(index).getId());

        InsuranceInfo insuranceinfo = new InsuranceInfo();
        insuranceinfo.setPayerName("UHG");
        insuranceinfo.setLocation("HYD");
        request.setInsuranceInfo(insuranceinfo);

        AdvanceEligible advanceEligible = new AdvanceEligible();
        advanceEligible.setIsAdvance(true);
        advanceEligible.setPatientAcceptance(true);
        request.setAdvanceEligible(advanceEligible);

        Appointment appointment = new Appointment();
        appointment.setDateTime("07/06/2019 10:11:59");
        appointment.setProposedWaitTime("07/06/2019 11:11:59");
        appointment.setPurposeOfVisit(purpose.getText().toString());
        appointment.setPurposeOfVisit("neck pain");
        appointment.setPreferredSlot(10);
        request.setAppointment(appointment);

        appointmentViewModel.bookAppointment(request).observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                Toast.makeText(getApplicationContext(),""+apiResponse.appointmentBookingResponse.getMessage().toString(),Toast.LENGTH_LONG).show();
                navigateToHome(apiResponse.appointmentBookingResponse.getMessage());
            }
        });

    }

    public void navigateToHome(String message){


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Appointment Status!");
        builder.setMessage(message);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intents = new Intent(AppointmentBooking.this, HomeScreenActivity.class);
                intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intents);
                finish();
            }
        });

        /*builder.setNegativeButton("SKIP", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (AppSharedPrefs.getInstance(getApplicationContext()).getBoolean(SharedPreferenceConstants.SCAN_FROM_SETTINGS)) {
                    addFamilyMeberWithoutInsurance();
                } else {
                    registerUserWithoutInsurance();
                }
            }
        });*/
        builder.show();

    }
}
