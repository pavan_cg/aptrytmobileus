package com.aptrytus.uiactivities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.aptrytus.Others.SharedPreferenceConstants;
import com.aptrytus.R;
import com.aptrytus.repositories.AppSharedPrefs;

public class TermsAndConditionsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }



    public void goToHomeScreen(){
        Intent intent = new Intent(TermsAndConditionsActivity.this,HomeScreenActivity.class);
        startActivity(intent);
    }

    public void agreePolicy(View view){
        AppSharedPrefs.getInstance(getApplicationContext()).setBoolean(SharedPreferenceConstants.CHECK_APP_INSTALLED,true);
        finish();
        goToHomeScreen();
    }
}
