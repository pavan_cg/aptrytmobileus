package com.aptrytus.uiactivities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aptrytus.Others.Utility;
import com.aptrytus.R;
import com.aptrytus.ViewModels.DoctorsListModel;
import com.aptrytus.ViewModels.FacilitiesViewModelFactory;
import com.aptrytus.ViewModels.TextSuggestionModel;
import com.aptrytus.adapters.DoctotsListAdapter;
import com.aptrytus.adapters.InsuranceServiceAdapter;
import com.aptrytus.adapters.SelfPayServiceAdapter;
import com.aptrytus.adapters.TextSuggestionAdapter;
import com.aptrytus.facilities.Facility;
import com.aptrytus.facilities.FacilityResponse;
import com.aptrytus.facilities.SelfPayService;
import com.aptrytus.facilities.ServiceList;
import com.aptrytus.interfaces.DoctorListCallback;
import com.aptrytus.models.TextSuggestionsResponse;
import com.aptrytus.services.ApiResponse;

import java.util.List;

public class DoctorListActivity extends AppCompatActivity implements DoctorListCallback, TextSuggestionAdapter.OnItemClickListener {

    private Toolbar toolbar;
    ProgressDialog progressDialog;

    DoctorsListModel doctorViewModel;

    DoctotsListAdapter doctorsAdapter;
    RecyclerView recyclerView;

    RecyclerView recyclerView_textSearch;

    TextView doctorListMapBtn;

    TextSuggestionModel textSuggestionModel;

    RelativeLayout relativeLayout_llmapfilter;

    LinearLayout linear_doctorList;

    TextSuggestionAdapter mAdapter;

    SharedPreferences pref; // 0 - for private mode

    SharedPreferences.Editor editor;

    FacilitiesViewModelFactory facilitiesViewModelFactory;

    String selectedCity = "";

    String selectedZipCode = "";

    RelativeLayout emptyListLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_doctor_list);

        recyclerView = findViewById(R.id.doctor_recyclerview);
        recyclerView_textSearch = findViewById(R.id.doctor_text_suggestion);

        linear_doctorList = findViewById(R.id.linear_doctorList);
        pref = getApplicationContext().getSharedPreferences(Utility.APTRY_PREF, 0);

        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ActionBar actionBar = this.getSupportActionBar();

        actionBar.setTitle("Urgent Care Centers");

        emptyListLayout = (RelativeLayout) findViewById(R.id.emptyListLayout);

        doctorViewModel = ViewModelProviders.of(this).get(DoctorsListModel.class);

        textSuggestionModel = ViewModelProviders.of(this).get(TextSuggestionModel.class);


        if (pref.getBoolean(Utility.DATAFROMSEARCHFRAGMENT, false)) {
            editor = pref.edit();
            editor.putBoolean(Utility.DATAFROMSEARCHFRAGMENT, false);
            editor.commit();
            selectedCity = pref.getString(Utility.CITY, "");
            selectedZipCode = pref.getString(Utility.ZIPCODE, "");
            getDoctorsList(pref.getString(Utility.SEARHED_TEXT, ""), selectedCity, selectedZipCode);

        } else {
            displayDoctorList(doctorViewModel.getFacilitiesDeatilsScreen());
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_view_menu_item, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewAndroidActionBar.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() >= 3) {
                    //Toast.makeText(getActivity(),"s"+s,Toast.LENGTH_LONG).show();

                    getDoctorsList(newText, selectedCity, selectedZipCode);
                } else if (newText.length() == 0) {
                    //getDoctorsList(pref.getString(Utility.SEARHED_TEXT,""));
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    public void getDoctorsList(String searchText, String city, String zipcode) {

        progressDialog = Utility.getProgressDialog(this, "Please wait...");
        progressDialog.show();


        doctorViewModel.getFacilities(searchText, city, zipcode).observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                //displayDoctorList(apiResponse.facilitiesList);
              //  progressDialog.dismiss();
                if (apiResponse.facilitiesList != null) {
                    displayDoctorList(apiResponse.facilitiesList);
                } else {
                    String message = Utility.getApiError(apiResponse);
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }
        });

    }

    public void displayDoctorList(FacilityResponse facilityResponse) {
        if (facilityResponse != null) {
            int facilitiesArraySize = facilityResponse.getFacility() == null ? 0 : facilityResponse.getFacility().size();
            if(facilitiesArraySize ==0){
                emptyListLayout.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }else {
                emptyListLayout.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                doctorsAdapter = new DoctotsListAdapter(DoctorListActivity.this, facilityResponse);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(doctorsAdapter);
            }
        }else{
            emptyListLayout.setVisibility(View.VISIBLE);
        }

    }


    public void onDoctorListClicked(Facility facility, int position) {

        Intent intent = new Intent(DoctorListActivity.this, DoctorDetailsActivity.class);
        Bundle bundle = new Bundle();
        // bundle.putString("doctorName",doctorDetails.getDocFullName());
        bundle.putInt("selectedPosition", position);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    public void onUmberllaIconClicked(Facility facility, int position) {


        showInsuranceDialog(facility.getInsurancesAccepted());
    }

    public void selfServiceIconClicked(Facility facility, int position) {

        //showServiceDialog(facility.getSelfPayServices().getServiceList());
        showServiceDialog(facility.getSelfPayServices());
    }

    public void setTextList(List<TextSuggestionsResponse> textSuggestionsResponse) {
        progressDialog.dismiss();
        mAdapter = new TextSuggestionAdapter(textSuggestionsResponse, this);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(String test, int position) {

        Log.i("Position", "Test :" + test + " position" + position);
        CharSequence charSequence = test;
        //getDoctorsList();

    }


    public void showInsuranceDialog(List<String> insuranceList) {
        if (insuranceList != null) {
     /*       String[] displayInsuranceList = insuranceList.toArray(new String[insuranceList.size()]);

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

            alertDialog.setTitle("Accepted Insurance List");

            alertDialog.setItems(displayInsuranceList, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
               dialogInterface.dismiss();
                }
            });
            AlertDialog dialog = alertDialog.create();

            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();

            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));

            // Set the divider height of alert dialog list view
            listView.setDividerHeight(3);

            // Finally, display the alert dialog
            dialog.show();*/
            final Dialog dialog = new Dialog(this);
            View view = getLayoutInflater().inflate(R.layout.insurance_service_dialog, null);
            ListView lv = (ListView) view.findViewById(R.id.insurance_service_dialog_list);

            ImageView img = (ImageView) view.findViewById(R.id.insurance_dialog_close);
            img.setImageResource(R.drawable.cross);
            img.getLayoutParams().height = 80;
            img.getLayoutParams().width = 80;
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            /*RelativeLayout ll = new RelativeLayout(this);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            params.addRule(RelativeLayout.CENTER_HORIZONTAL);
            params.addRule(RelativeLayout.CENTER_VERTICAL);

            RelativeLayout.LayoutParams paramsImage = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            paramsImage.setMargins(0, 50, 50, 50);

            paramsImage.addRule(RelativeLayout.CENTER_VERTICAL);
            paramsImage.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

            ll.setMinimumHeight(150);
            TextView textView = new TextView(this);
            textView.setText("Insurances Accepted");
            textView.setTextColor(getResources().getColor(R.color.mainBackground));
            textView.setTypeface(null, Typeface.BOLD);
            textView.setTextSize(22);
            textView.setLayoutParams(params); //causes layout updat

            RelativeLayout llImg = new RelativeLayout(this);

            ImageView imageView = new ImageView(this);
            imageView.setImageResource(R.drawable.cross);
            imageView.setLayoutParams(paramsImage);

            ll.addView(textView);
            llImg.addView(imageView);
            ll.addView(llImg);
            imageView.getLayoutParams().height = 80;
            imageView.getLayoutParams().width =80;


            lv.addHeaderView(ll);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    dialog.dismiss();
                }
            });*/
            // Change MyActivity.this and myListOfItems to your own values
            InsuranceServiceAdapter clad = new InsuranceServiceAdapter(this, insuranceList);

            lv.setAdapter(clad);
            dialog.setTitle("Insurance Accepted");
            dialog.setContentView(view);
            dialog.show();

        } else {
            Toast.makeText(getApplicationContext(), "No Insurances will accept", Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("ResourceAsColor")
    public void showServiceDialog(List<SelfPayService> serviceListOfItems) {
        Log.i("serviceListOfItems","serviceListOfItems:"+serviceListOfItems.size());
        final Dialog dialog = new Dialog(this);
        View view = getLayoutInflater().inflate(R.layout.selft_service_dialog, null);
        ListView lv = (ListView) view.findViewById(R.id.self_service_dialog_list);
        ImageView img = (ImageView) view.findViewById(R.id.self_dialog_close);
        img.setImageResource(R.drawable.cross);
        img.getLayoutParams().height = 80;
        img.getLayoutParams().width = 80;
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

       /* RelativeLayout ll = new RelativeLayout(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        params.addRule(RelativeLayout.CENTER_VERTICAL);

        RelativeLayout.LayoutParams paramsImage = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsImage.setMargins(0, 50, 50, 50);

        paramsImage.addRule(RelativeLayout.CENTER_VERTICAL);
        paramsImage.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        ll.setMinimumHeight(150);
        TextView textView = new TextView(this);
        textView.setText("Selfpay Services");
        textView.setTextColor(getResources().getColor(R.color.mainBackground));
        textView.setTypeface(null, Typeface.BOLD);
        textView.setTextSize(22);
        textView.setLayoutParams(params); //causes layout updat

        RelativeLayout llImg = new RelativeLayout(this);

        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.cross);
        imageView.setLayoutParams(paramsImage);

        ll.addView(textView);
        llImg.addView(imageView);
        ll.addView(llImg);
        imageView.getLayoutParams().height = 80;
        imageView.getLayoutParams().width =80;


        lv.addHeaderView(ll);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dialog.dismiss();
            }
        });*/
        // Change MyActivity.this and myListOfItems to your own values
        SelfPayServiceAdapter clad = new SelfPayServiceAdapter(this, serviceListOfItems);

        lv.setAdapter(clad);
        dialog.setTitle("Selfpay Services");
        dialog.setContentView(view);
        dialog.show();

    }


}
