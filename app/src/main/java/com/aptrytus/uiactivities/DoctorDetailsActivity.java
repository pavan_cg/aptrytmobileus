package com.aptrytus.uiactivities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aptrytus.Others.Utility;
import com.aptrytus.R;
import com.aptrytus.ViewModels.DoctorsListModel;
import com.aptrytus.ViewModels.FacilitiesViewModelFactory;
import com.aptrytus.adapters.CustomExpandableListAdapter;
import com.aptrytus.facilities.FacilityResponse;
import com.aptrytus.facilities.Regular;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DoctorDetailsActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private Toolbar toolbar;
    private boolean fabExpanded = false;
    private FloatingActionButton fabSettings;
    private LinearLayout layoutFabSave;
    private LinearLayout layoutFabEdit;
    private LinearLayout layoutFabPhoto;
    DoctorsListModel doctorViewModel;
    FacilitiesViewModelFactory facilitiesViewModelFactory;
    FacilityResponse facilityResponse;

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDetail;

    TextView tv_details_facilityName;
    TextView tv_details_facilityBrand;
    TextView tv_details_facility_list_address;

    TextView tv_details_phoneNumber;

    //TextView tv_book_now;
    Button book_now_btn;


    ExpandableListView insurance_ExpandableListView;
    List<String> insurance_expandableListTitle;
    HashMap<String, List<String>> insurance_expandableListDetail;
    ExpandableListAdapter insuranceExpandableListAdapter;


    ExpandableListView timings_ExpandableListView;
    List<String> timings_expandableListTitle;
    HashMap<String, List<String>> timings_expandableListDetail;
    ExpandableListAdapter timingsExpandableListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_doctor_details);

        toolbar =  findViewById(R.id.toolbar_dr_details);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ActionBar actionBar = this.getSupportActionBar();

        actionBar.setTitle("Urgent Care Details");


        doctorViewModel = ViewModelProviders.of(this).get(DoctorsListModel.class);
        facilityResponse = doctorViewModel.getFacilitiesDeatilsScreen();

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);

        insurance_ExpandableListView = (ExpandableListView) findViewById(R.id.insurance_expandableListView);

        timings_ExpandableListView = (ExpandableListView) findViewById(R.id.time_expandableListView);

        tv_details_facilityName = (TextView) findViewById(R.id.details_facilityName);

        tv_details_facilityBrand = (TextView) findViewById(R.id.details_facilityBrand);

        tv_details_facility_list_address = (TextView) findViewById(R.id.details_facility_list_address);

        tv_details_phoneNumber = (TextView) findViewById(R.id.details_phonenumber);

        book_now_btn = (Button) findViewById(R.id.book_now_btn);

        Bundle bundleData = getIntent().getExtras();
        int position = bundleData.getInt("selectedPosition");

        tv_details_facilityName.setText(facilityResponse.getFacility().get(position).getFacilityName());

        tv_details_facilityBrand.setText(facilityResponse.getFacility().get(position).getFacilityBrand());

        tv_details_facility_list_address.setText(facilityResponse.getFacility().get(position).getAddress().getStreet()+","+facilityResponse.getFacility().get(position).getAddress().getCity());

        tv_details_phoneNumber.setText("Telephone : "+facilityResponse.getFacility().get(position).getFacilityContact().getTelephone());

        expandableListDetail = getData(facilityResponse,position);
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
               /* Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();*/
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
               /* Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT).show();*/

            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
               /* Toast.makeText(
                        getApplicationContext(),
                        expandableListTitle.get(groupPosition)
                                + " -> "
                                + expandableListDetail.get(
                                expandableListTitle.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT
                ).show();*/
                return false;
            }
        });

        //Insurances
        insurance_expandableListDetail = getInsuranceDetails(facilityResponse,position);
        insurance_expandableListTitle = new ArrayList<String>(insurance_expandableListDetail.keySet());
        insuranceExpandableListAdapter = new CustomExpandableListAdapter(this, insurance_expandableListTitle, insurance_expandableListDetail);
        insurance_ExpandableListView.setAdapter(insuranceExpandableListAdapter);
        insurance_ExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
               /* Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();*/
            }
        });

        insurance_ExpandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
               /* Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT).show();*/

            }
        });

        insurance_ExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
               /* Toast.makeText(
                        getApplicationContext(),
                        expandableListTitle.get(groupPosition)
                                + " -> "
                                + expandableListDetail.get(
                                expandableListTitle.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT
                ).show();*/
                return false;
            }
        });


        //timings

        timings_expandableListDetail = getTime(facilityResponse,position);
        timings_expandableListTitle = new ArrayList<String>(timings_expandableListDetail.keySet());
        timingsExpandableListAdapter = new CustomExpandableListAdapter(this, timings_expandableListTitle, timings_expandableListDetail);
        timings_ExpandableListView.setAdapter(timingsExpandableListAdapter);
        timings_ExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
               /* Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();*/
            }
        });

        timings_ExpandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
               /* Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT).show();*/

            }
        });

        timings_ExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
               /* Toast.makeText(
                        getApplicationContext(),
                        expandableListTitle.get(groupPosition)
                                + " -> "
                                + expandableListDetail.get(
                                expandableListTitle.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT
                ).show();*/
                return false;
            }
        });

        book_now_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 displayBookingScreen();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public static HashMap<String, List<String>> getData(FacilityResponse facilityResponse,int position) {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> immunizations = facilityResponse.getFacility().get(position).getServicesProvided().getImmunizations();

        List<String> medicalTests = facilityResponse.getFacility().get(position).getServicesProvided().getMedicalTests();

        List<String> labTests = facilityResponse.getFacility().get(position).getServicesProvided().getLabTests();

        List<String> imagingTests = facilityResponse.getFacility().get(position).getServicesProvided().getImagingTests();

       // List<String> phyiscalTherapy = facilityResponse.getFacility().get(position).getServicesProvided().get(0).getPhyiscalTherapy();

        List<String> injuries = facilityResponse.getFacility().get(position).getServicesProvided().getInjuries();

        List<String> illnessTreatments = facilityResponse.getFacility().get(position).getServicesProvided().getIllnessTreatments();

        List<String> occpWorkRelatedInjuries = facilityResponse.getFacility().get(position).getServicesProvided().getOccpWorkRelatedInjuries();

        List<String> occpDrugAndAlcoholTests = facilityResponse.getFacility().get(position).getServicesProvided().getOccpDrugAndAlcoholTests();

        List<String> occpPhysicals = facilityResponse.getFacility().get(position).getServicesProvided().getOccpPhysicals();

        List<String> otherServices = facilityResponse.getFacility().get(position).getServicesProvided().getOtherServices();

        expandableListDetail.put("Immuunization", immunizations);
        expandableListDetail.put("MedicalTests", medicalTests);
        expandableListDetail.put("Imaging Tests", imagingTests);
        expandableListDetail.put("Lab Tests", labTests);
        expandableListDetail.put("Imaging Tests", imagingTests);
        //expandableListDetail.put("Phyiscal Therapy", phyiscalTherapy);
        expandableListDetail.put("Immuunization", immunizations);
        expandableListDetail.put("injuries", injuries);
        expandableListDetail.put("Illness Treatments", illnessTreatments);
        expandableListDetail.put("Occupational Work Related Injuries", occpWorkRelatedInjuries);
        expandableListDetail.put("Occupational DrugAndAlcohol Tests", occpDrugAndAlcoholTests);
        expandableListDetail.put("Occupational Physicals", occpPhysicals);
        expandableListDetail.put("Other Services", otherServices);

        return expandableListDetail;
    }
    public static HashMap<String, List<String>> getInsuranceDetails(FacilityResponse facilityResponse,int position) {
        HashMap<String, List<String>> insuarnceExpandableListDetail = new HashMap<String, List<String>>();

        List<String> insurancesAccepted = facilityResponse.getFacility().get(position).getInsurancesAccepted();

        insuarnceExpandableListDetail.put("Insurances", insurancesAccepted);

        return insuarnceExpandableListDetail;
    }

    public static HashMap<String, List<String>> getTime(FacilityResponse facilityResponse,int position) {
        HashMap<String, List<String>> timeExpandableListDetail = new HashMap<String, List<String>>();

        List<String> listRegular = facilityResponse.getFacility().get(position).getFacilityWorkingHours().getRegular();

        List<String> occupational = facilityResponse.getFacility().get(position).getFacilityWorkingHours().getOccupational();

        List<String> physicalTherapy = facilityResponse.getFacility().get(position).getFacilityWorkingHours().getPhysicalTherapy();

        timeExpandableListDetail.put("Physical Theraphy", physicalTherapy);

        timeExpandableListDetail.put("occupational", occupational);

        timeExpandableListDetail.put("Regular", listRegular);


        return timeExpandableListDetail;
    }

    public void displayBookingScreen(){
        Intent intent = new Intent(DoctorDetailsActivity.this,AppointmentBooking.class);
        /*Bundle bundle = new Bundle();
        bundle.putParcelable("facilityResponseObj", (Parcelable) facilityResponse);
        intent.putExtras(bundle);*/
        Bundle bundleData = getIntent().getExtras();
        int position = bundleData.getInt("selectedPosition");
        bundleData.putInt("pos",position);
        intent.putExtras(bundleData);
        Utility.facilityResponse = facilityResponse;
        startActivity(intent);
    }
}

