package com.aptrytus.uiactivities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.aptrytus.Others.SharedPreferenceConstants;
import com.aptrytus.Others.Utility;
import com.aptrytus.R;
import com.aptrytus.ViewModels.CreateUserModel;
import com.aptrytus.adapters.SingleAdapter;
import com.aptrytus.adapters.StateAdapter;
import com.aptrytus.models.IndividualDrivingLicense;
import com.aptrytus.models.State;
import com.aptrytus.models.StateList;
import com.aptrytus.repositories.AppSharedPrefs;
import com.aptrytus.request.Address;
import com.aptrytus.request.CreateUserRequest;
import com.aptrytus.request.IdDetails;
import com.aptrytus.services.ApiResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditPersonalInfo extends AppCompatActivity implements View.OnClickListener{


    @BindView(R.id.toolbar_dr_details)
    Toolbar toolbar;

    @BindView(R.id.firstnameLayout)
    TextInputLayout firstnameLayout;
    @BindView(R.id.lastnameLayout)
    TextInputLayout lastnameLayout;
    @BindView(R.id.licenseNumberLayout)
    TextInputLayout licenseNumberLayout;
    @BindView(R.id.addressLayout)
    TextInputLayout addressLayout;
    @BindView(R.id.cityLayout)
    TextInputLayout cityLayout;
    @BindView(R.id.zipcodeLayout)
    TextInputLayout zipcodeLayout;
    @BindView(R.id.dobLayout)
    TextInputLayout dobLayout;

    @BindView(R.id.editFirstName)
    EditText editFirstName;
    @BindView(R.id.editLastName)
    EditText editLastName;
    @BindView(R.id.editLicenseNumber)
    EditText editLicenseNumber;
    @BindView(R.id.editAddress)
    EditText editAddress;
    @BindView(R.id.editCity)
    EditText editCity;
    @BindView(R.id.editZipcode)
    EditText editZipcode;
    @BindView(R.id.editStatesSpinner)
    Spinner editStatesSpinner;
    @BindView(R.id.editDob)
    EditText editDob;
    @BindView(R.id.editGenderSpinner)
    Spinner editGenderSpinner;
    @BindView(R.id.editMaritalStatusSpinner)
    Spinner editMaritalStatusSpinner;

    @BindView(R.id.dateImg)
    ImageView dateImg;

    IndividualDrivingLicense drivingLicense;

    private int mYear, mMonth, mDay, mHour, mMinute;

    List<State> countryList;
    StateList stateList;

    String selectedState = "";
    int editType;

    CreateUserModel createUserModel;
    ProgressDialog progressDialog;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_edit_personal_info);

        editType = getIntent().getExtras().getInt("editType");
        drivingLicense = getIntent().getParcelableExtra("drivingLicense");

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ActionBar actionBar = this.getSupportActionBar();

        actionBar.setTitle("Edit User");

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // Gender
        String spinnerArray[] = {"Male","Female"};
        SingleAdapter spinnerArrayAdapter = new SingleAdapter(this,spinnerArray);
        editGenderSpinner.setAdapter(spinnerArrayAdapter);

        if(drivingLicense.getGender().equals("M")){
            editGenderSpinner.setSelection(0);
        }else{
            editGenderSpinner.setSelection(1);
        }

        String marriedStatusArray[] = {"Married","Single"};
        SingleAdapter maritalSpinner = new SingleAdapter(this,marriedStatusArray);
        editMaritalStatusSpinner.setAdapter(maritalSpinner);

        // currently Driving license is not returning any value for marital status

        /*if(drivingLicense.getMaritalStatus().equals("Married")){
            editMaritalStatusSpinner.setSelection(0);
        }else{
            editMaritalStatusSpinner.setSelection(1);
        }*/


        dateImg.setOnClickListener(this);

        setStateList();

        setUserDetails();
        displayEditable(editType);

        createUserModel = ViewModelProviders.of(this).get(CreateUserModel.class);



    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void setStateList(){
        String json = null;
        try {
            InputStream is = getAssets().open("states.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

             /*stateList = new Gson().fromJson(json,StateList.class);
             Toast.makeText(this,"",Toast.LENGTH_LONG).show();*/
            Gson gson=new Gson();
            // Usinf fromJson(reader,type)
            countryList=gson.fromJson(json,new TypeToken<List<State>>(){}.getType());
            Toast.makeText(this,"--> "+countryList.size(),Toast.LENGTH_LONG).show();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        StateAdapter adapter = new StateAdapter(this,countryList);
        editStatesSpinner.setAdapter(adapter);

        String selectedStateCode = drivingLicense.getAddressState();
        int selectedIndex = 0;

        /*for(State state:countryList){

            if(state.getStateCode().equals(selectedStateCode)){
                int index = state.
            }
        }*/
        for(int i=0;i<countryList.size();i++){
            if(selectedStateCode.equals(countryList.get(i).getStateCode())){
                selectedIndex = i;
            }
        }

        editStatesSpinner.setSelection(selectedIndex);
    }

    public void setUserDetails(){

        editFirstName.setText(drivingLicense.getFirstName());
        editLastName.setText(drivingLicense.getLastName());
        editLicenseNumber.setText(drivingLicense.getLicenseNumber());
        editAddress.setText(drivingLicense.getAddressStreet());
        editCity.setText(drivingLicense.getAddressCity());
        editZipcode.setText(drivingLicense.getAddressZip());
        //editStatesSpinner.setSelection(2);  // hard coded
        editDob.setText(drivingLicense.getBirthDate());
        editGenderSpinner.setSelection(0);
        editMaritalStatusSpinner.setSelection(1);

    }

    public void displayEditable(int type){
        editFirstName.setEnabled(false);
        editStatesSpinner.setEnabled(false);
        addressLayout.setEnabled(false);
        cityLayout.setEnabled(false);
        zipcodeLayout.setEnabled(false);
        firstnameLayout.setEnabled(false);
        lastnameLayout.setEnabled(false);
        licenseNumberLayout.setEnabled(false);
        dobLayout.setEnabled(false);

        if(type == 1){
            addressLayout.setEnabled(true);
            cityLayout.setEnabled(true);
            zipcodeLayout.setEnabled(true);
            editStatesSpinner.setEnabled(true);

            editFirstName.setTextColor(Color.GRAY);
            editLastName.setTextColor(Color.GRAY);
            editLicenseNumber.setTextColor(Color.GRAY);
            editDob.setTextColor(Color.GRAY);



        }else if(type == 2){
            firstnameLayout.setEnabled(true);
            lastnameLayout.setEnabled(true);
            licenseNumberLayout.setEnabled(true);
            dobLayout.setEnabled(true);

            editAddress.setTextColor(Color.GRAY);
            editCity.setTextColor(Color.GRAY);
            editZipcode.setTextColor(Color.GRAY);
            //editStatesSpinner.setTextColor(Color.GRAY);
        }


    }
    
    public void saveChanges(View view){
        
        // update changes here

        drivingLicense.setFirstName(editFirstName.getText().toString());
        drivingLicense.setLastName(editLastName.getText().toString());
        drivingLicense.setLicenseNumber(editLicenseNumber.getText().toString());
        drivingLicense.setBirthDate(editDob.getText().toString());
        drivingLicense.setAddressStreet(editAddress.getText().toString());
        drivingLicense.setAddressCity(editCity.getText().toString());
        drivingLicense.setAddressZip(editZipcode.getText().toString());
        drivingLicense.setAddressState(countryList.get(editStatesSpinner.getSelectedItemPosition()).getStateCode());

        if(editType != 3){
            Intent intent = new Intent();
            intent.putExtra("editPersonaldata", drivingLicense);
            setResult(RESULT_OK,intent);
            finish();
        }else{

            CreateUserRequest createUserRequest = new CreateUserRequest();
            createUserRequest = createUserRequestObject(createUserRequest);

            progressDialog = Utility.getProgressDialog(this, "Please wait...");
            progressDialog.show();
            createUserModel.registerUser(createUserRequest).observe(this, new Observer<ApiResponse>() {
                @Override
                public void onChanged(@Nullable ApiResponse apiResponse) {

                    if (apiResponse.createUserResponse != null) {

                        String msg = apiResponse.createUserResponse.getMessage();
                        Toast.makeText(getApplicationContext(), " " + msg, Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent();
                        intent.putExtra("editPersonaldata", drivingLicense);
                        setResult(RESULT_OK,intent);
                        finish();
                        /*Log.i("Response received ",apiResponse.createUserResponse.getMessage().toString());*/
                    } else {
                        String message = Utility.getApiError(apiResponse);
                        Toast.makeText(getApplicationContext(), "Error received " + message, Toast.LENGTH_SHORT).show();
                    }

                    progressDialog.dismiss();
                }
            });

        }


        
    }

    private CreateUserRequest createUserRequestObject(CreateUserRequest createUserRequest) {

        createUserRequest.setFirstName(drivingLicense.getFirstName());
        createUserRequest.setLastName(drivingLicense.getLastName());
        createUserRequest.setMiddleName(drivingLicense.getMiddleName());
        if (drivingLicense.getMaritalStatus() == null) {
            createUserRequest.setMaritalStatus("Single");
        } else {
            createUserRequest.setMaritalStatus(drivingLicense.getMaritalStatus());
        }

        IdDetails idDetails = new IdDetails();

        idDetails.setNumber(drivingLicense.getLicenseNumber());
        idDetails.setType(drivingLicense.getDocumentType());
        idDetails.setIdIssuedState(drivingLicense.getAddressState());


        if (drivingLicense.getIssueDate() != null) {

            idDetails.setValidFromDate(Utility.getFormatedDate(drivingLicense.getIssueDate()));
            idDetails.setValidToDate(Utility.getFormatedDate(drivingLicense.getExpiryDate()));
        } else {
           // idDetails.setIdIssuedState("");
            idDetails.setValidFromDate("");
            idDetails.setValidToDate("");
        }

        /*createUserRequest.setDriverLicenseNumber(drivingLicense.getLicenseNumber());
        createUserRequest.setDriverLicenseValidFromDate(Utility.getFormatedDate(drivingLicense.getIssueDate()));
        createUserRequest.setDriverLicenseValidToDate(Utility.getFormatedDate(drivingLicense.getExpiryDate()));*/
        createUserRequest.setIdDetails(idDetails);

        String genderVal = "M";
        if (drivingLicense.getGender().equals("2")) {
            genderVal = "F";
        }

        createUserRequest.setGender(genderVal);
        createUserRequest.setDateOfBirth(Utility.getFormatedDate(drivingLicense.getBirthDate()));

        String loggedInUserId = AppSharedPrefs.getInstance(this).getName(SharedPreferenceConstants.LOGEED_IN_USER_ID);
        String mailId = AppSharedPrefs.getInstance(this).getName(SharedPreferenceConstants.LOGEED_IN_USER_EMAIL_ID);
        String loggedInMobileNumber = AppSharedPrefs.getInstance(this).getName(SharedPreferenceConstants.LOGEED_IN_USER_MOBILE);

        createUserRequest.setId(loggedInUserId);
        createUserRequest.setEmail(mailId);

        //String phone = Utility.formatUSPhoneNum(loggedInMobileNumber);
        createUserRequest.setPrimaryPhone(loggedInMobileNumber);
        createUserRequest.setUserId(mailId);

        /*createUserRequest.setIsGuest("0");
        createUserRequest.setOtpVerfCode("");
        createUserRequest.setOtpCreateDateTime("2018-12-29T06:06:54.416+0000");
        createUserRequest.setUserStatus("active");
        createUserRequest.setPrimaryUser(true);*/

        // address
        Address address = new Address();
        address.setStreetAddress(drivingLicense.getAddressStreet());
        address.setCity(drivingLicense.getAddressCity());
        address.setState(drivingLicense.getAddressState());
        address.setZipcode(drivingLicense.getAddressZip());
        address.setCountry(drivingLicense.getIssuingCountry());

        createUserRequest.setAddress(address);
        return  createUserRequest;
    }

    /*private int getAge(String dobString){

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month+1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }



        return age;
    }*/

    @Override
    public void onClick(View v) {

        if (v == dateImg) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            String dobFromLicense = drivingLicense.getBirthDate();

            if(editType == 3){

                mYear = Integer.parseInt(dobFromLicense.substring(0,4));
                mMonth = Integer.parseInt(dobFromLicense.substring(5,7));
                mDay = Integer.parseInt(dobFromLicense.substring(7,9));

            }else{
                mYear = Integer.parseInt(dobFromLicense.substring(4));
                mMonth = Integer.parseInt(dobFromLicense.substring(0,2));
                mDay = Integer.parseInt(dobFromLicense.substring(2,4));
            }




            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            String months ="";
                            if(monthOfYear < 9){
                                int monthNum = monthOfYear+1;
                                months = "0"+monthNum;

                            }else{
                                int monthNum = monthOfYear+1;
                                months = ""+monthNum;
                            }

                            editDob.setText(year + "-" + months + "-" +dayOfMonth );

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }

    }
}
