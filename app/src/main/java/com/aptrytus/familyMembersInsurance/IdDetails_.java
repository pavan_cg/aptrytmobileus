
package com.aptrytus.familyMembersInsurance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IdDetails_ {

    @SerializedName("validFromDate")
    @Expose
    private String validFromDate;
    @SerializedName("validToDate")
    @Expose
    private String validToDate;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("idIssuedState")
    @Expose
    private String idIssuedState;
    @SerializedName("number")
    @Expose
    private String number;

    public String getValidFromDate() {
        return validFromDate;
    }

    public void setValidFromDate(String validFromDate) {
        this.validFromDate = validFromDate;
    }

    public String getValidToDate() {
        return validToDate;
    }

    public void setValidToDate(String validToDate) {
        this.validToDate = validToDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdIssuedState() {
        return idIssuedState;
    }

    public void setIdIssuedState(String idIssuedState) {
        this.idIssuedState = idIssuedState;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

}
