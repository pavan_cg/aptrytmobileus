
package com.aptrytus.familyMembersInsurance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FamilyMemberInsuranceResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userInfoResponse")
    @Expose
    private UserInfoResponse userInfoResponse;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserInfoResponse getUserInfoResponse() {
        return userInfoResponse;
    }

    public void setUserInfoResponse(UserInfoResponse userInfoResponse) {
        this.userInfoResponse = userInfoResponse;
    }

}
