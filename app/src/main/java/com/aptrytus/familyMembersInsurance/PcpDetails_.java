
package com.aptrytus.familyMembersInsurance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PcpDetails_ {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("contact")
    @Expose
    private String contact;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

}
