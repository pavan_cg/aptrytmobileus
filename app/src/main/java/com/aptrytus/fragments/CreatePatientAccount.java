package com.aptrytus.fragments;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.aptrytus.R;
import com.aptrytus.ViewModels.SendOtpModel;
import com.aptrytus.ViewModels.TextSuggestionModel;
import com.aptrytus.services.ApiResponse;

import static android.support.constraint.Constraints.TAG;

public class CreatePatientAccount extends Fragment {

    View view;
    Button registerBtn;
    SendOtpModel sendOtpModel;

    public static CreatePatientAccount newInstance(int someInt, String someTitle){

        CreatePatientAccount patientAccount = new CreatePatientAccount();
        Bundle args = new Bundle();
        args.putInt("someInt",someInt);
        args.putString("someTitle",someTitle);
        patientAccount.setArguments(args);
        return patientAccount;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //  Get the arguments supplied
        int someInt = getArguments().getInt("someInt");
        String someTitle = getArguments().getString("someTitle");

        sendOtpModel = ViewModelProviders.of(getActivity()).get(SendOtpModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_create_patient, container, false);

        registerBtn = view.findViewById(R.id.registerBtn);



        /*registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getOtpData();
            }
        });*/
        return view;
    }

    /*public void getOtpData(){
        sendOtpModel.getOtp("Nagendra","919916270211",false,"PATIENT").observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                if (apiResponse == null) {
                    // handle error here
                    return;
                }
                if (apiResponse.getError() == null) {
                    // call is successful
                    //setTextList(apiResponse.getTextSuggestions());
                    //progressBar.setVisibility(View.GONE);
                    //hideLoading();
                    Log.i(TAG, "Data response is " + apiResponse.getTextSuggestions());
                } else {
                    // call failed.
                    Throwable e = apiResponse.getError();
                    Toast.makeText(getActivity(), "Error is " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Error is " + e.getLocalizedMessage());
                    //hideLoading();

                }
            }

        });
    }*/
}

