package com.aptrytus.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aptrytus.Others.SharedPreferenceConstants;
import com.aptrytus.Others.Utility;
import com.aptrytus.R;
import com.aptrytus.ViewModels.LoginViewModel;
import com.aptrytus.ViewModels.TextSuggestionModel;
import com.aptrytus.adapters.TextSuggestionAdapter;
import com.aptrytus.login.LoginResponse;
import com.aptrytus.models.TextSuggestion;
import com.aptrytus.models.TextSuggestionsResponse;
import com.aptrytus.repositories.AppSharedPrefs;
import com.aptrytus.services.ApiResponse;
import com.aptrytus.uiactivities.DoctorListActivity;
import com.aptrytus.uiactivities.OtpVerificationActivity;
import com.aptrytus.uiactivities.PersonalInfoActivity;
import com.aptrytus.uiactivities.PopuActivity;
import com.aptrytus.uiactivities.UserCreationActivity;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class TextSearchFragment extends Fragment implements TextSuggestionAdapter.OnItemClickListener {

    private static final String TAG = "@@@@@#######::";
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static EditText search_directions;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 111;
    private FusedLocationProviderClient mFusedLocationClient;
    private Location mLastLocation;
    SearchView textSuggestionSearchView;
    private RecyclerView recyclerView;
    private List<TextSuggestion> data;
    private LinearLayout linearLayoutChild = null;
    private LinearLayout ll_botton_view_one;
    TextSuggestionAdapter mAdapter;
    OnTextChangedSearchView mCallback;
    private Button createAccount;
    private Button loginButton;
    View view;
    String loggedInUserId;
    private Button doctorSearch_btn;

    private ProgressDialog progressDialog;

    TextSuggestionModel textSuggestionModel;

    public static String selectedZipCode="";
    public static String selectedCity="";
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    LoginViewModel loginViewModel;
    LoginResponse loginResponse;

    public TextSearchFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        progressDialog = new ProgressDialog(this.getActivity());

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_text_search, container, false);

        textSuggestionSearchView = (SearchView) view.findViewById(R.id.suggestionSearch);
        textSuggestionSearchView.setQueryHint("Provides,Specialities,Facilities");
        //Shared Preferences
        pref = getActivity().getSharedPreferences(Utility.APTRY_PREF, 0);
        editor = pref.edit();

        search_directions = (EditText) view.findViewById(R.id.search_directions);

        //progressBar = view.findViewById(R.id.progressBar);

        linearLayoutChild = (LinearLayout) view.findViewById(R.id.ll_botton_view);
        ll_botton_view_one = view.findViewById(R.id.ll_botton_view_one);
        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        //    mAdapter =  new TextSuggestionAdapter(data);
        //    recyclerView.setAdapter(mAdapter);


        textSuggestionSearchView.setActivated(true);

        textSuggestionSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.i("Search Clicked", "Test");
                //displayDoctorsList();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.i("onQueryTextChange Back", ":" + s);
                if (s.length() >= 3) {
                    //Toast.makeText(getActivity(),"s"+s,Toast.LENGTH_LONG).show();
                    recyclerView.setVisibility(View.VISIBLE);
                    search_directions.setVisibility(View.GONE);
                    linearLayoutChild.setVisibility(View.GONE);
                    ll_botton_view_one.setVisibility(View.GONE);
                    doctorSearch_btn.setVisibility(View.GONE);
                    //progressBar.setVisibility(View.VISIBLE);
                    displayLoading();
                    getTextSuggestions(s, selectedZipCode, selectedCity);
                } else {
                    recyclerView.setVisibility(View.GONE);
                    search_directions.setVisibility(View.VISIBLE);
                    linearLayoutChild.setVisibility(View.VISIBLE);
                    if(loggedInUserId!=""){
                        ll_botton_view_one.setVisibility(View.INVISIBLE);
                    }else {
                        ll_botton_view_one.setVisibility(View.VISIBLE);
                    }
                    doctorSearch_btn.setVisibility(View.VISIBLE);
                    //progressBar.setVisibility(View.GONE);
                    hideLoading();
                }
                return false;
            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        search_directions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .build(getActivity());
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
            }
        });

        createAccount = view.findViewById(R.id.btCreateAccount);

        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Intent intent = new Intent(getContext(),PopuActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("FRAGMENT_FLAG",1);
                intent.putExtras(bundle);
                startActivity(intent);*/
                AppSharedPrefs.getInstance(getActivity()).setBoolean(SharedPreferenceConstants.SCAN_FROM_SETTINGS,false);
                Intent intent = new Intent(getContext(), UserCreationActivity.class);
                startActivity(intent);
                /*Intent intent = new Intent(getContext(), PersonalInfoActivity.class);
                startActivity(intent);*/

            }
        });

        loginButton = view.findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(),PopuActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("FRAGMENT_FLAG",0);
                intent.putExtras(bundle);
                startActivity(intent);

                //getOTPValidation("","","");

            }
        });

        doctorSearch_btn = view.findViewById(R.id.doctor_search_button);

        doctorSearch_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayDoctorsList();
            }
        });

        initViewModel();
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        loginResponse = loginViewModel.getLoginDetails();

        if(loginResponse==null){

        }

        loggedInUserId = AppSharedPrefs.getInstance(getActivity()).getName(SharedPreferenceConstants.LOGEED_IN_USER_ID);
        String loggedInUserName = AppSharedPrefs.getInstance(getActivity()).getName(SharedPreferenceConstants.LOGEED_IN_USER_NAME);
        String loggedInUserMobile = AppSharedPrefs.getInstance(getActivity()).getName(SharedPreferenceConstants.LOGEED_IN_USER_MOBILE);
        if(loggedInUserId!=""){
            ll_botton_view_one.setVisibility(View.INVISIBLE);
        }
        return view;
    }

    // method for  initialize the View Model

    public void initViewModel() {

        textSuggestionModel = ViewModelProviders.of(this).get(TextSuggestionModel.class);
    }

    private void getTextSuggestions(String s, String zipcode, String city) {

        /*textSuggestionModel.getTextSuggestions(s).observe(this, new Observer<TextSuggestionsResponse>() {
            @Override
            public void onChanged(@Nullable TextSuggestionsResponse textSuggestionsResponse) {
                //progress_bar.setVisibility(View.GONE);
                setTextList(textSuggestionsResponse);
                progressBar.setVisibility(View.GONE);

            }
        });*/
        textSuggestionModel.getTextSuggestions(s, city,zipcode).observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {

                if (apiResponse != null) {

                    if (apiResponse.getTextSuggestions().size() == 0) {
                        makeViewsVisible();
                    } else {
                        setTextList(apiResponse.getTextSuggestions());
                    }
                    /*Log.i("Response received ",apiResponse.createUserResponse.getMessage().toString());*/
                } else {
                    String message = Utility.getApiError(apiResponse);
                    Toast.makeText(getActivity(), "Error received " + message, Toast.LENGTH_SHORT).show();
                    makeViewsVisible();
                }
                hideLoading();
                /*if (apiResponse == null) {
                    // handle error here
                    makeViewsVisible();
                    return;
                }
                if (apiResponse.getError() == null) {
                    // call is successful
                    if (apiResponse.getTextSuggestions().size() == 0) {
                        makeViewsVisible();
                    } else {
                        setTextList(apiResponse.getTextSuggestions());
                    }
                    //progressBar.setVisibility(View.GONE);
                    hideLoading();
                    Log.i(TAG, "Data response is " + apiResponse.getTextSuggestions());
                } else {
                    // call failed.
                    Throwable e = apiResponse.getError();
                    Log.e(TAG, "Error is " + e.getLocalizedMessage());
                    hideLoading();

                }*/
            }

        });

    }

    public void setTextList(List<TextSuggestionsResponse> textSuggestionsResponse) {
        mAdapter = new TextSuggestionAdapter(textSuggestionsResponse, this);
        recyclerView.setAdapter(mAdapter);
    }

    private BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                    //progress_bar.setVisibility(View.VISIBLE);
                    //updatePhotosList();
                } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
                    Toast.makeText(getContext(), "Please check Internet connection", Toast.LENGTH_LONG).show();
                    //progress_bar.setVisibility(View.GONE);
                }
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                search_directions.setText("" + place.getName());
                try {
                    getPlaceInfo(place.getLatLng().latitude, place.getLatLng().longitude);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!checkPermissions()) {
            Log.i(TAG, "Inside onStart function; requesting permission when permission is not available");
            requestPermissions();
        } else {
            Log.i(TAG, "Inside onStart function; getting location when permission is already available");
            getLastLocation();
        }
    }

    //Return whether permissions is needed as boolean value.
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    //Request permission from user
    private void requestPermissions() {
        Log.i(TAG, "Inside requestPermissions function");
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION);

        //Log an additional rationale to the user. This would happen if the user denied the
        //request previously, but didn't check the "Don't ask again" checkbox.
        // In case you want, you can also show snackbar. Here, we used Log just to clear the concept.
        if (shouldProvideRationale) {
            Log.i(TAG, "****Inside requestPermissions function when shouldProvideRationale = true");
            startLocationPermissionRequest();
        } else {
            Log.i(TAG, "****Inside requestPermissions function when shouldProvideRationale = false");
            startLocationPermissionRequest();
        }
    }

    //Start the permission request dialog
    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    /**
     * Callback to the following function is received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // user interaction is cancelled; in such case we will receive empty grantResults[]
                //In such case, just record/log it.
                Log.i(TAG, "User interaction has been cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted by the user.
                Log.i(TAG, "User permission has been given. Now getting location");
                getLastLocation();
            } else {
                // Permission is denied by the user.
                Log.i(TAG, "User denied permission.");
            }
        }
    }

    /**
     * This method should be called after location permission is granted. It gets the recently available location,
     * In some situations, when location, is not available, it may produce null result.
     * WE used SuppressWarnings annotation to avoid the missing permission warnng. You can comment the annotation
     * and check the behaviour yourself.
     */
    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            Geocoder gcd = new Geocoder(getActivity(), Locale.getDefault());
                            try {
                                List<Address> geoAddresses = gcd.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
                                if (geoAddresses.size() > 0) {
                                    String mUserLocation = "";
                                    for (int i = 0; i < 4; i++) { //Since it return only four value we declare this as static.
                                        mUserLocation = mUserLocation + geoAddresses.get(0).getAddressLine(i).replace(",", "") + "";
                                        search_directions.setText(mUserLocation);
                                        selectedZipCode = geoAddresses.get(0).getPostalCode();
                                        selectedCity = geoAddresses.get(0).getLocality();
                                    }
                                }
                            } catch (Exception e) {

                            }
                        } else {
                            Log.i(TAG, "Inside getLocation function. Error while getting location");
                            System.out.println(TAG + task.getException());
                        }
                    }
                });
    }

    @Override
    public void onItemClick(String test, int position) {

        CharSequence charSequence = test;
        textSuggestionSearchView.setQuery(test, true);
        makeViewsVisible();
    }

    public void makeViewsVisible() {
        recyclerView.setVisibility(View.GONE);
        search_directions.setVisibility(View.VISIBLE);
        linearLayoutChild.setVisibility(View.VISIBLE);
        if(loggedInUserId!=""){
            ll_botton_view_one.setVisibility(View.INVISIBLE);
        }else{
            ll_botton_view_one.setVisibility(View.VISIBLE);
        }
        doctorSearch_btn.setVisibility(View.VISIBLE);

    }

    public interface OnTextChangedSearchView {
        public void onSearchViewChanges(String text);
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        /*try {
            mCallback = (OnTextChangedSearchView) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement TextSearchFragment");
        }*/
    }

    public void displayDoctorsList() {

        editor.putString(Utility.SEARHED_TEXT, textSuggestionSearchView.getQuery().toString().trim());
        editor.putBoolean(Utility.DATAFROMSEARCHFRAGMENT, true);
        editor.putString(Utility.CITY, selectedCity);
        editor.putString(Utility.ZIPCODE, selectedZipCode);
        editor.commit();
        Intent intent = new Intent(getActivity(), DoctorListActivity.class);
        startActivity(intent);

    }

    public void displayLoading() {
        //progressBar.setVisibility(View.VISIBLE);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void hideLoading() {
        //progressBar.setVisibility(View.GONE);
        progressDialog.dismiss();
    }

    private void getPlaceInfo(double lat, double lon) throws IOException {
        Geocoder mGeocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses.get(0).getPostalCode() != null) {
            String ZIP = addresses.get(0).getPostalCode();
            selectedZipCode = ZIP;
            Log.d("ZIP CODE", ZIP);
        }

        if (addresses.get(0).getLocality() != null) {
            String city = addresses.get(0).getLocality();
            Log.d("CITY", city);
            selectedCity = city;
        }

        if (addresses.get(0).getAdminArea() != null) {
            String state = addresses.get(0).getAdminArea();
            Log.d("STATE", state);
        }

        if (addresses.get(0).getCountryName() != null) {
            String country = addresses.get(0).getCountryName();
            Log.d("COUNTRY", country);
        }
    }

    public String readJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getContext().getAssets().open("states.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void getOTPValidation(String id, String userId, String phNumber){

        Intent otpIntent = new Intent(this.getActivity(),OtpVerificationActivity.class);
        startActivity(otpIntent);

    }

    public void onResume(){
        super.onResume();
        loggedInUserId = AppSharedPrefs.getInstance(getActivity()).getName(SharedPreferenceConstants.LOGEED_IN_USER_ID);
        if(loggedInUserId!=""){
            ll_botton_view_one.setVisibility(View.INVISIBLE);
        }
    }

}
