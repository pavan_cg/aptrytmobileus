package com.aptrytus.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.aptrytus.Others.SharedPreferenceConstants;
import com.aptrytus.Others.Utility;
import com.aptrytus.R;
import com.aptrytus.ViewModels.DoctorsListModel;
import com.aptrytus.familyMembersInsurance.FamilyMemberInsuranceResponse;
import com.aptrytus.models.IndividualDrivingLicense;
import com.aptrytus.repositories.AppSharedPrefs;
import com.aptrytus.services.ApiResponse;
import com.aptrytus.uiactivities.AddFamilyActivity;
import com.aptrytus.uiactivities.AddInsuranceActivity;
import com.aptrytus.uiactivities.EditPersonalInfo;
import com.aptrytus.uiactivities.PopuActivity;
import com.aptrytus.uiactivities.UserCreationActivity;

public class SettingsFragment extends Fragment {

    View view;
    signoutLoadTextSearch mCallback;
    private LinearLayout signout_layout;
    private LinearLayout signin_layout;
    private Button signoutBtn;
    private Button signinBtn;
    private Button signupBtn;
    private LinearLayout me_layout;
    private LinearLayout family_layout;
    private LinearLayout insurance_layout;
    String userEmailId;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    DoctorsListModel doctorViewModel;
    ProgressDialog progressDialog;

    int EDIT_PERSONAL_INFO = 3;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.settings, container, false);
        signout_layout = (LinearLayout) view.findViewById(R.id.ll_settings_signout);
        signin_layout = (LinearLayout) view.findViewById(R.id.ll_settings_signin);

        signoutBtn = (Button) view.findViewById(R.id.btsignout);
        signinBtn = (Button) view.findViewById(R.id.settingsLoginButton);
        signupBtn = (Button) view.findViewById(R.id.btSettingsCreateAccount);
        me_layout = (LinearLayout) view.findViewById(R.id.ll_settings_me);
        family_layout = (LinearLayout) view.findViewById(R.id.ll_settings_family);
        insurance_layout = (LinearLayout) view.findViewById(R.id.ll_settings_insurance);

        if(AppSharedPrefs.getInstance(getActivity()).getName(SharedPreferenceConstants.LOGEED_IN_USER_ID) ==""){
            signout_layout.setVisibility(View.GONE);
            signin_layout.setVisibility(View.VISIBLE);
        }else{
            signout_layout.setVisibility(View.VISIBLE);
            signin_layout.setVisibility(View.GONE);
        }

        signoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signoutDialog();
            }
        });

        signinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),PopuActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("FRAGMENT_FLAG",0);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppSharedPrefs.getInstance(getActivity()).setBoolean(SharedPreferenceConstants.SCAN_FROM_SETTINGS,false);
                Intent intent = new Intent(getContext(), UserCreationActivity.class);
                startActivity(intent);

            }
        });

        me_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if (userEmailId != null)
                    displayFamilyDetailsList(userEmailId);

            }
        });

        family_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intObj = new Intent(getActivity(), AddFamilyActivity.class);
                startActivity(intObj);
            }
        });

        insurance_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent addInsuranceIntent = new Intent(getActivity(), AddInsuranceActivity.class);
                startActivity(addInsuranceIntent);
                //startActivityForResult(addInsuranceIntent, ADD_INSURANCE);
            }
        });

        doctorViewModel = ViewModelProviders.of(this).get(DoctorsListModel.class);
        userEmailId = AppSharedPrefs.getInstance(getActivity()).getName(SharedPreferenceConstants.LOGEED_IN_USER_EMAIL_ID);


        return view;
    }

    private void signoutDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you wish to sign out ?")
                .setCancelable(false)

                .setPositiveButton("SIGN OUT",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                AppSharedPrefs.getInstance(getActivity()).clearData();
                                AppSharedPrefs.getInstance(getActivity()).setBoolean(SharedPreferenceConstants.CHECK_APP_INSTALLED,true);
                                mCallback.onTextSearchFragmentSelected();
                            }
                        })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });

        final AlertDialog alert = builder.create();
        alert.show();
    }


    // Container Activity must implement this interface
    public interface signoutLoadTextSearch {
        public void onTextSearchFragmentSelected();
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (signoutLoadTextSearch) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement IFragmentToActivity");
        }
    }

    public void displayFamilyDetailsList(String userEmailId) {

        progressDialog = Utility.getProgressDialog(getActivity(), "Please wait...");
        progressDialog.show();


        doctorViewModel.getFamilyMebersList(userEmailId).observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                progressDialog.dismiss();
                if (apiResponse.familyMemberInsuranceResponse != null) {
                    FamilyMemberInsuranceResponse response = apiResponse.familyMemberInsuranceResponse;
                    setDrivingLiscense(response);
                    // display the data into edit screen here
                    //displayFamilyList(response);
                } else {
                    String message = Utility.getApiError(apiResponse);
                    Toast.makeText(getActivity(), "Error received " + message, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void setDrivingLiscense(FamilyMemberInsuranceResponse response) {

        //IndividualDrivingLicense drivingLicense;

        //editStatesSpinner.setSelection(0);
        IndividualDrivingLicense dl = new IndividualDrivingLicense();
        dl.setFirstName(response.getUserInfoResponse().getFirstName());
        dl.setLastName(response.getUserInfoResponse().getLastName());
        //dl.setLicenseNumber(response.getUserInfoResponse().getIdDetails().getNumber());
        dl.setAddressStreet(response.getUserInfoResponse().getAddress().getStreet());
        dl.setAddressCity(response.getUserInfoResponse().getAddress().getCity());
        dl.setAddressZip(""+response.getUserInfoResponse().getAddress().getZipcode());
        dl.setBirthDate(response.getUserInfoResponse().getDateOfBirth());
        dl.setAddressState(response.getUserInfoResponse().getAddress().getState());
        dl.setDocumentType("DL");
        dl.setLicenseNumber(response.getUserInfoResponse().getIdDetails().getNumber());
        dl.setIssuingCountry(response.getUserInfoResponse().getAddress().getCountry());
        dl.setIssueDate(response.getUserInfoResponse().getIdDetails().getValidFromDate());
        dl.setExpiryDate(response.getUserInfoResponse().getIdDetails().getValidToDate());

        /*String genderTxt = editGenderSpinner.getSelectedItem().toString();
        String gender = "M";
        if (genderTxt.equals("Male")) {
            gender = "M";
        } else {
            gender = "F";
        }*/
        dl.setGender(response.getUserInfoResponse().getGender());
        dl.setMaritalStatus(response.getUserInfoResponse().getMaritalStatus());



        Intent editPersonaInfoIntent = new Intent(getActivity(), EditPersonalInfo.class);
        Bundle bundle = new Bundle();
        bundle.putInt("editType", 3);
        bundle.putParcelable("drivingLicense", (Parcelable) dl);
        editPersonaInfoIntent.putExtras(bundle);
        startActivityForResult(editPersonaInfoIntent, EDIT_PERSONAL_INFO);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
