package com.aptrytus.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.aptrytus.R;
import com.aptrytus.uiactivities.RegisterUserActivity;

public class CreateAccountFragment extends Fragment {

    Button nextButton;
    View view;

    public static CreateAccountFragment newInstance(){
        CreateAccountFragment createAccountFragment = new CreateAccountFragment();
        return createAccountFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_create_account, container, false);

        nextButton = view.findViewById(R.id.nextButton);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RegisterUserActivity.class);

                Bundle bundle = new Bundle();
                bundle.putInt("FRAGMENT_FLAG",0);
                intent.putExtras(bundle);

                startActivity(intent);
            }
        });

        return view;


    }


}

