package com.aptrytus.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import android.widget.Toast;

import com.aptrytus.Others.SharedPreferenceConstants;
import com.aptrytus.Others.Utility;
import com.aptrytus.R;
import com.aptrytus.ViewModels.LoginViewModel;

import com.aptrytus.login.LoginRequest;
import com.aptrytus.login.LoginResponse;
import com.aptrytus.repositories.AppSharedPrefs;
import com.aptrytus.services.ApiResponse;

public class LoginFragment extends Fragment {

    Button loginBtn;

    EditText userName;

    EditText password;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    ProgressDialog progressDialog;

    LoginViewModel loginViewModel;

    LoginResponse loginResponse;

    public static LoginFragment newInstance(int someInt, String someTitle){

        LoginFragment loginFragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putInt("someInt",someInt);
        args.putString("someTitle",someTitle);
        loginFragment.setArguments(args);
        return loginFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //  Get the arguments supplied
        int someInt = getArguments().getInt("someInt");
        String someTitle = getArguments().getString("someTitle");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View view;
        view = inflater.inflate(R.layout.fragment_login, container, false);

        loginBtn = view.findViewById(R.id.loginBtn);

        userName = view.findViewById(R.id.login_username);

        password = view.findViewById(R.id.login_password);

        progressDialog = Utility.getProgressDialog(getActivity(), "Please wait...");

        initViewModel();

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userName.getText().toString().equals("") || password.getText().toString().equals("")){
                    Toast.makeText(getActivity(),"Please enter Username/Password",Toast.LENGTH_LONG).show();
                }else if (!userName.getText().toString().matches(emailPattern))
                {
                    Toast.makeText(getActivity(),"Invalid email address",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.setUserId(userName.getText().toString());
                    loginRequest.setPassword(password.getText().toString());
                    loginServiceRequest(loginRequest);
                }

            }
        });

        return view;
    }
    // method for  initialize the View Model

    public void initViewModel() {
        loginViewModel = ViewModelProviders.of((FragmentActivity) getActivity()).get(LoginViewModel.class);
    }


    public void loginServiceRequest(LoginRequest loginRequest){
        progressDialog = Utility.getProgressDialog(getActivity(), "Please wait...");
        progressDialog.show();
        loginViewModel = ViewModelProviders.of((FragmentActivity) getActivity()).get(LoginViewModel.class);
        loginViewModel.loginUser(loginRequest).observe((LifecycleOwner) getActivity(), new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                progressDialog.dismiss();
                //loginResponse = apiResponse.loginResponse;
                if (apiResponse.loginResponse != null) {

                    Utility.loginResponse = apiResponse.loginResponse;

                    String msg = apiResponse.loginResponse.getMessage();
                    String id = apiResponse.loginResponse.getResponseObject().getId();
                    String userId = apiResponse.loginResponse.getResponseObject().getUserId();
                    String primaryPhNo = apiResponse.loginResponse.getResponseObject().getPrimaryPhone();
                    String userName = apiResponse.loginResponse.getResponseObject().getFirstName();

                    AppSharedPrefs.getInstance(getActivity()).setName(SharedPreferenceConstants.LOGEED_IN_USER_ID,id);
                    AppSharedPrefs.getInstance(getActivity()).setName(SharedPreferenceConstants.LOGEED_IN_USER_NAME,userName);
                    AppSharedPrefs.getInstance(getActivity()).setName(SharedPreferenceConstants.LOGEED_IN_USER_MOBILE,primaryPhNo);
                    AppSharedPrefs.getInstance(getActivity()).setName(SharedPreferenceConstants.LOGEED_IN_USER_EMAIL_ID,userId);

                    Toast.makeText(getActivity(), " " + msg, Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                    /*Log.i("Response received ",apiResponse.createUserResponse.getMessage().toString());*/
                } else {
                    String message = Utility.getApiError(apiResponse);
                    Toast.makeText(getActivity(), "Error received " + message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
