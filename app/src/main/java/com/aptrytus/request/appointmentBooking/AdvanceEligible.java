
package com.aptrytus.request.appointmentBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class AdvanceEligible {

    @SerializedName("isAdvance")
    @Expose
    private Boolean isAdvance;
    @SerializedName("patientAcceptance")
    @Expose
    private Boolean patientAcceptance;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Boolean getIsAdvance() {
        return isAdvance;
    }

    public void setIsAdvance(Boolean isAdvance) {
        this.isAdvance = isAdvance;
    }

    public Boolean getPatientAcceptance() {
        return patientAcceptance;
    }

    public void setPatientAcceptance(Boolean patientAcceptance) {
        this.patientAcceptance = patientAcceptance;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
