
package com.aptrytus.request.appointmentBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class AppointmentBookingRequest {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("userInfoId")
    @Expose
    private String userInfoId;
    @SerializedName("selfPay")
    @Expose
    private Boolean selfPay;
    @SerializedName("insuranceInfo")
    @Expose
    private InsuranceInfo insuranceInfo;
    @SerializedName("advanceEligible")
    @Expose
    private AdvanceEligible advanceEligible;
    @SerializedName("facilityId")
    @Expose
    private String facilityId;
    @SerializedName("appointment")
    @Expose
    private Appointment appointment;

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(String userInfoId) {
        this.userInfoId = userInfoId;
    }

    public Boolean getSelfPay() {
        return selfPay;
    }

    public void setSelfPay(Boolean selfPay) {
        this.selfPay = selfPay;
    }

    public InsuranceInfo getInsuranceInfo() {
        return insuranceInfo;
    }

    public void setInsuranceInfo(InsuranceInfo insuranceInfo) {
        this.insuranceInfo = insuranceInfo;
    }

    public AdvanceEligible getAdvanceEligible() {
        return advanceEligible;
    }

    public void setAdvanceEligible(AdvanceEligible advanceEligible) {
        this.advanceEligible = advanceEligible;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
