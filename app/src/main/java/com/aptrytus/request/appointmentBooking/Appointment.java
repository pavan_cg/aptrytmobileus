
package com.aptrytus.request.appointmentBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class Appointment {

    @SerializedName("dateTime")
    @Expose
    private String dateTime;
    @SerializedName("proposedWaitTime")
    @Expose
    private String proposedWaitTime;
    @SerializedName("purposeOfVisit")
    @Expose
    private String purposeOfVisit;
    @SerializedName("preferredSlot")
    @Expose
    private Integer preferredSlot;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getProposedWaitTime() {
        return proposedWaitTime;
    }

    public void setProposedWaitTime(String proposedWaitTime) {
        this.proposedWaitTime = proposedWaitTime;
    }

    public String getPurposeOfVisit() {
        return purposeOfVisit;
    }

    public void setPurposeOfVisit(String purposeOfVisit) {
        this.purposeOfVisit = purposeOfVisit;
    }

    public Integer getPreferredSlot() {
        return preferredSlot;
    }

    public void setPreferredSlot(Integer preferredSlot) {
        this.preferredSlot = preferredSlot;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
