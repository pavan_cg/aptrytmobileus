
package com.aptrytus.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Insurance implements Serializable{

    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("insuranceName")
    @Expose
    private String insuranceName;

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Insurance withPriority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public String getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
    }

    public Insurance withInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
        return this;
    }

}
