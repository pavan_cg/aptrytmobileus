package com.aptrytus.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IdDetails {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("validFromDate")
    @Expose
    private String validFromDate;
    @SerializedName("validToDate")
    @Expose
    private String validToDate;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("idIssuedState")
    @Expose
    private String idIssuedState;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public IdDetails withNumber(String number) {
        this.number = number;
        return this;
    }

    public String getValidFromDate() {
        return validFromDate;
    }

    public void setValidFromDate(String validFromDate) {
        this.validFromDate = validFromDate;
    }

    public IdDetails withValidFromDate(String validFromDate) {
        this.validFromDate = validFromDate;
        return this;
    }

    public String getValidToDate() {
        return validToDate;
    }

    public void setValidToDate(String validToDate) {
        this.validToDate = validToDate;
    }

    public IdDetails withValidToDate(String validToDate) {
        this.validToDate = validToDate;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public IdDetails withType(String type) {
        this.type = type;
        return this;
    }

    public String getIdIssuedState() {
        return idIssuedState;
    }

    public void setIdIssuedState(String idIssuedState) {
        this.idIssuedState = idIssuedState;
    }

    public IdDetails withIdIssuedState(String idIssuedState) {
        this.idIssuedState = idIssuedState;
        return this;
    }
}
