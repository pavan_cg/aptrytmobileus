
package com.aptrytus.request;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateUserRequest {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("primaryPhone")
    @Expose
    private String primaryPhone;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("maritalStatus")
    @Expose
    private String maritalStatus;
    @SerializedName("idDetails")
    @Expose
    private IdDetails idDetails;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("userRelationShip")
    @Expose
    private String userRelationShip;
    @SerializedName("insuranceDetails")
    @Expose
    private List<InsuranceDetail> insuranceDetails = null;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("pcpDetails")
    @Expose
    private PcpDetails pcpDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CreateUserRequest withId(String id) {
        this.id = id;
        return this;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public CreateUserRequest withPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public CreateUserRequest withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public CreateUserRequest withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public CreateUserRequest withMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public CreateUserRequest withMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
        return this;
    }

    public IdDetails getIdDetails() {
        return idDetails;
    }

    public void setIdDetails(IdDetails idDetails) {
        this.idDetails = idDetails;
    }

    public CreateUserRequest withIdDetails(IdDetails idDetails) {
        this.idDetails = idDetails;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public CreateUserRequest withGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public CreateUserRequest withDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public CreateUserRequest withAddress(Address address) {
        this.address = address;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CreateUserRequest withEmail(String email) {
        this.email = email;
        return this;
    }

    public String getUserRelationShip() {
        return userRelationShip;
    }

    public void setUserRelationShip(String userRelationShip) {
        this.userRelationShip = userRelationShip;
    }

    public CreateUserRequest withUserRelationShip(String userRelationShip) {
        this.userRelationShip = userRelationShip;
        return this;
    }

    public List<InsuranceDetail> getInsuranceDetails() {
        return insuranceDetails;
    }

    public void setInsuranceDetails(List<InsuranceDetail> insuranceDetails) {
        this.insuranceDetails = insuranceDetails;
    }

    public CreateUserRequest withInsuranceDetails(List<InsuranceDetail> insuranceDetails) {
        this.insuranceDetails = insuranceDetails;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public CreateUserRequest withUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CreateUserRequest withPassword(String password) {
        this.password = password;
        return this;
    }

    public PcpDetails getPcpDetails() {
        return pcpDetails;
    }

    public void setPcpDetails(PcpDetails pcpDetails) {
        this.pcpDetails = pcpDetails;
    }

    public CreateUserRequest withPcpDetails(PcpDetails pcpDetails) {
        this.pcpDetails = pcpDetails;
        return this;
    }

}
