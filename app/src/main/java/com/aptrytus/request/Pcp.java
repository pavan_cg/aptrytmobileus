
package com.aptrytus.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pcp {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Pcp withName(String name) {
        this.name = name;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Pcp withPhone(String phone) {
        this.phone = phone;
        return this;
    }

}
