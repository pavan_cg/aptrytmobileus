package com.aptrytus.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PcpDetails {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("contact")
    @Expose
    private String contact;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PcpDetails withName(String name) {
        this.name = name;
        return this;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public PcpDetails withContact(String contact) {
        this.contact = contact;
        return this;
    }
}
