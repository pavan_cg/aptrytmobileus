package com.aptrytus.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InsuranceDetail {

    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public InsuranceDetail withPriority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InsuranceDetail withName(String name) {
        this.name = name;
        return this;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public InsuranceDetail withIsActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }
}
