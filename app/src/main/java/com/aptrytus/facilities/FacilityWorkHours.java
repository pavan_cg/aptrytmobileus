
package com.aptrytus.facilities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilityWorkHours {

    @SerializedName("regular")
    @Expose
    private List<Regular> regular = null;
    @SerializedName("occupational")
    @Expose
    private List<Occupational> occupational = null;

    public List<Regular> getRegular() {
        return regular;
    }

    public void setRegular(List<Regular> regular) {
        this.regular = regular;
    }

    public List<Occupational> getOccupational() {
        return occupational;
    }

    public void setOccupational(List<Occupational> occupational) {
        this.occupational = occupational;
    }

}
