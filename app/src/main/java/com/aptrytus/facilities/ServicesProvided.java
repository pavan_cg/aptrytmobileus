
package com.aptrytus.facilities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServicesProvided {

    @SerializedName("immunizations")
    @Expose
    private List<String> immunizations = null;
    @SerializedName("medicalTests")
    @Expose
    private List<String> medicalTests = null;
    @SerializedName("labTests")
    @Expose
    private List<String> labTests = null;
    @SerializedName("imagingTests")
    @Expose
    private List<String> imagingTests = null;
    @SerializedName("phyiscalTherapy")
    @Expose
    private String phyiscalTherapy;
    @SerializedName("injuries")
    @Expose
    private List<String> injuries = null;
    @SerializedName("illnessTreatments")
    @Expose
    private List<String> illnessTreatments = null;
    @SerializedName("occpWorkRelatedInjuries")
    @Expose
    private List<String> occpWorkRelatedInjuries = null;
    @SerializedName("occpDrugAndAlcoholTests")
    @Expose
    private List<String> occpDrugAndAlcoholTests = null;
    @SerializedName("occpPhysicals")
    @Expose
    private List<String> occpPhysicals = null;
    @SerializedName("otherServices")
    @Expose
    private List<String> otherServices = null;

    public List<String> getImmunizations() {
        return immunizations;
    }

    public void setImmunizations(List<String> immunizations) {
        this.immunizations = immunizations;
    }

    public ServicesProvided withImmunizations(List<String> immunizations) {
        this.immunizations = immunizations;
        return this;
    }

    public List<String> getMedicalTests() {
        return medicalTests;
    }

    public void setMedicalTests(List<String> medicalTests) {
        this.medicalTests = medicalTests;
    }

    public ServicesProvided withMedicalTests(List<String> medicalTests) {
        this.medicalTests = medicalTests;
        return this;
    }

    public List<String> getLabTests() {
        return labTests;
    }

    public void setLabTests(List<String> labTests) {
        this.labTests = labTests;
    }

    public ServicesProvided withLabTests(List<String> labTests) {
        this.labTests = labTests;
        return this;
    }

    public List<String> getImagingTests() {
        return imagingTests;
    }

    public void setImagingTests(List<String> imagingTests) {
        this.imagingTests = imagingTests;
    }

    public ServicesProvided withImagingTests(List<String> imagingTests) {
        this.imagingTests = imagingTests;
        return this;
    }

    public String getPhyiscalTherapy() {
        return phyiscalTherapy;
    }

    public void setPhyiscalTherapy(String phyiscalTherapy) {
        this.phyiscalTherapy = phyiscalTherapy;
    }

    public ServicesProvided withPhyiscalTherapy(String phyiscalTherapy) {
        this.phyiscalTherapy = phyiscalTherapy;
        return this;
    }

    public List<String> getInjuries() {
        return injuries;
    }

    public void setInjuries(List<String> injuries) {
        this.injuries = injuries;
    }

    public ServicesProvided withInjuries(List<String> injuries) {
        this.injuries = injuries;
        return this;
    }

    public List<String> getIllnessTreatments() {
        return illnessTreatments;
    }

    public void setIllnessTreatments(List<String> illnessTreatments) {
        this.illnessTreatments = illnessTreatments;
    }

    public ServicesProvided withIllnessTreatments(List<String> illnessTreatments) {
        this.illnessTreatments = illnessTreatments;
        return this;
    }

    public List<String> getOccpWorkRelatedInjuries() {
        return occpWorkRelatedInjuries;
    }

    public void setOccpWorkRelatedInjuries(List<String> occpWorkRelatedInjuries) {
        this.occpWorkRelatedInjuries = occpWorkRelatedInjuries;
    }

    public ServicesProvided withOccpWorkRelatedInjuries(List<String> occpWorkRelatedInjuries) {
        this.occpWorkRelatedInjuries = occpWorkRelatedInjuries;
        return this;
    }

    public List<String> getOccpDrugAndAlcoholTests() {
        return occpDrugAndAlcoholTests;
    }

    public void setOccpDrugAndAlcoholTests(List<String> occpDrugAndAlcoholTests) {
        this.occpDrugAndAlcoholTests = occpDrugAndAlcoholTests;
    }

    public ServicesProvided withOccpDrugAndAlcoholTests(List<String> occpDrugAndAlcoholTests) {
        this.occpDrugAndAlcoholTests = occpDrugAndAlcoholTests;
        return this;
    }

    public List<String> getOccpPhysicals() {
        return occpPhysicals;
    }

    public void setOccpPhysicals(List<String> occpPhysicals) {
        this.occpPhysicals = occpPhysicals;
    }

    public ServicesProvided withOccpPhysicals(List<String> occpPhysicals) {
        this.occpPhysicals = occpPhysicals;
        return this;
    }

    public List<String> getOtherServices() {
        return otherServices;
    }

    public void setOtherServices(List<String> otherServices) {
        this.otherServices = otherServices;
    }

    public ServicesProvided withOtherServices(List<String> otherServices) {
        this.otherServices = otherServices;
        return this;
    }

}
