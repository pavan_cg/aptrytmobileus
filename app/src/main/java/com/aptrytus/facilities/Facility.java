
package com.aptrytus.facilities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Facility {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("facilityName")
    @Expose
    private String facilityName;
    @SerializedName("facilityImage")
    @Expose
    private String facilityImage;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("facilityContact")
    @Expose
    private FacilityContact facilityContact;
    @SerializedName("facilityBrand")
    @Expose
    private String facilityBrand;
    @SerializedName("facilityAttributes")
    @Expose
    private FacilityAttributes facilityAttributes;
    @SerializedName("facilityWorkingHours")
    @Expose
    private FacilityWorkingHours facilityWorkingHours;

    public List<SelfPayService> getSelfPayServices() {
        return selfPayServices;
    }

    public void setSelfPayServices(List<SelfPayService> selfPayServices) {
        this.selfPayServices = selfPayServices;
    }

    @SerializedName("insurancesAccepted")

    @Expose
    private List<String> insurancesAccepted = null;
    @SerializedName("facilityActiveStatus")
    @Expose
    private String facilityActiveStatus;
    @SerializedName("servicesProvided")
    @Expose
    private ServicesProvided servicesProvided;
    @SerializedName("providerSearchTags")
    @Expose
    private Object providerSearchTags;
    @SerializedName("selfPayServices")
    @Expose
    private List<SelfPayService> selfPayServices = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Facility withId(Integer id) {
        this.id = id;
        return this;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public Facility withFacilityName(String facilityName) {
        this.facilityName = facilityName;
        return this;
    }

    public String getFacilityImage() {
        return facilityImage;
    }

    public void setFacilityImage(String facilityImage) {
        this.facilityImage = facilityImage;
    }

    public Facility withFacilityImage(String facilityImage) {
        this.facilityImage = facilityImage;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Facility withAddress(Address address) {
        this.address = address;
        return this;
    }

    public FacilityContact getFacilityContact() {
        return facilityContact;
    }

    public void setFacilityContact(FacilityContact facilityContact) {
        this.facilityContact = facilityContact;
    }

    public Facility withFacilityContact(FacilityContact facilityContact) {
        this.facilityContact = facilityContact;
        return this;
    }

    public String getFacilityBrand() {
        return facilityBrand;
    }

    public void setFacilityBrand(String facilityBrand) {
        this.facilityBrand = facilityBrand;
    }

    public Facility withFacilityBrand(String facilityBrand) {
        this.facilityBrand = facilityBrand;
        return this;
    }

    public FacilityAttributes getFacilityAttributes() {
        return facilityAttributes;
    }

    public void setFacilityAttributes(FacilityAttributes facilityAttributes) {
        this.facilityAttributes = facilityAttributes;
    }

    public Facility withFacilityAttributes(FacilityAttributes facilityAttributes) {
        this.facilityAttributes = facilityAttributes;
        return this;
    }

    public FacilityWorkingHours getFacilityWorkingHours() {
        return facilityWorkingHours;
    }

    public void setFacilityWorkingHours(FacilityWorkingHours facilityWorkingHours) {
        this.facilityWorkingHours = facilityWorkingHours;
    }

    public Facility withFacilityWorkingHours(FacilityWorkingHours facilityWorkingHours) {
        this.facilityWorkingHours = facilityWorkingHours;
        return this;
    }

    public List<String> getInsurancesAccepted() {
        return insurancesAccepted;
    }

    public void setInsurancesAccepted(List<String> insurancesAccepted) {
        this.insurancesAccepted = insurancesAccepted;
    }

    public Facility withInsurancesAccepted(List<String> insurancesAccepted) {
        this.insurancesAccepted = insurancesAccepted;
        return this;
    }

    public String getFacilityActiveStatus() {
        return facilityActiveStatus;
    }

    public void setFacilityActiveStatus(String facilityActiveStatus) {
        this.facilityActiveStatus = facilityActiveStatus;
    }

    public Facility withFacilityActiveStatus(String facilityActiveStatus) {
        this.facilityActiveStatus = facilityActiveStatus;
        return this;
    }

    public ServicesProvided getServicesProvided() {
        return servicesProvided;
    }

    public void setServicesProvided(ServicesProvided servicesProvided) {
        this.servicesProvided = servicesProvided;
    }

    public Facility withServicesProvided(ServicesProvided servicesProvided) {
        this.servicesProvided = servicesProvided;
        return this;
    }

    public Object getProviderSearchTags() {
        return providerSearchTags;
    }

    public void setProviderSearchTags(Object providerSearchTags) {
        this.providerSearchTags = providerSearchTags;
    }

    public Facility withProviderSearchTags(Object providerSearchTags) {
        this.providerSearchTags = providerSearchTags;
        return this;
    }

}
