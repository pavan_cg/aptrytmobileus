
package com.aptrytus.facilities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilityContact {

    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("fax")
    @Expose
    private String fax;

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public FacilityContact withTelephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public FacilityContact withFax(String fax) {
        this.fax = fax;
        return this;
    }

}
