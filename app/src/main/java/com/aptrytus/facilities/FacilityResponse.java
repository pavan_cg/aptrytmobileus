
package com.aptrytus.facilities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilityResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("resultList")
    @Expose
    private List<Facility> resultList = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public FacilityResponse withMessage(String message) {
        this.message = message;
        return this;
    }

    public List<Facility> getFacility() {
        return resultList;
    }

    public void setResultList(List<Facility> resultList) {
        this.resultList = resultList;
    }

    public FacilityResponse withResultList(List<Facility> resultList) {
        this.resultList = resultList;
        return this;
    }

}
