
package com.aptrytus.facilities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilityAttributes {

    @SerializedName("occupationalHealth")
    @Expose
    private String occupationalHealth;
    @SerializedName("pediatricCare")
    @Expose
    private String pediatricCare;
    @SerializedName("familyCare")
    @Expose
    private String familyCare;
    @SerializedName("selfPay")
    @Expose
    private String selfPay;
    @SerializedName("medicare")
    @Expose
    private String medicare;

    public String getOccupationalHealth() {
        return occupationalHealth;
    }

    public void setOccupationalHealth(String occupationalHealth) {
        this.occupationalHealth = occupationalHealth;
    }

    public FacilityAttributes withOccupationalHealth(String occupationalHealth) {
        this.occupationalHealth = occupationalHealth;
        return this;
    }

    public String getPediatricCare() {
        return pediatricCare;
    }

    public void setPediatricCare(String pediatricCare) {
        this.pediatricCare = pediatricCare;
    }

    public FacilityAttributes withPediatricCare(String pediatricCare) {
        this.pediatricCare = pediatricCare;
        return this;
    }

    public String getFamilyCare() {
        return familyCare;
    }

    public void setFamilyCare(String familyCare) {
        this.familyCare = familyCare;
    }

    public FacilityAttributes withFamilyCare(String familyCare) {
        this.familyCare = familyCare;
        return this;
    }

    public String getSelfPay() {
        return selfPay;
    }

    public void setSelfPay(String selfPay) {
        this.selfPay = selfPay;
    }

    public FacilityAttributes withSelfPay(String selfPay) {
        this.selfPay = selfPay;
        return this;
    }

    public String getMedicare() {
        return medicare;
    }

    public void setMedicare(String medicare) {
        this.medicare = medicare;
    }

    public FacilityAttributes withMedicare(String medicare) {
        this.medicare = medicare;
        return this;
    }

}
