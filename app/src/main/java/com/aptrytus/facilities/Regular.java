
package com.aptrytus.facilities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Regular {

    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("end")
    @Expose
    private String end;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

}
