package com.aptrytus.facilities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultList {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("facilityName")
    @Expose
    private String facilityName;
    @SerializedName("facilityImage")
    @Expose
    private String facilityImage;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("facilityContact")
    @Expose
    private FacilityContact facilityContact;
    @SerializedName("facilityBrand")
    @Expose
    private String facilityBrand;
    @SerializedName("facilityAttributes")
    @Expose
    private FacilityAttributes facilityAttributes;
    @SerializedName("facilityWorkingHours")
    @Expose
    private FacilityWorkingHours facilityWorkingHours;
    @SerializedName("insurancesAccepted")
    @Expose
    private List<String> insurancesAccepted = null;
    @SerializedName("facilityActiveStatus")
    @Expose
    private String facilityActiveStatus;
    @SerializedName("servicesProvided")
    @Expose
    private ServicesProvided servicesProvided;
    @SerializedName("providerSearchTags")
    @Expose
    private Object providerSearchTags;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ResultList withId(Integer id) {
        this.id = id;
        return this;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public ResultList withFacilityName(String facilityName) {
        this.facilityName = facilityName;
        return this;
    }

    public String getFacilityImage() {
        return facilityImage;
    }

    public void setFacilityImage(String facilityImage) {
        this.facilityImage = facilityImage;
    }

    public ResultList withFacilityImage(String facilityImage) {
        this.facilityImage = facilityImage;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ResultList withAddress(Address address) {
        this.address = address;
        return this;
    }

    public FacilityContact getFacilityContact() {
        return facilityContact;
    }

    public void setFacilityContact(FacilityContact facilityContact) {
        this.facilityContact = facilityContact;
    }

    public ResultList withFacilityContact(FacilityContact facilityContact) {
        this.facilityContact = facilityContact;
        return this;
    }

    public String getFacilityBrand() {
        return facilityBrand;
    }

    public void setFacilityBrand(String facilityBrand) {
        this.facilityBrand = facilityBrand;
    }

    public ResultList withFacilityBrand(String facilityBrand) {
        this.facilityBrand = facilityBrand;
        return this;
    }

    public FacilityAttributes getFacilityAttributes() {
        return facilityAttributes;
    }

    public void setFacilityAttributes(FacilityAttributes facilityAttributes) {
        this.facilityAttributes = facilityAttributes;
    }

    public ResultList withFacilityAttributes(FacilityAttributes facilityAttributes) {
        this.facilityAttributes = facilityAttributes;
        return this;
    }

    public FacilityWorkingHours getFacilityWorkingHours() {
        return facilityWorkingHours;
    }

    public void setFacilityWorkingHours(FacilityWorkingHours facilityWorkingHours) {
        this.facilityWorkingHours = facilityWorkingHours;
    }

    public ResultList withFacilityWorkingHours(FacilityWorkingHours facilityWorkingHours) {
        this.facilityWorkingHours = facilityWorkingHours;
        return this;
    }

    public List<String> getInsurancesAccepted() {
        return insurancesAccepted;
    }

    public void setInsurancesAccepted(List<String> insurancesAccepted) {
        this.insurancesAccepted = insurancesAccepted;
    }

    public ResultList withInsurancesAccepted(List<String> insurancesAccepted) {
        this.insurancesAccepted = insurancesAccepted;
        return this;
    }

    public String getFacilityActiveStatus() {
        return facilityActiveStatus;
    }

    public void setFacilityActiveStatus(String facilityActiveStatus) {
        this.facilityActiveStatus = facilityActiveStatus;
    }

    public ResultList withFacilityActiveStatus(String facilityActiveStatus) {
        this.facilityActiveStatus = facilityActiveStatus;
        return this;
    }

    public ServicesProvided getServicesProvided() {
        return servicesProvided;
    }

    public void setServicesProvided(ServicesProvided servicesProvided) {
        this.servicesProvided = servicesProvided;
    }

    public ResultList withServicesProvided(ServicesProvided servicesProvided) {
        this.servicesProvided = servicesProvided;
        return this;
    }

    public Object getProviderSearchTags() {
        return providerSearchTags;
    }

    public void setProviderSearchTags(Object providerSearchTags) {
        this.providerSearchTags = providerSearchTags;
    }

    public ResultList withProviderSearchTags(Object providerSearchTags) {
        this.providerSearchTags = providerSearchTags;
        return this;
    }

}
