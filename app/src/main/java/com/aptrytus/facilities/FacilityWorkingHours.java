package com.aptrytus.facilities;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilityWorkingHours {

    @SerializedName("regular")
    @Expose
    private List<String> regular = null;
    @SerializedName("occupational")
    @Expose
    private List<String> occupational = null;
    @SerializedName("physicalTherapy")
    @Expose
    private List<String> physicalTherapy = null;
    @SerializedName("serviceList")
    @Expose
    private List<ServiceList> serviceList = null;

    public List<String> getRegular() {
        return regular;
    }

    public void setRegular(List<String> regular) {
        this.regular = regular;
    }

    public FacilityWorkingHours withRegular(List<String> regular) {
        this.regular = regular;
        return this;
    }

    public List<String> getOccupational() {
        return occupational;
    }

    public void setOccupational(List<String> occupational) {
        this.occupational = occupational;
    }

    public FacilityWorkingHours withOccupational(List<String> occupational) {
        this.occupational = occupational;
        return this;
    }

    public List<String> getPhysicalTherapy() {
        return physicalTherapy;
    }

    public void setPhysicalTherapy(List<String> physicalTherapy) {
        this.physicalTherapy = physicalTherapy;
    }

    public FacilityWorkingHours withPhysicalTherapy(List<String> physicalTherapy) {
        this.physicalTherapy = physicalTherapy;
        return this;
    }

    public List<ServiceList> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<ServiceList> serviceList) {
        this.serviceList = serviceList;
    }

    public FacilityWorkingHours withServiceList(List<ServiceList> serviceList) {
        this.serviceList = serviceList;
        return this;
    }

}
