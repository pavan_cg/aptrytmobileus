
package com.aptrytus.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuditInfo {

    @SerializedName("lastUpdatedby")
    @Expose
    private String lastUpdatedby;
    @SerializedName("lastUpdatedDateTime")
    @Expose
    private String lastUpdatedDateTime;
    @SerializedName("acctCreationDateTime")
    @Expose
    private String acctCreationDateTime;

    public String getLastUpdatedby() {
        return lastUpdatedby;
    }

    public void setLastUpdatedby(String lastUpdatedby) {
        this.lastUpdatedby = lastUpdatedby;
    }

    public String getLastUpdatedDateTime() {
        return lastUpdatedDateTime;
    }

    public void setLastUpdatedDateTime(String lastUpdatedDateTime) {
        this.lastUpdatedDateTime = lastUpdatedDateTime;
    }

    public String getAcctCreationDateTime() {
        return acctCreationDateTime;
    }

    public void setAcctCreationDateTime(String acctCreationDateTime) {
        this.acctCreationDateTime = acctCreationDateTime;
    }

}
