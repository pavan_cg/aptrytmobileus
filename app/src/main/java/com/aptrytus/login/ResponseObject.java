
package com.aptrytus.login;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseObject {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("primaryPhone")
    @Expose
    private String primaryPhone;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("maritalStatus")
    @Expose
    private String maritalStatus;
    @SerializedName("driverLicenseNumber")
    @Expose
    private String driverLicenseNumber;
    @SerializedName("driverLicenseValidFromDate")
    @Expose
    private String driverLicenseValidFromDate;
    @SerializedName("driverLicenseValidToDate")
    @Expose
    private String driverLicenseValidToDate;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("family")
    @Expose
    private Object family;
    @SerializedName("insurance")
    @Expose
    private List<Insurance> insurance = null;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("isGuest")
    @Expose
    private String isGuest;
    @SerializedName("otpVerfCode")
    @Expose
    private String otpVerfCode;
    @SerializedName("otpCreateDateTime")
    @Expose
    private String otpCreateDateTime;
    @SerializedName("userStatus")
    @Expose
    private String userStatus;
    @SerializedName("pcp")
    @Expose
    private Pcp pcp;
    @SerializedName("auditInfo")
    @Expose
    private AuditInfo auditInfo;
    @SerializedName("primaryUser")
    @Expose
    private Boolean primaryUser;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getDriverLicenseNumber() {
        return driverLicenseNumber;
    }

    public void setDriverLicenseNumber(String driverLicenseNumber) {
        this.driverLicenseNumber = driverLicenseNumber;
    }

    public String getDriverLicenseValidFromDate() {
        return driverLicenseValidFromDate;
    }

    public void setDriverLicenseValidFromDate(String driverLicenseValidFromDate) {
        this.driverLicenseValidFromDate = driverLicenseValidFromDate;
    }

    public String getDriverLicenseValidToDate() {
        return driverLicenseValidToDate;
    }

    public void setDriverLicenseValidToDate(String driverLicenseValidToDate) {
        this.driverLicenseValidToDate = driverLicenseValidToDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getFamily() {
        return family;
    }

    public void setFamily(Object family) {
        this.family = family;
    }

    public List<Insurance> getInsurance() {
        return insurance;
    }

    public void setInsurance(List<Insurance> insurance) {
        this.insurance = insurance;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIsGuest() {
        return isGuest;
    }

    public void setIsGuest(String isGuest) {
        this.isGuest = isGuest;
    }

    public String getOtpVerfCode() {
        return otpVerfCode;
    }

    public void setOtpVerfCode(String otpVerfCode) {
        this.otpVerfCode = otpVerfCode;
    }

    public String getOtpCreateDateTime() {
        return otpCreateDateTime;
    }

    public void setOtpCreateDateTime(String otpCreateDateTime) {
        this.otpCreateDateTime = otpCreateDateTime;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public Pcp getPcp() {
        return pcp;
    }

    public void setPcp(Pcp pcp) {
        this.pcp = pcp;
    }

    public AuditInfo getAuditInfo() {
        return auditInfo;
    }

    public void setAuditInfo(AuditInfo auditInfo) {
        this.auditInfo = auditInfo;
    }

    public Boolean getPrimaryUser() {
        return primaryUser;
    }

    public void setPrimaryUser(Boolean primaryUser) {
        this.primaryUser = primaryUser;
    }

}
