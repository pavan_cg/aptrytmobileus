package com.aptrytus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.aptrytus.Others.SharedPreferenceConstants;
import com.aptrytus.repositories.AppSharedPrefs;
import com.aptrytus.uiactivities.HomeScreenActivity;
import com.aptrytus.uiactivities.TermsAndConditionsActivity;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Thread() {

            @Override
            public void run() {
                try {
                    sleep(2000);
                    if (!AppSharedPrefs.getInstance(getApplicationContext()).getBoolean(SharedPreferenceConstants.CHECK_APP_INSTALLED)) {
                        finish();
                        startActivity(new Intent(SplashScreen.this, TermsAndConditionsActivity.class));
                    } else {
                        finish();
                        startActivity(new Intent(SplashScreen.this, HomeScreenActivity.class));
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
}
