package com.aptrytus.Others;

public class SharedPreferenceConstants {

    public static final String APTRY_PREF = "APTRYTPREFERENCES";

    public static final String CHECK_APP_INSTALLED = "APPINSTALLEDCHECK";

    public static final String LOGEED_IN_USER_NAME = "USERNAME";

    public static final String LOGEED_IN_USER_MOBILE = "MOBILE";

    public static final String LOGEED_IN_USER_ID = "USERID";

    public static final String LOGEED_IN_USER_EMAIL_ID = "USEREMAILID";

    public static final String SCAN_FROM_SETTINGS = "settingscheck";

    public static final String SELECTED_FAMILY_MEMBERS = "selectedmember";

}
