package com.aptrytus.Others;

import android.app.ProgressDialog;
import android.content.Context;

import com.aptrytus.facilities.FacilityResponse;
import com.aptrytus.login.LoginResponse;
import com.aptrytus.services.ApiResponse;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utility {

    public static final String APTRY_PREF = "PREFERENCES";
    public static final String SEARHED_TEXT ="SEARCHEDTEXT";
    public static final String DATAFROMSEARCHFRAGMENT = "SEARCHFLAG";
    public static final String ZIPCODE = "ZIPCODE";
    public static final String CITY = "CITY";

    public static LoginResponse loginResponse = null;

    public static FacilityResponse facilityResponse;

    public static ProgressDialog getProgressDialog(Context context, String msg) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

    public static String getFormatedDate(String date){
        String modifiedDate = "";
        String year = date.substring(4);
        String month = date.substring(0,2);
        String day = date.substring(2,4);
        modifiedDate = year+"-"+month+"-"+day;
        return modifiedDate;
    }

    public static String formatUSPhoneNum(String phoneNo) {
        String phoneNumber = "";

        String code = phoneNo.substring(1,4);
        String ph1 = phoneNo.substring(6,9);
        String ph2 = phoneNo.substring(10,14);
        String countryCode = "+91";
        phoneNumber = countryCode+code+ph1+ph2;

        return phoneNumber;
    }


    public static String getApiError(ApiResponse apiResponse){
        String message = "";
        try {
            message = apiResponse.getApiError().getString("message").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }
        return message;
    }

    public static int getAge(String dobString) {

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        return age;
    }


}
