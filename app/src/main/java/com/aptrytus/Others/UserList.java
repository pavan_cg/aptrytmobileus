package com.aptrytus.Others;

public class UserList {

    private String firstName;
    private String lastName;
    private String age;
    private String relation;
    private String userId;

    public UserList(String firstName,String lastName,String age,String relation,String userId){

        this.age = age;
        this.firstName = firstName;
        this.lastName = lastName;
        this.relation = relation;
        this.userId = userId;

    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
