package com.aptrytus.viewholders;

import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aptrytus.R;

public class DoctorViewHolder extends RecyclerView.ViewHolder {
    public TextView doctorNameView;

    public ImageView umberalla_icon;

    public ImageView drlist_appointment;

    public ImageView selfService_icon;

    public ImageView ocupational_icon;

    public ImageView pediatric_icon;

    public ImageView medicare_icon;

    public ImageView family_icon;

    public TextView facilityAddress;

    public TextView facilityBrand;

    public DoctorViewHolder(View itemView) {

        super(itemView);

        doctorNameView = (TextView) itemView.findViewById(R.id.doctorName);

        umberalla_icon = (ImageView) itemView.findViewById(R.id.icon_insurance);

        drlist_appointment = (ImageView) itemView.findViewById(R.id.dr_list_appointment);

        facilityAddress = (TextView) itemView.findViewById(R.id.facility_list_address);

        selfService_icon = (ImageView) itemView.findViewById(R.id.icon_selfpay);

        ocupational_icon = (ImageView) itemView.findViewById(R.id.icon_occupational);

        pediatric_icon = (ImageView) itemView.findViewById(R.id.icon_pediatric);

        medicare_icon = (ImageView) itemView.findViewById(R.id.icon_medicare);

        family_icon = (ImageView) itemView.findViewById(R.id.icon_familycare);

        facilityBrand = (TextView) itemView.findViewById(R.id.facilityBrand);
    }

    /**
     * Set value in our textview
     *
     * @param text
     */
    public void setText(String text) {

        this.doctorNameView.setText(text);
    }
}
